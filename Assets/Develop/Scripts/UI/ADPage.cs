﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ADPage : Page
{
    public MyScrollRect scroll;
    BannerInfo info;
    public Button shopLinkBTN;
    public Button webLinkBTN;

    public override void Pull()
    {
        this.GetComponent<Canvas>().enabled = true;
        info = ApplicationManager.GetInstance().current_banner_info;
        SetTitle(info._title);
        isOn = true;
        move = true;
        direction = LEFT;
        targetX = 0;
        if(!reStart)
            scroll.verticalNormalizedPosition = 1;
        SetSpeed();
        if (header)
            header.RePosition();
        if((info._web_url == "") && (info._shop_id == -1))
            ApplicationManager.GetInstance().SetDragThreshold(false);
        else
            ApplicationManager.GetInstance().SetDragThreshold(true);

        ApplicationManager.GetInstance().mainPage.Pushed();
    }
    public override void Push()
    {
        isOn = false;
        move = true;
        direction = RIGHT;
        targetX = ApplicationManager.GetInstance().canvas.sizeDelta.x;
        SetSpeed();
        ApplicationManager.GetInstance().mainPage.Pulled();
        ApplicationManager.GetInstance().SetDragThreshold(true);
    }
    public override void Stop()
    {
        move_time = 0.0f;
        move = false;

        Vector2 newPos = new Vector2(targetX, 0);
        this.GetComponent<RectTransform>().anchoredPosition = newPos;

        direction = STOP;
        if (targetX != 0) // 밀어 냈을 때
        {
            ApplicationManager.GetInstance().bannerManager.ClearBanner();
            this.GetComponent<Canvas>().enabled = false;
            this.GetComponent<GraphicRaycaster>().enabled = false;
            ApplicationManager.GetInstance().page_index = 0;
        }
        else // 당겼을 때
        {
            ApplicationManager.GetInstance().page_index = 6;
            this.GetComponent<GraphicRaycaster>().enabled = true;
            if (!reStart)
                SetPage();
        }
        reStart = false;
    }
    private void SetPage()
    {
        ApplicationManager.GetInstance().bannerManager.LoadCurrentBanner();
        //float y = 0;
        // 여기서 콘텐츠 이미지 로드 및 적용
        
        // y = 콘텐츠 이미지.y;


        // 링크 버튼 만들기 및 위치 조정 // 쿠폰버튼처러 웹방문 or 상세페이지 보러가기 버튼 만들자
        // y += shopLinkBTN.y + a;
        if(info._shop_id == -1)
            shopLinkBTN.gameObject.SetActive(false);
        else
            shopLinkBTN.gameObject.SetActive(true);

        // y += webLinkBTN.y + a;
        if (info._web_url == "")
            webLinkBTN.gameObject.SetActive(false);
        else
            webLinkBTN.gameObject.SetActive(true);

        // 콘텐츠 size.x = screen.width, size.y = y;
    }
}
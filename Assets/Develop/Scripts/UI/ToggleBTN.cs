﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleBTN : MonoBehaviour
{
    RectTransform circle;
    bool move = false;
    protected bool toggle = false;
    protected bool available = true;
    float target;
    float timer = 0;
    const float TIME = 0.15f;
    float speed = 0.1f;
    static Color inactive = new Color(0.8f,0.8f,0.8f);

    private void Start()
    {
        Init();
    }
    private void Init()
    {
        move = false;
        timer = 0;
        circle = transform.GetChild(0).transform.GetChild(2).GetComponent<RectTransform>();
        target = transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.x;
        speed = target / TIME;

        InitToggle();
    }
    protected virtual void InitToggle()
    {
        available = true;
        toggle = false;
    }
    private void Update()
    {
        if (move)
        {
            timer += Time.deltaTime;
            if (toggle)
            {
                if (timer * speed >= target)
                {
                    circle.anchoredPosition = new Vector2(target, 0);
                    move = false;
                }
                else
                    circle.anchoredPosition = new Vector2(timer * speed, 0);
            }
            else
            {
                if ((target - timer * speed) <= 0)
                {
                    circle.anchoredPosition = new Vector2(0, 0);
                    move = false;
                }
                else
                    circle.anchoredPosition = new Vector2((target - timer * speed), 0);
            }
        }
    }
    void OnEnable()
    {
        InitToggle();
        ChangeBTNStyle(true);
    }
    public void Toggle()
    {
        if(available)
        {
            toggle = !toggle;
            SetToggle();
            ChangeBTNStyle();
        }
        else
        {
            ApplicationManager.GetInstance().ShowOneBtnDialog("권한 필요","현재 앱에서 해당 권한이 거부 되어 있습니다. 앱 설정에서 해당 권한을 허용해주세요","확인",null);
        }
    }
    void SetToggle()
    {
        if(toggle)
        {
            ToggleON();
        }
        else
        {
            ToggleOFF();
        }
    }

    protected virtual void ToggleON() { }
    protected virtual void ToggleOFF() { }

    void ChangeBTNStyle(bool init = false)
    {
        if(circle == null)
            Init();

        if (toggle)
        {
            transform.GetChild(0).GetComponent<RawImage>().color = ApplicationManager.yellow;
            transform.GetChild(0).transform.GetChild(0).GetComponent<RawImage>().color = ApplicationManager.yellow;
            transform.GetChild(0).transform.GetChild(1).GetComponent<RawImage>().color = ApplicationManager.yellow;
            if (init)
                circle.anchoredPosition = new Vector2(target,0);
            else
                move = true;
            timer = 0;
        }
        else
        {
            transform.GetChild(0).GetComponent<RawImage>().color = inactive;
            transform.GetChild(0).transform.GetChild(0).GetComponent<RawImage>().color = inactive;
            transform.GetChild(0).transform.GetChild(1).GetComponent<RawImage>().color = inactive;
            if (init)
                circle.anchoredPosition = new Vector2(0, 0);
            else
                move = true;
            timer = 0;
        }
    }
}

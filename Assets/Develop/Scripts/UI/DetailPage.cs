﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DetailPage : Page
{
    public LikeBTN likBTN;
    [HideInInspector]
    public ShopBTN shopBTN;

    public AuthBTN authBTN;

    public GameObject cheerUpload;
    public GameObject cheerBoard;

    public override void Pull()
    {
        this.GetComponent<Canvas>().enabled = true;
        isOn = true;
        move = true;
        direction = LEFT;
        targetX = 0;
        if (header)
            header.RePosition();
        ApplicationManager.GetInstance().SetDragThreshold(true);
        SetSpeed();
        ApplicationManager.GetInstance().mainPage.Pushed();
    }

    public override void Push()
    {
        if(shopBTN.like != null)
            shopBTN.like.SetSprite();
        isOn = false;
        ApplicationManager.GetInstance().mainPage.GetComponent<Canvas>().enabled = true;
        move = true;
        direction = RIGHT;
        targetX = ApplicationManager.GetInstance().canvas.sizeDelta.x;
        ApplicationManager.GetInstance().SetDragThreshold(true);

        SetSpeed();
        ApplicationManager.GetInstance().mainPage.Pulled();
    }
    public void CouponClick()
    {
        cheerUpload.SetActive(true);
        //ApplicationManager.GetInstance().CouponClick(shopBTN.shop);
    }
    public override void Stop()
    {
        if(!back)
        {
            move_time = 0.0f;
            move = false;
            Vector2 newPos = new Vector2(targetX, 0);
            this.GetComponent<RectTransform>().anchoredPosition = newPos;
            direction = STOP;
            if (targetX != 0) // 밀어 냈을 때,
            {
                this.transform.GetChild(0).GetComponent<MyScrollRect>().verticalNormalizedPosition = 1;
                ApplicationManager.GetInstance().infoManager.ReSet();
                this.GetComponent<Canvas>().enabled = false;
                this.transform.GetChild(0).GetComponent<Canvas>().enabled = false;
                this.GetComponent<GraphicRaycaster>().enabled = false;
                ApplicationManager.GetInstance().page_index = ApplicationManager.GetInstance().mainPage.page_index;
                
            }
            else
            {
                if(!reStart)
                {
                    this.GetComponent<GraphicRaycaster>().enabled = true;
                    this.transform.GetChild(0).GetComponent<Canvas>().enabled = true;
                    ApplicationManager.GetInstance().LoadDetail(shopBTN.shop);
                    ApplicationManager.GetInstance().ShopCountUp(shopBTN.shop._id, false);
                    ApplicationManager.GetInstance().page_index = 3;
                }
            }
            reStart = false;
        }
        else
        {
            if (direction == RIGHT)
                GetComponent<RectTransform>().anchoredPosition = new Vector2(pull_targetX, 0);
            move = false;
            //isOn = false;
            back = false;
            direction = STOP;
        }
    }

    public void ResetPositionToRightSide()
    {
        this.transform.GetChild(0).GetComponent<MyScrollRect>().verticalNormalizedPosition = 1;
        ApplicationManager.GetInstance().infoManager.ReSet();
        this.GetComponent<Canvas>().enabled = false;
        this.transform.GetChild(0).GetComponent<Canvas>().enabled = false;
        this.GetComponent<RectTransform>().anchoredPosition = new Vector2(ApplicationManager.GetInstance().canvas.sizeDelta.x, 0);
    }

    public void ShowCheerBoard()
    {
        ApplicationManager.GetInstance().page_index = 1001;
        ApplicationManager.GetInstance().subwindow = cheerBoard.gameObject;
        cheerBoard.SetActive(true);
        this.GetComponent<CheerManager>().StartDownloadCheer();
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CouponPage : Page
{
    public TextBox benefit;
    public Text help;
    const float SCALE = 1.0f;

    [HideInInspector]
    public Shop shop;

    public GameObject afterPage;
    public Text branch;
    public AnimationSimulator coupon;
    public override void Pull()
    {
        if (!reStart)
        {
            benefit.gameObject.SetActive(false);
            coupon.StartSimulation();
        }
        this.GetComponent<Canvas>().enabled = true;
        isOn = true;
        move = true;
        direction = LEFT;
        targetX = 0;

        SetSpeed();

        if (header)
            header.RePosition();
        
        if (afterPage.activeSelf == true)
            ApplicationManager.GetInstance().mainPage.Pushed();
        else
            ApplicationManager.GetInstance().detailPage.Pushed();
    }
    public void SetBenefit(string b)
    {
        benefit.gameObject.SetActive(true);
        benefit.Set(b);
    }
    public void SetText(bool service = false)
    {
        if(service == false)
        {
            help.text = "결제 시, 해당 화면을 제시 해 주세요";
        }
        else
        {
            help.text = "주문 시, 해당 화면을 제시 해 주세요";
        }
    }
    public void SetShop(Shop s)
    {
        shop = s;
        SetText(shop._discountType == "서비스");
        SetTitle(shop._name);
        SetBranch(shop._branch);
        SetBenefit(shop._benefit);
    }
    public override void SetTitle(string t)
    {
        title.text = t;
    }
    public void SetBranch(string t)
    {
        branch.text = t;
    }
    public override void Push()
    {
        isOn = false;
        move = true;
        direction = RIGHT;
        targetX = ApplicationManager.GetInstance().canvas.sizeDelta.x;
        SetSpeed();
        if (afterPage.activeSelf == true)
            ApplicationManager.GetInstance().mainPage.Pulled();
        else
            ApplicationManager.GetInstance().detailPage.Pulled();

    }
    public override void Stop()
    {
        move_time = 0.0f;
        move = false;
        Vector2 newPos = new Vector2(targetX, 0);
        this.GetComponent<RectTransform>().anchoredPosition = newPos;
        direction = STOP;
        if (targetX != 0) // 밀어 냈을 때,
        {
            this.GetComponent<GraphicRaycaster>().enabled = false;
            this.GetComponent<Canvas>().enabled = false;

            if (afterPage.activeSelf == true)
                ApplicationManager.GetInstance().page_index = ApplicationManager.GetInstance().mainPage.page_index;
            else
                ApplicationManager.GetInstance().page_index = 3;

            coupon.StopSimulation();
            this.GetComponent<GestureSlider>().backPage = ApplicationManager.GetInstance().detailPage.GetComponent<RectTransform>();

            afterPage.SetActive(false);
        }
        else
        {
            this.GetComponent<GraphicRaycaster>().enabled = true;
            ApplicationManager.GetInstance().page_index = 4;
        }
        reStart = false;
    }
    public void UseCoupon()
    {
        afterPage.SetActive(true);

        ApplicationManager.GetInstance().detailPage.GetComponent<DetailPage>().ResetPositionToRightSide();
        this.GetComponent<GestureSlider>().backPage = ApplicationManager.GetInstance().mainPage.GetComponent<RectTransform>();
        if (!ApplicationManager.GetInstance().ADMIN_MODE)
        {
            ApplicationManager.GetInstance().ShopCountUp(shop._id);
        }
    }
}
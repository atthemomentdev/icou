﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TextureContainer
{
    public string URL;
    public Texture texture = null;
}

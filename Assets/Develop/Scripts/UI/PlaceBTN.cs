﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlaceBTN : MonoBehaviour
{
    [HideInInspector]
    static public Color selected = new Color(0.1875f, 0.4196f, 0.3451f);
    [HideInInspector]
    static public Color unselected = new Color(0.5f, 0.5f, 0.5f);
    
    [SerializeField]
    public GameObject placeInfoObj;

    [HideInInspector]
    public PlaceInfo info;

    public void SetPlaceInfo(string id, string name, string img)
    {
        info = Instantiate(placeInfoObj, ApplicationManager.GetInstance().placeManager.contents)
               .GetComponent<PlaceInfo>();

        this.transform.GetChild(0).GetComponent<Text>().text = name;
        info.SetInfo(name, img, id);
    }

    public void Click()
    {
        int i = this.transform.GetSiblingIndex();
        ApplicationManager.GetInstance().placeManager.ChangePlace(i);
    }
    public void OFF()
    {
        this.transform.GetChild(0).GetComponent<Text>().color = unselected;
        this.transform.GetChild(0).GetComponent<Text>().fontStyle = FontStyle.Normal;
    }
    public void ON()
    {
        this.transform.GetChild(0).GetComponent<Text>().color = selected;
        this.transform.GetChild(0).GetComponent<Text>().fontStyle = FontStyle.Bold;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FilterBTN : MonoBehaviour
{
    public GameObject category;

    public void SwitchActive()
    {
        category.SetActive(!category.activeSelf);
    }
}

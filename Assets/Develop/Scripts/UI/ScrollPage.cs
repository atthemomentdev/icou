﻿using System;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityEngine.UI
{
    [SelectionBase]
    [ExecuteInEditMode]
    [DisallowMultipleComponent]
    [RequireComponent(typeof(RectTransform))]
    public class ScrollPage : MyScrollRect
    {
        bool begin = false;
        public override void OnBeginDrag(PointerEventData eventData)
        {
            begin = true;
            // 터치 좌표 변화값
            Vector2 localCursor;
            if (RectTransformUtility.ScreenPointToLocalPointInRectangle(viewRect, eventData.position, eventData.pressEventCamera, out localCursor))

                if (eventData.button != PointerEventData.InputButton.Left)
                    return;

            if (!IsActive())
                return;

            UpdateBounds();

            m_PointerStartLocalCursor = Vector2.zero;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(viewRect, eventData.position, eventData.pressEventCamera, out m_PointerStartLocalCursor);

            m_ContentStartPosition = m_Content.anchoredPosition;
            m_Dragging = true;
        }

        public override void OnDrag(PointerEventData eventData)
        {
            if (eventData.button != PointerEventData.InputButton.Left)
                return;

            if (!IsActive())
                return;

            Vector2 localCursor;
            if (!RectTransformUtility.ScreenPointToLocalPointInRectangle(viewRect, eventData.position, eventData.pressEventCamera, out localCursor))
                return;

            UpdateBounds();

            var pointerDelta = localCursor - m_PointerStartLocalCursor;

            if ((begin == true) && ((pointerDelta.x != 0) || (pointerDelta.y != 0)))
            {
                float _x = Mathf.Abs(pointerDelta.x);
                float _y = Mathf.Abs(pointerDelta.y);
                if (_x > _y)
                {
                    m_Horizontal = true;
                    m_Vertical = false;
                }
                else
                {
                    m_Vertical = true;
                    m_Horizontal = false;
                }

                begin = false;
            }
            //pointerDelta *= m_ScrollFactor;    //이 줄은 새로운 것입니다!
            Vector2 position = m_ContentStartPosition + pointerDelta;

            // Offset to get content into place in the view.
            Vector2 offset = CalculateOffset(position - m_Content.anchoredPosition);
            position += offset;

            //
            if (m_HorizontalMovementType == MovementType.Elastic)
            {
                if (offset.x != 0)
                    position.x = position.x - RubberDelta(offset.x, m_ViewBounds.size.x);
            }

            //
            if (m_VerticalMovementType == MovementType.Elastic)
            {
                if (offset.y != 0)
                    position.y = position.y - RubberDelta(offset.y, m_ViewBounds.size.y);
            }

            SetContentAnchoredPosition(position);
        }

        public override void OnEndDrag(PointerEventData eventData)
        {
        }

        protected override void LateUpdate()
        {
            if (!m_Content)
                return;

            EnsureLayoutHasRebuilt();
            UpdateScrollbarVisibility();
            UpdateBounds();
            float deltaTime = Time.unscaledDeltaTime;
            Vector2 offset = CalculateOffset(Vector2.zero);
            if (!m_Dragging)
            {
            }

            if (m_Dragging && m_Inertia)
            {
                Vector3 newVelocity = (m_Content.anchoredPosition - m_PrevPosition) / deltaTime;
                m_Velocity = newVelocity; //Vector3.Lerp(m_Velocity, newVelocity, deltaTime * 10);
            }
            if (m_ViewBounds != m_PrevViewBounds || m_ContentBounds != m_PrevContentBounds || m_Content.anchoredPosition != m_PrevPosition)
            {
                UpdateScrollbars(offset);
                m_OnValueChanged.Invoke(normalizedPosition);
                UpdatePrevData();
            }
        }
    }
}

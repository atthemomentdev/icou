﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Upper : MonoBehaviour
{
    LayoutElement layout;
    float y;
    // Start is called before the first frame update
    void Start()
    {
        layout = this.GetComponent<LayoutElement>();
        y = layout.preferredHeight;
        layout.preferredHeight = y + ApplicationManager.GetInstance().notchUp;
    }

    // Update is called once per frame
    void Update()
    {
        layout.preferredHeight = y + ApplicationManager.GetInstance().notchUp;
    }
}

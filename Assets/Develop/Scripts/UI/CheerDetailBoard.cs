﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CheerDetailBoard : MonoBehaviour
{
    public Text nickName;
    public Text order;
    public Text shopName;
    public Text contents;
    string cheerId;
    public GameObject removeBTN;
    public CheerManager cheerManager;
    public void SetDetail(CheerDetail detail)
    {
        cheerId = detail.id;
        nickName.text = detail.nickName + "님의";
        order.text = detail.order.ToString() + "번째 방문";
        contents.text = ReplaceNewLine(detail.contents);
        shopName.text = cheerManager.detail.shopBTN.shop._branch + " " + cheerManager.detail.shopBTN.shop._name;

        if ((detail.userId == ApplicationManager.GetInstance().auth.CurrentUser.UserId.ToString())
            || ApplicationManager.GetInstance().ADMIN_MODE)
        {
            removeBTN.SetActive(true);
        }
        else
        {
            removeBTN.SetActive(false);
        }
    }
    string ReplaceNewLine(string origin)
    {
        string newLine = "";

        int i = 0;
        while(i < origin.Length)
        {
            if (origin[i] == '\\')
            {
                if (origin[i + 1] == 'n')
                    newLine += '\n';

                i += 2;
            }
            else
            {
                newLine += origin[i++];
            }
        }
        return newLine;
    }
    public void ConfirmToRemove()
    {
        ApplicationManager.GetInstance().ShowTwoBtnDialog("응원 삭제", "정말로 응원글을 삭제하시겠습니까?", "네", "아니오", RemoveCheer,null);
    }
    void RemoveCheer()
    {
        cheerManager.StartRemoveCheer(cheerId);
        this.gameObject.SetActive(false);
        cheerManager.detail.cheerBoard.GetComponent<SubWindow>().Close();
        ApplicationManager.GetInstance().ShowToast("응원이 삭제되었습니다");
    }
}

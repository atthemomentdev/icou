﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CategoryBTN : MonoBehaviour
{
    private Text text;
    private string category;
    public int index;

    //현재 버튼이 생성 될 위치
    public static float posX = 0;
    static int i = 0;
    public void Start()
    {
        index = i++;
    }
    public static void Init()
    {
        i = 0;
        posX = 0;
    }
    public void SetCategory(string cat_name)
    {
        text = this.transform.GetChild(0).GetComponent<Text>();
        category = cat_name;
        text.text = category;
    }
    
    public void ChangeCategory()
    {
        CategoryManager mgr = ApplicationManager.GetInstance().categoryManager;
        ShopBTN.Init();
        mgr.ChangeCategory(this);
    }
}
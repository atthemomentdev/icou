﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LogInPage : Page
{
    public GameObject notification;
    private void Start()
    {
        SetSpeed();
        this.GetComponent<Canvas>().enabled = true;
    }
    public override void Push()
    {
        Debug.Log("Push");
        SetSpeed();
        isOn = false;
        move = true;
        direction = LEFT;
        targetX = 0;
    }
    public override void Stop()
    {
        move_time = 0.0f;
        move = false;
        Vector2 newPos = new Vector2(targetX, 0);
        this.GetComponent<RectTransform>().anchoredPosition = newPos;
        direction = STOP;
        if (targetX == 0) // 밀어 냈을 때,
        {
            this.GetComponent<Canvas>().enabled = false;
        }
    }

    public void SetNotification(string info) {
        notification.SetActive(true);
        notification.transform.GetChild(0).GetComponent<Text>().text = info;
    }
}
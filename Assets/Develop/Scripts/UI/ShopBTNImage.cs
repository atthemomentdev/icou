﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopBTNImage : BenefitImage
{
    public override void AfterSetImage()
    {
        isLoaded = true;
        this.transform.GetComponent<RawImage>().color = after_color;
        this.transform.parent.GetComponent<ShopBTN>().gradient.GetComponent<RawImage>().enabled = true;
        this.transform.parent.GetComponent<ShopBTN>().shopManager.LoadFinish();
    }
}

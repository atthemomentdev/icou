﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBar : MonoBehaviour
{
    Vector3 RIGHT = new Vector3(1.0f, 0.0f, 0.0f);
    Vector3 LEFT = new Vector3(-1.0f, 0.0f, 0.0f);
    Vector3 STOP = new Vector3(0.0f, 0.0f, 0.0f);

    float moveSpeed = 0;

    Vector3 direction = new Vector3(0.0f, 0.0f, 0.0f);

    bool move = false;

    float targetX;
    float targetW;

    const float TIME = 0.15f;
    float move_time = 0.0f;

    Vector2 startPos;
    // Start is called before the first frame update
    void Start()
    {
        startPos = this.GetComponent<RectTransform>().anchoredPosition;
    }
    // Update is called once per frame
    void Update()
    {
        if (move)
        {
            this.GetComponent<RectTransform>().anchoredPosition
                 = new Vector2(startPos.x + moveSpeed * move_time * direction.x, startPos.y);

            if (move_time > TIME)
                Stop();

            move_time += Time.deltaTime;
        }
    }
    public void SetSide(int side = 0)
    {
        switch(side)
        {
            case -1: // left
                this.transform.GetChild(0).gameObject.SetActive(true);
                this.transform.GetChild(1).gameObject.SetActive(false);
                break;
            case 0:
                this.transform.GetChild(0).gameObject.SetActive(false);
                this.transform.GetChild(1).gameObject.SetActive(false);
                break;
            case 1: //right
                this.transform.GetChild(0).gameObject.SetActive(false);
                this.transform.GetChild(1).gameObject.SetActive(true);
                break;

        }
    }
    public void Move(float where, float size)
    {
        move = true;
        targetX = where;
        targetW = size;

        SetSpeed();
        SetDirection();
        startPos = this.GetComponent<RectTransform>().anchoredPosition;
    }

    public void Move(float where)
    {
        move = true;
        targetX = where;
        targetW = GetComponent<RectTransform>().sizeDelta.x;

        SetSpeed();
        SetDirection();
        startPos = this.GetComponent<RectTransform>().anchoredPosition;
    }

    private void Stop()
    {
        move_time = 0.0f;
        move = false;
        Vector3 thisPos = this.transform.position;
        Vector3 newPos = new Vector3(targetX, 0, thisPos.z);
        this.GetComponent<RectTransform>().anchoredPosition = newPos;

        direction = STOP;

        transform.GetComponent<RectTransform>().sizeDelta = new Vector2(targetW, transform.GetComponent<RectTransform>().sizeDelta.y);
    }

    private void SetSpeed()
    {
        float d = Mathf.Abs(transform.GetComponent<RectTransform>().anchoredPosition.x - targetX);
              // v = s / t
        moveSpeed = d / TIME;
    }

    private void SetDirection()
    {
        if (this.transform.position.x == targetX)
        {
            direction = STOP;
            move = false;
        }
        else if (transform.GetComponent<RectTransform>().anchoredPosition.x < targetX)
            direction = RIGHT;
        else
            direction = LEFT;
    }
}

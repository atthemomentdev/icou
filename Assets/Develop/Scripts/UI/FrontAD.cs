﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FrontAD : MonoBehaviour
{
    public Toggle toggle;
    public void Init()
    {
        if (DataController.Instance.data.readNotification == true)
            this.gameObject.SetActive(false);
    }
    public void Close()
    {
        if (toggle.isOn)
        {
            DataController.Instance.data.readNotification = true;
            DataController.Instance.SaveData();
        }
        this.gameObject.SetActive(false);
    }
}

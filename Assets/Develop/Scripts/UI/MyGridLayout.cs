﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MyGridLayout : MonoBehaviour
{
    public RectOffset padding;
    public Vector2 cellSize;
    public Vector2 spacing;
    public int columnCount;
    public RectTransform viewport;
    public int maxCount;
    ShopManager shopManager;

    RectTransform layoutRect;
    int cellCount = 0;
    [SerializeField]
    public GameObject cellObject;

    public RectTransform scrollObject;

    float scale;
    Vector2 size;
    float contentWidth;
    float alignSize;

    public bool search = false;

    void InitGrid()
    {
        SetColumnCount();
        cellCount = 0;
        Vector2 size = viewport.sizeDelta;
        layoutRect = this.transform.GetComponent<RectTransform>();
        int newMax = (int)((size.y / (cellSize.y + spacing.y) + 3)) * columnCount;

        maxCount = newMax;
        for(int i = transform.childCount-1; i >=0; i--)
        {
            GameObject.Destroy(transform.GetChild(i).gameObject);
        }
        for(int i = 0; i < maxCount; i++)
        {
            GameObject obj = AddCell(cellObject);
        }
        
        this.transform.GetChild(0).SetAsLastSibling();
    }
    void SetColumnCount()
    {
        scale = 1f;
        size = ApplicationManager.GetInstance().canvas.sizeDelta;

        float ratio = ApplicationManager.GetInstance()._screen.x / ApplicationManager.GetInstance()._screen.y;
        float MAX_RATIO = 0.68f;
        if(ratio > MAX_RATIO)
        {
            scale = ApplicationManager.GetInstance().canvas.sizeDelta.x / contentWidth;
            columnCount = 3;

            contentWidth = padding.left + padding.right + columnCount * cellSize.x + (columnCount - 1) * spacing.x;

            alignSize = (contentWidth - this.GetComponent<RectTransform>().sizeDelta.x)/2f;

            scale = size.x / contentWidth;

        }
        else
        {
            columnCount = 2;

            contentWidth = padding.left + padding.right + columnCount * cellSize.x + (columnCount - 1) * spacing.x;

            alignSize = 0;
        }
        this.transform.localScale = new Vector3(scale, scale, scale);
    }
    private void Start()
    {
        if (cellCount == 0)
            InitGrid();
    }
    // Update is called once per frame
    void Update()
    {
        Vector2 newSize = ApplicationManager.GetInstance().canvas.sizeDelta;
        if(size != newSize)
        {
            Set();
            size = newSize;
            InitGrid();
        }
    }
    public void Init(ShopManager mgr)
    {
        if (cellCount == 0)
            InitGrid();

        shopManager = mgr;
        cellCount = 0;
        scrollObject.anchoredPosition = new Vector2(0,0);
        for (int i = 0; i < maxCount; i++)
        {
            RectTransform rect = transform.GetChild(i).GetComponent<RectTransform>();
            rect.sizeDelta = cellSize;

            float x = padding.left + (cellCount % columnCount) * cellSize.x + (cellCount % columnCount) * spacing.x - alignSize;

            float y = padding.top + (cellCount / columnCount) * cellSize.y + (cellCount / columnCount) * spacing.y;

            rect.anchoredPosition = new Vector2(x, -y);


            if ( i >= shopManager.target_shop_list.Count )
            {
                rect.gameObject.SetActive(false);
            }
            else
            {
                rect.gameObject.SetActive(true);
                rect.GetComponent<ShopBTN>().SetInfo(mgr.target_shop_list[i], shopManager, search);
                rect.GetComponent<ShopBTN>().SetImg();
                SetSize();
            }

            cellCount++;
        }
    }
    void Set()
    {
        SetColumnCount();

        if (shopManager)
            Init(shopManager);

    }
    public GameObject AddCell(GameObject cell)
    {
        if (cellCount >= maxCount)
            return null;
        GameObject obj =  Instantiate(cell, this.transform);
        RectTransform rect = cell.GetComponent<RectTransform>();
        rect.sizeDelta = cellSize;

        float x = padding.left + (cellCount % columnCount) * cellSize.x + (cellCount % columnCount) * spacing.x - alignSize;
        
        float y = padding.top + (cellCount / columnCount) * cellSize.y + (cellCount / columnCount) * spacing.y;
        
        rect.anchoredPosition = new Vector2(x, - y);
        
        SetSize();
        
        cellCount++;

        return obj;
    }
    void SetSize()
    {
        float x = layoutRect.sizeDelta.x;
        float y = padding.top + ((cellCount / columnCount) +1) * cellSize.y + (cellCount / columnCount) * spacing.y + padding.bottom;

        layoutRect.sizeDelta = new Vector2(x,y);
    }
    public void UpdateLayout()
    {
        if (transform.childCount < columnCount)
            return;
        if(shopManager == null)
            return;

        RectTransform upRect = transform.GetChild(columnCount).GetComponent<RectTransform>();
        RectTransform downRect = transform.GetChild(maxCount - 1).GetComponent<RectTransform>();

        if (downRect.position.y > 0)
        {
            if (cellCount >= shopManager.target_shop_list.Count)
                return;

            for (int i = 0; i < columnCount; i++)
            {
                RectTransform targetRect = transform.GetChild(0).GetComponent<RectTransform>();

                float x = padding.left + (cellCount % columnCount) * cellSize.x + (cellCount % columnCount) * spacing.x - alignSize;

                float y = padding.top + (cellCount / columnCount) * cellSize.y + (cellCount / columnCount) * spacing.y;

                targetRect.anchoredPosition = new Vector2(x, -y);

                targetRect.SetAsLastSibling();

                SetSize();

                if (shopManager.target_shop_list != null)
                {
                    if (cellCount >= shopManager.target_shop_list.Count)
                    {
                        targetRect.gameObject.SetActive(false);
                    }
                    else
                    {
                        targetRect.gameObject.SetActive(true);

                        targetRect.GetComponent<ShopBTN>().SetInfo
                                (shopManager.target_shop_list[cellCount], shopManager, search);
                        targetRect.GetComponent<ShopBTN>().SetImg();
                    }
                }

                cellCount++;
            }
        }
        else if (cellCount > maxCount)
        {
            if (upRect.position.y < viewport.position.y)
            {
                for (int i = 0; i < columnCount; i++)
                {
                    RectTransform targetRect = transform.GetChild(maxCount - 1).GetComponent<RectTransform>();
                    
                    cellCount--;

                    int targetIndex = (cellCount - maxCount);
                    float x = padding.left + (targetIndex % columnCount) * cellSize.x + (targetIndex % columnCount) * spacing.x - alignSize;

                    float y = padding.top + (targetIndex / columnCount) * cellSize.y + (targetIndex / columnCount) * spacing.y;

                    targetRect.anchoredPosition = new Vector2(x, -y);

                    targetRect.gameObject.SetActive(true);

                    if (shopManager.target_shop_list != null)
                    {
                        targetRect.GetComponent<ShopBTN>().SetInfo
                            (shopManager.target_shop_list[targetIndex], shopManager, search);
                        targetRect.GetComponent<ShopBTN>().SetImg();
                    }

                    targetRect.SetAsFirstSibling();

                    SetSize();
                }
            }
        }

    }
}

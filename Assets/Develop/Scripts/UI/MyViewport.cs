﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyViewport : MonoBehaviour
{
    public RectTransform upper;
    public RectTransform footer;
    RectTransform rect;
    private void OnEnable()
    {
        ReSize();
    }
    public void ReSize()
    {
        rect = this.GetComponent<RectTransform>(); 
        // x : y = x' : y'
        // y' = x' * y / x
        Vector2 ratio = ApplicationManager.GetInstance().GetRatio();

        float width, height;

        width = ApplicationManager.GetInstance()._camera.pixelWidth;

        height = ApplicationManager.GetInstance()._camera.pixelHeight - upper.sizeDelta.y - footer.sizeDelta.y;

        rect.sizeDelta = new Vector2(width, height);

        rect.anchoredPosition = new Vector2(0, 0);
    }
}

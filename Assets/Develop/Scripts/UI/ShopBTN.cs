﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
public class ShopBTN : MonoBehaviour
{
    public enum Type
    {
        Main, Like, Search
    };
    [HideInInspector]
    public Shop shop;
    [HideInInspector]
    public Text title;
    [HideInInspector]
    public Text branch;

    public RawImage gradient;

    public Text discount;
    [HideInInspector]
    public BenefitImage img;

    public LikeBTN like;

    public Text new_indicator;

    [HideInInspector]
    public static int index = 0;
    [HideInInspector]
    public static float posY = 0;

    bool searchBTN = false;

    [HideInInspector]
    public ShopManager shopManager;

    public static void Init()
    {
        index = 0;
        posY = 0;
    }
    // Start is called before the first frame update
    void Start()
    {
        index++;
    }
    private void OnEnable()
    {
        if(shop != null)
        {
            if (like != null)
                like.SetSprite();
        }
    }
    void SetName()
    {
        title.text = shop._name;
    }
    void SetBranch()
    {
        branch.text = shop._branch;
    }
    public void SetImg()
    {
        img.LoadImage(shop._thumbnail);
    }
    void CheckNew()
    {
        if (ApplicationManager.GetInstance().ADMIN_MODE && shop._hide == true)
        {
            new_indicator.gameObject.SetActive(true);
            new_indicator.text = "HIDE";
            new_indicator.color = new Color(0,0,0,1);
            return;
        }
        if (shop.new_shop)
            new_indicator.gameObject.SetActive(true);
        else
            new_indicator.gameObject.SetActive(false);
    }
    void SetDiscount()
    {
        if (shop._discountType != "")
            discount.text = shop._discountType;
    }
    public void SetInfo(Shop shop_ref, ShopManager shopMgr, bool search = false)
    {
        gradient.GetComponent<RawImage>().enabled = false;
        shopManager = shopMgr;
        shop = shop_ref;
        
        int id = int.Parse(shop._id);
        if(shop._hide == false)
        {
            if (id < 0)
            {
                shop._hide = true;
                shop._id = Mathf.Abs(id).ToString();
            }
            else
                shop._hide = false;
        }

        SetName();
        SetBranch();
        SetDiscount();
        like.InitLike(shop);
        CheckNew();

        searchBTN = search;
    }
    public void Click()
    {
        ApplicationManager.GetInstance().ShopClick(this);

        if (searchBTN)
        {
            for (int i = 0; i < DataController.Instance.data.search_list.Count; i++)
            {
                if (DataController.Instance.data.search_list[i] == shop._id)
                    DataController.Instance.data.search_list.RemoveAt(i);
            }
            DataController.Instance.data.search_list.Add(shop._id);
            DataController.Instance.SaveData();
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Toast : MonoBehaviour
{
    const float TOAST_TIME = 1.2F;
    RawImage img;
    float alpha = 0.6f;
    public void Show(string msg)
    {
        this.transform.GetChild(0).GetComponent<Text>().text = msg;
        img = this.transform.GetComponent<RawImage>();
        alpha = 0.6f;
        img.color = new Color(0,0,0,alpha);
        StartCoroutine(ShowMessage());

    }
    IEnumerator ShowMessage()
    {
        float time = 0;
        while (time < TOAST_TIME)
        {
            time += Time.deltaTime;
            yield return null;
        }

        while(alpha > 0)
        {
            alpha -= Time.deltaTime * 5f;
            if (alpha < 0)
                alpha = 0;
            img.color = new Color(0, 0, 0, alpha);
            yield return null;
        }

        this.gameObject.SetActive(false);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MainPage : Page
{
    ApplicationManager instance;
    public enum PageType
    {
        Home = 1, Search = 2, Like = 3, Place = 4, MyPage = 5
    };
    public PageType rootPage = PageType.Home;
    public LikeManager likeManager;
    public GameObject footer;
    public GameObject[] pages = new GameObject[5];
    public MainPageBTN prevSelected;
    public MoveBar bar;
    public int page_index = 0;
    public Text place;

    // Start is called before the first frame update
    void Start()
    {
        instance = ApplicationManager.GetInstance();
        SetSpeed();
    }
    public override void Pulled()
    {
        move_time = 0;
        move = true;
        isOn = true;
        back = true;
        direction = RIGHT;
        targetX = 0;
        SetSpeed();
        if (rootPage == PageType.Like)
        {
            likeManager.ShowLikeShops();
        }
    }
    void ReSetSlideBanner(PageType type)
    {
        Transform obj = null;
        MyScrollRect scroll = null;
        switch (type)
        {
            case PageType.Home:
                obj = ApplicationManager.GetInstance().shopManager.transform.GetChild(0);
                scroll = ApplicationManager.GetInstance().shopManager.transform.parent.parent.GetComponent<MyScrollRect>();
                break;
            case PageType.Like:
                obj = likeManager.slideBanner;
                scroll = likeManager.transform.parent.parent.GetComponent<MyScrollRect>();
                break;
            case PageType.Search:
                scroll = ApplicationManager.GetInstance().searchManager.transform.GetChild(0).GetComponent<MyScrollRect>();
                break;
        }
        if(obj != null)
        {
            ApplicationManager.GetInstance().slide.transform.SetParent(obj);
            RectTransform rect = ApplicationManager.GetInstance().slide.GetComponent<RectTransform>();
            ApplicationManager.GetInstance().slide.vertical_content = obj.parent.GetComponent<RectTransform>();
            ApplicationManager.GetInstance().slide.viewport = obj.parent.parent.GetComponent<RectTransform>();
            ApplicationManager.GetInstance().slide.largeRect = scroll;
            rect.anchoredPosition = new Vector2(0, 0);
            ApplicationManager.GetInstance().slide.transform.SetSiblingIndex(0);
        }
    }
    public void ChangeMainPage(PageType type)
    {
        if (rootPage != type)
        {
            pages[(int)type - 1].SetActive(true);
            
            ReSetSlideBanner(type);
            pages[(int)rootPage - 1].SetActive(false);
            rootPage = type;

            switch(type)
            {
                case PageType.Place:
                    page_index = -1;
                    break;
                case PageType.MyPage:
                    page_index = -10;
                    break;
                default:
                    page_index = (int)(type) - 1;
                    break;
            }


            instance.page_index = page_index;
        }
    }
}

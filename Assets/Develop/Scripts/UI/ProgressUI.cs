﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressUI : MonoBehaviour
{
    protected RectTransform rect;
    public float amount;
    float timeOfTravel = 2; //time after object reach a target place 
    float currentTime = 0; // actual floting time 
    float normalizedValue;
    protected Coroutine co = null;
     
    protected void Start()
    {
        amount = 0;
        rect = this.GetComponent<RectTransform>();
    }
    virtual public void Progress(float progress)
    {
    }
    protected IEnumerator Lerp(Vector2 oldPos, Vector2 newPos)
    {
        while (currentTime <= timeOfTravel)
        {
            currentTime += Time.deltaTime;
            normalizedValue = currentTime / timeOfTravel; // we normalize our time 

            rect.anchoredPosition = Vector3.Lerp(oldPos, newPos, normalizedValue);
            yield return null;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AuthBTN : MonoBehaviour
{
    Shop shop;

    public Sprite visible;
    public Sprite invisible;
    public void SetBTN(Shop ref_shop)
    {
        if (!ApplicationManager.GetInstance().ADMIN_MODE)
        {
            this.gameObject.SetActive(false);
            return;
        }
        shop = ref_shop;

        if (shop._hide)
            this.GetComponent<Image>().sprite = invisible;
        else
            this.GetComponent<Image>().sprite = visible;
    }
    public void Click()
    {
        if (shop._hide)
            ApplicationManager.GetInstance().ShowTwoBtnDialog("페이지 활성화",
                "확인 버튼을 누르면 유저들이 페이지를 볼 수 있게됩니다. 정말로 활성화 하시겠습니까?",
                "확인","돌아가기", Switch, null);
        else
            ApplicationManager.GetInstance().ShowTwoBtnDialog("페이지 비활성화",
                "확인 버튼을 누르면 유저들이 페이지를 볼 수 없게됩니다. 정말로 비활성화 하시겠습니까?",
                "확인", "돌아가기", Switch, null);
    }
    void Switch()
    {
        shop._hide = !shop._hide;
        ApplicationManager.GetInstance().ShopUpdate(shop._id, shop._hide);
        if (shop._hide)
            this.GetComponent<Image>().sprite = invisible;
        else
            this.GetComponent<Image>().sprite = visible;
    }
    void Nothing()
    { }
}

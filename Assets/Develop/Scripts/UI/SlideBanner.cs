﻿using System;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityEngine.UI
{
    [SelectionBase]
    [ExecuteInEditMode]
    [DisallowMultipleComponent]
    [RequireComponent(typeof(RectTransform))]
    public class SlideBanner : MyScrollRect
    {
        private bool begin = false;

        public RectTransform vertical_content;
        public RectTransform horizontal_content;
        public MyScrollRect largeRect;

        private void SetBanners()
        {}
        public RectTransform left;
        public RectTransform right;
        public RectTransform center;
        private RectTransform rect;
        bool move = false;
        float target = 0;
        public float timer = 0.0f;
        const float SLIDE_TIME = 5.0f;
        public override void OnBeginDrag(PointerEventData eventData)
        {
            begin = true;
            // 터치 좌표 변화값
            Vector2 localCursor;
            if (RectTransformUtility.ScreenPointToLocalPointInRectangle(viewRect, eventData.position, eventData.pressEventCamera, out localCursor))

            if (eventData.button != PointerEventData.InputButton.Left)
                return;

            if (!IsActive())
                return;

            UpdateBounds();

            m_PointerStartLocalCursor = Vector2.zero;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(viewRect, eventData.position, eventData.pressEventCamera, out m_PointerStartLocalCursor);

            m_ContentStartPosition = m_Content.anchoredPosition;
            m_Dragging = true;
        }
        private void Update()
        {
            timer += Time.deltaTime;
            if(timer > SLIDE_TIME)
            {
                SlideToRight();
                timer = 0;
            }
        }
        public void SlideToRight()
        {
            m_Content = horizontal_content;
            move = true;
            m_Horizontal = true;
            m_Vertical = false;
            m_Dragging = false;
            float sizeX = 1080f;
            target = -sizeX;
            ApplicationManager.GetInstance().bannerManager.SetCountUp();
        }
        public override void OnDrag(PointerEventData eventData)
        {
            largeRect.enabled = false;
            timer = 0.0f;
            if (eventData.button != PointerEventData.InputButton.Left)
                return;

            if (!IsActive())
                return;

            Vector2 localCursor;
            if (!RectTransformUtility.ScreenPointToLocalPointInRectangle(viewRect, eventData.position, eventData.pressEventCamera, out localCursor))
                return;

            UpdateBounds();

            var pointerDelta = localCursor - m_PointerStartLocalCursor;

            if ((begin == true) && ((pointerDelta.x != 0) || (pointerDelta.y != 0)))
            {
                if(move)
                {
                    m_Horizontal = true;
                }
                else
                {
                    float _x = Mathf.Abs(pointerDelta.x);
                    float _y = Mathf.Abs(pointerDelta.y);
                    if (_x > _y)
                    {
                        m_Horizontal = true;
                        m_Vertical = false;
                        m_Content = horizontal_content;
                    }
                    else
                    {
                        m_Vertical = true;
                        m_Horizontal = false;
                        m_Content = vertical_content;
                    }
                }

                begin = false;
            }
            //pointerDelta *= m_ScrollFactor;    //이 줄은 새로운 것입니다!
            Vector2 position = m_ContentStartPosition + pointerDelta;

            // Offset to get content into place in the view.
            Vector2 offset = CalculateOffset(position - m_Content.anchoredPosition);
            position += offset;
            
            /////////////////////////////////
            if (m_HorizontalMovementType == MovementType.Elastic)
            {
                if (offset.x != 0)
                    position.x = position.x - RubberDelta(offset.x, m_ViewBounds.size.x);
            }
            /////////////////////////////////
            if (m_VerticalMovementType == MovementType.Elastic)
            {
                if (offset.y != 0)
                    position.y = position.y - RubberDelta(offset.y, m_ViewBounds.size.y);
            }

            SetContentAnchoredPosition(position);
        }
        public override void OnEndDrag(PointerEventData eventData)
        {
            largeRect.enabled = true;

            if (eventData.button != PointerEventData.InputButton.Left)
                return;
            
            if(m_Horizontal == true)
            {
                float sizeX = 1080f;
                float bound = sizeX * 0.2f;

                target = 0;

                move = true;
                m_Dragging = false;

                if (ApplicationManager.GetInstance().bannerManager.GetCount() == 1)
                    return;

                if (m_Content.anchoredPosition.x <= (-bound))
                {
                    target = -sizeX;
                    ApplicationManager.GetInstance().bannerManager.SetCountUp();
                }
                else if (m_Content.anchoredPosition.x >= (bound))
                {
                    target = sizeX;
                    ApplicationManager.GetInstance().bannerManager.SetCountDown();
                }
            }
            else if(m_Vertical == true)
            {
                base.OnEndDrag(eventData);
            }
        }

        protected override void LateUpdate()
        {
            if (!m_Content)
                return;

            float gap = 50.0f;

            EnsureLayoutHasRebuilt();
            UpdateScrollbarVisibility();
            UpdateBounds();
            float deltaTime = Time.unscaledDeltaTime;
            Vector2 offset = CalculateOffset(Vector2.zero);
            if (!m_Dragging)
            {
                Vector2 position = m_Content.anchoredPosition;
                float x = Mathf.Abs(target - m_Content.anchoredPosition.x);
                if((x < gap) && (move))
                {
                    m_Content.anchoredPosition = new Vector2(0,0);
                    m_Velocity[0] = 0;
                    move = false;
                    ApplicationManager.GetInstance().bannerManager.SetBannerInfos();
                }
                // Apply spring physics if movement is elastic and content has an offset from the view.
                else if ((m_HorizontalMovementType == MovementType.Elastic) && move)
                {
                    float speed = m_Velocity[0];
                    position[0] = Mathf.SmoothDamp(m_Content.anchoredPosition[0], target, ref speed, m_Elasticity, Mathf.Infinity, deltaTime);
                    m_Velocity[0] = speed;
                }
                // Else move content according to velocity with deceleration applied.
                else if (m_Inertia)
                {
                    m_Velocity[0] *= Mathf.Pow(m_DecelerationRate, deltaTime);
                    if (Mathf.Abs(m_Velocity[0]) < 1)
                        m_Velocity[0] = 0;
                    position[0] += m_Velocity[0] * deltaTime;
                }
                // If we have neither elaticity or friction, there shouldn't be any velocity.
                else
                {
                    m_Velocity[0] = 0;
                }

                if (m_Velocity != Vector2.zero)
                {
                    if (m_HorizontalMovementType == MovementType.Clamped)
                    {
                        offset = CalculateOffset(position - m_Content.anchoredPosition);
                        position.x += offset.x;
                    }

                    if (m_VerticalMovementType == MovementType.Clamped)
                    {
                        offset = CalculateOffset(position - m_Content.anchoredPosition);
                        position.y += offset.y;
                    }

                    SetContentAnchoredPosition(position);
                }
            }

            if (m_Dragging && m_Inertia)
            {
                Vector3 newVelocity = (m_Content.anchoredPosition - m_PrevPosition) / deltaTime;
                m_Velocity = newVelocity; //Vector3.Lerp(m_Velocity, newVelocity, deltaTime * 10);
            }
            if (m_ViewBounds != m_PrevViewBounds || m_ContentBounds != m_PrevContentBounds || m_Content.anchoredPosition != m_PrevPosition)
            {
                UpdateScrollbars(offset);
                m_OnValueChanged.Invoke(normalizedPosition);
                UpdatePrevData();
            }
        }
    }
}
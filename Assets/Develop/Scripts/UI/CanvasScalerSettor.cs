﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasScalerSettor : MonoBehaviour
{
    RectTransform rect;
    CanvasScaler canvas;
    ApplicationManager appMgr;
    const float MAX_RATIO = 0.68f;
    float ratio;
    // Start is called before the first frame update
    void Start()
    {
        rect = this.GetComponent<RectTransform>();
        canvas = this.GetComponent<CanvasScaler>();
        appMgr = ApplicationManager.GetInstance();
        ratio = appMgr._screen.x / appMgr._screen.y;
        SetScaler();
    }
    private void Update()
    {
        float new_ratio = appMgr._screen.x / appMgr._screen.y;
        if (new_ratio != ratio)
        {
            SetScaler();
            ratio = new_ratio;
        }
    }
    void SetScaler()
    {
        if (ratio > MAX_RATIO)
            canvas.screenMatchMode = CanvasScaler.ScreenMatchMode.Expand;
        else
            canvas.screenMatchMode = CanvasScaler.ScreenMatchMode.MatchWidthOrHeight;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LikeBTN : MonoBehaviour
{
    public Sprite likeSprite;
    public Sprite unlikeSprite;

    Shop shop = null;
    bool first_like = false;
    
    public void InitLike(Shop target)
    {
        first_like = target._like;

        SetLike(target);
    }
    public void SetSprite()
    {
        if (shop._like == true)
            this.transform.GetChild(0).GetComponent<Image>().sprite = likeSprite;
        else
            this.transform.GetChild(0).GetComponent<Image>().sprite = unlikeSprite;
    }
    void SetLike(Shop target)
    {
        shop = target;

        SetSprite();
    }

    public void ToggleLike()
    {
        shop._like = !(shop._like);
        SetLike(shop);
        FinalSave();
    }
    public void FinalSave()
    {
        if (shop == null)
            return;

        if (first_like == true)
        {
            Debug.Log(DataController.Instance.data.like_list.IndexOf(shop._id) + "of " + DataController.Instance.data.like_list.Count);
            DataController.Instance.data.like_list.RemoveAt(
               DataController.Instance.data.like_list.IndexOf(shop._id));
        }
        else
        {
            DataController.Instance.data.like_list.Add(shop._id);
        }
        first_like = shop._like;
        DataController.Instance.SaveData();
    }
}

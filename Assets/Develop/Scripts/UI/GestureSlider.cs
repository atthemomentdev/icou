﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GestureSlider : MonoBehaviour
{
    public RectTransform frontPage;
    public RectTransform backPage;
    float GESTURE_SECTION = 100f;
    [HideInInspector]
    public bool gesture;

    Vector3 mouseStart;
    Vector2 startPos;
    Vector2 startPos2;
    bool outMaxRatio = false;
    private void Start()
    {
        float ratio = ApplicationManager.GetInstance()._screen.x / ApplicationManager.GetInstance()._screen.y;
        float MAX_RATIO = 0.68f;
        if (ratio > MAX_RATIO)
            outMaxRatio = true;
        else
            outMaxRatio = false;
    }
    public void OnMouseDown()
    {
        Vector2 mouse = Input.mousePosition;
        mouseStart = mouse;
        if (!outMaxRatio)
            mouse /= ApplicationManager.GetInstance().GetRatio().x;

        if (mouse.x < GESTURE_SECTION)
            gesture = true;
        else
            gesture = false;
    }
    public void OnBeginDrag()
    {
        if(gesture)
        {
            Vector2 mouse = Input.mousePosition;

            float x = Mathf.Abs(mouse.x - mouseStart.x);

            float y = Mathf.Abs(mouse.y - mouseStart.y);

            if (x >= y)
            {
                gesture = true;
                startPos = frontPage.anchoredPosition;
                startPos2 = backPage.anchoredPosition;
            }
            else
                gesture = false;
        }
    }
    float speedThreahold = 30f;
    float speedX;
    public void OnDrag()
    {
        if(gesture)
        {
            if (Input.mousePosition.x < 0)
            {
                frontPage.anchoredPosition = new Vector2(startPos.x, startPos.y);
                backPage.anchoredPosition = new Vector2(startPos2.x, startPos2.y);
                return;
            }
            Vector2 mouse = (Input.mousePosition - mouseStart);

            float ratio = ApplicationManager.GetInstance()._screen.x / ApplicationManager.GetInstance()._screen.y;

            float MAX_RATIO = 0.68f;
            if (ratio <= MAX_RATIO)
                mouse /= ApplicationManager.GetInstance().GetRatio().x;

            Vector2 prevPos = frontPage.anchoredPosition;

            float pos1 = startPos.x + mouse.x;

            if (ratio > MAX_RATIO)
            {
                if(pos1 > ApplicationManager.GetInstance()._screen.x)
                    pos1 = ApplicationManager.GetInstance()._screen.x;
            }
            else if (pos1 > 1080f)
                pos1 = 1080f;

            frontPage.anchoredPosition = new Vector2(pos1, startPos.y);

            float pos2 = startPos2.x + mouse.x * 0.3f;

            if (pos2 > 0)
                pos2 = 0;

            backPage.anchoredPosition = new Vector2(pos2, startPos2.y);
            speedX = (frontPage.anchoredPosition.x - prevPos.x);
        }
    }
    public void OnEndDrag()
    {
        if (gesture)
        {
            if(speedX > speedThreahold)
                frontPage.GetComponent<Page>().Push();
            else
            {
                if(frontPage.anchoredPosition.x > ApplicationManager.GetInstance().canvas.sizeDelta.x / 2)
                    frontPage.GetComponent<Page>().Push();
                else
                {
                    frontPage.GetComponent<Page>().reStart = true;
                    frontPage.GetComponent<Page>().Pull();
                }
            }
        }
    }
}

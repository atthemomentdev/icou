﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubWindow : MonoBehaviour
{
    public RectTransform frontPage;
    
    float moveThreshold = 30f;
    float speedThreshold = -30f;
    float speedY;

    Vector3 mouseStart;
    Vector2 startPos;
    bool outMaxRatio = false;
    bool gesture = false;
    public float target_y;
    Vector2 target;
    public delegate void callbackFunc();

    private void OnEnable()
    {
        Show(VOID);
    }
    void VOID()
    {

    }

    void SetTarget()
    {
        float ratio = ApplicationManager.GetInstance()._screen.x / ApplicationManager.GetInstance()._screen.y;
        float MAX_RATIO = 0.68f;

        if (ratio > MAX_RATIO)
            outMaxRatio = true;
        else
            outMaxRatio = false;

        if (outMaxRatio == false)
        {
            float y = ApplicationManager.GetInstance()._screen.y - ApplicationManager.GetInstance().notchUp;
            y = y * 1080 / ApplicationManager.GetInstance()._screen.x;

            float scaleY = y / 1920f;

            target = new Vector2(0, target_y * scaleY);

            frontPage.sizeDelta = new Vector2(frontPage.sizeDelta.x, target.y);
        }
        else
        {
            float y = ApplicationManager.GetInstance()._screen.y - ApplicationManager.GetInstance().notchUp;

            float scaleY = y / 1920f;

            target = new Vector2(0, target_y * scaleY);

            frontPage.sizeDelta = new Vector2(frontPage.sizeDelta.x, target.y);
        }

    }
    public void MouseStart()
    {
        gesture = false;
        mouseStart = Input.mousePosition;
        if (!outMaxRatio)
          mouseStart /= ApplicationManager.GetInstance().GetRatio().x;
    }

    public void OnBeginDrag()
    {        
        startPos = frontPage.anchoredPosition;
    }
    public void OnDrag()
    {
        Vector2 currMouse = Input.mousePosition;
        if (!outMaxRatio)
            currMouse /= ApplicationManager.GetInstance().GetRatio().x;

        float y = startPos.y + (currMouse.y - mouseStart.y);

        float prevPosY = frontPage.anchoredPosition.y;

        if (y < target.y)
            frontPage.anchoredPosition = new Vector2(0, y);
        else
            frontPage.anchoredPosition = target;
        
        speedY = (y - prevPosY);

        if (Mathf.Abs(mouseStart.y - currMouse.y) > moveThreshold)
            gesture = true;
    }
    public void OnEndDrag()
    {
        if ((gesture == false) || (speedY < speedThreshold))
            Close();
        else
        {
            if (frontPage.anchoredPosition.y < ApplicationManager.GetInstance().canvas.sizeDelta.y / 2)
                Close();
            else
                Show(VOID);
        }
    }
    public void Show(callbackFunc function)
    {
        SetTarget();
        StartCoroutine(Move(target, function));
    }

    public void Close()
    {
        StartCoroutine(Move(new Vector2(0, 0), DisavbleObject));

        // 다른곳에서도 sub창 쓰면 나중에는 PAGE INDEX를 상수말고 변수로!
        ApplicationManager.GetInstance().page_index = 3;
    }
    public void DisavbleObject()
    {
        frontPage.GetComponent<RectTransform>().anchoredPosition = new Vector2(0,0);
        this.gameObject.SetActive(false);
    }

    float move_time;
    float moveSpeed;
    float a;
    const float TIME = 0.1F;

    void SetSpeed(float startY, float targetY)
    {
        move_time = 0;
        startPos = this.GetComponent<RectTransform>().anchoredPosition;
        float d = Mathf.Abs(targetY - startY);
        moveSpeed = 2 * d / TIME;
        a = -moveSpeed / TIME;
    }

    private IEnumerator Move(Vector2 target, callbackFunc func)
    {
        RectTransform rect = frontPage.GetComponent<RectTransform>();
        float startY = rect.anchoredPosition.y;
        float direction = 1;

        if (startY > target.y)
            direction = -1;

        SetSpeed(startY, target.y);
        

        while (move_time < TIME)
        {
            float y = startY + (moveSpeed * move_time + a * (move_time) * (move_time) / 2) * direction;

            move_time += Time.deltaTime;

            rect.anchoredPosition = new Vector2(0,y);
            yield return null;
        }
        rect.anchoredPosition = target;
        func();
    }

}

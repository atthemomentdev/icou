﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingPage : Page
{
    public GameObject notification;
    CanvasGroup canvas;
    private void Start()
    {
        canvas = this.GetComponent<CanvasGroup>();
        SetSpeed();
        this.GetComponent<Canvas>().enabled = true;
    }
    public override void Push()
    {
        StartCoroutine(FadeOut());
    }

    public void SetNotification(string info) {
        notification.SetActive(true);
        notification.transform.GetChild(0).GetComponent<Text>().text = info;
    }
    float fadeSpeed = 2f;
    IEnumerator FadeOut()
    {
        while(canvas.alpha > 0.1f)
        {
            canvas.alpha -= fadeSpeed * Time.deltaTime;
            yield return null;
        }

        this.gameObject.SetActive(false);
    }
}
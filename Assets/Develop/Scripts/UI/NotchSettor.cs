﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotchSettor : MonoBehaviour
{
    public bool up = true;
    private void Update()
    {
#if !UNITY_IOS
        Vector2 scale = this.transform.GetComponent<RectTransform>().sizeDelta;
        if (up)
            this.transform.GetComponent<RectTransform>().sizeDelta = new Vector2 (scale.x, ApplicationManager.GetInstance().notchUp);
        else
            this.transform.GetComponent<RectTransform>().sizeDelta = new Vector2(scale.x, ApplicationManager.GetInstance().notchDown);
#endif
    }
}

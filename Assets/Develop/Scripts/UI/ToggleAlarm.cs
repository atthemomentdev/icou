﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_ANDROID && !UNITY_EDITOR
using DeadMosquito.AndroidGoodies;
#elif UNITY_IOS && !UNITY_EDITOR
using DeadMosquito.IosGoodies;
#endif

public class ToggleAlarm : ToggleBTN
{
    protected override void InitToggle()
    {
        DataController.Instance.LoadData();
        toggle = DataController.Instance.data.pushAlarm;
        available = true;
#if UNITY_ANDROID && !UNITY_EDITOR

#elif UNITY_IOS && !UNITY_EDITOR

#endif
    }
    protected override void ToggleON()
    {
        DataController.Instance.data.pushAlarm = toggle;
        DataController.Instance.SaveData();
    }
    protected override void ToggleOFF()
    {
        DataController.Instance.data.pushAlarm = toggle;
        DataController.Instance.SaveData();
    }
}

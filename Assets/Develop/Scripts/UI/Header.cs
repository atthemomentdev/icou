﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Header : MonoBehaviour
{
    private Vector2 HIDE = new Vector3(0,  1);
    private Vector2 SHOW = new Vector3(0, -1);
    private const float SPEED = 800.0f;
    private Vector2 TOP;
    private Vector2 BOT;
    private Vector2 STOP = new Vector3(0, 0);
    private Vector2 direction;
    private Vector2 target;
    private float alpha = 0.03f;

    private float prevY = 0;
    private bool active = false;
    private RectTransform rect;

    public GestureSlider gestureOn;
    float y = 0;
    // Start is called before the first frame update
    void Start()
    {
        direction = STOP;
        rect = this.transform.GetComponent<RectTransform>();

        y = rect.sizeDelta.y;

        rect.sizeDelta = new Vector2(rect.sizeDelta.x, y + ApplicationManager.GetInstance().notchUp);

        TOP = new Vector2(0, rect.sizeDelta.y);
        BOT = new Vector2(0, 0);
    }

    // Update is called once per frame
    void Update()
    {
        RectTransform rect = this.transform.GetComponent<RectTransform>();
        TOP = new Vector2(0, rect.sizeDelta.y);

        rect.sizeDelta = new Vector2(rect.sizeDelta.x, y + ApplicationManager.GetInstance().notchUp);
        if (active)
        {
            this.transform.Translate(direction * SPEED * Time.deltaTime);
            this.GetComponent<CanvasGroup>().alpha += alpha;

            if ((direction == HIDE) && (rect.anchoredPosition.y > target.y))
                Stop();
            else if ((direction == SHOW) && (rect.anchoredPosition.y < target.y))
                Stop();
        }
    }
    private void Move()
    {
        active = true;
    }
    private void Stop()
    {
        active = false;
        rect.anchoredPosition = target;
        if (direction == HIDE)
            this.GetComponent<CanvasGroup>().alpha = 0.0f;
        else if (direction == SHOW)
            this.GetComponent<CanvasGroup>().alpha = 1.0f;
        direction = STOP;
    }
    public void Check()
    {
        if (gestureOn != null)
        {
            if(gestureOn.gesture == true)
                return;
        }

        float mouseY = Input.mousePosition.y;

        if ((active == false) && (Mathf.Abs(prevY - mouseY) > 10.0f))
        {
            if (prevY > mouseY)
                Show();
            else if (prevY < mouseY)
                Hide();

            Move();
        }

        prevY = mouseY;
    }
    public void MouseOn()
    {
        if (gestureOn != null)
        {
            if (gestureOn.gesture == true)
                return;
        }

        prevY = Input.mousePosition.y;
    }
    private void Hide()
    {
        direction = HIDE;
        target = TOP;
        alpha = -0.03f;
    }
    private void Show()
    {
        direction = SHOW;
        target = BOT;
        alpha = 0.03f;
    }
    public void RePosition()
    {
        active = false;
        rect.anchoredPosition = BOT;
        this.GetComponent<CanvasGroup>().alpha = 1.0f;
        direction = STOP;
    }
}

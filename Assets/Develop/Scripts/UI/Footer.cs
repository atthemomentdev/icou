﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Footer : MonoBehaviour
{
    float y;
    private void Start()
    {
        RectTransform rect = this.transform.GetComponent<RectTransform>();


        y = rect.sizeDelta.y;
        rect.sizeDelta = new Vector2(rect.sizeDelta.x, rect.sizeDelta.y + ApplicationManager.GetInstance().notchDown);
    }

    private void Update()
    {
        RectTransform rect = this.transform.GetComponent<RectTransform>();

        rect.sizeDelta = new Vector2(rect.sizeDelta.x, y + ApplicationManager.GetInstance().notchDown);
    }
}

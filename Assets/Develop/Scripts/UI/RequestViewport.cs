﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RequestViewport : MonoBehaviour
{
    RectTransform rect;
    Camera _camera;
    float prevY = 0;
    int index = 0;
    // Start is called before the first frame update
    void Start()
    {
        rect = this.GetComponent<RectTransform>();
        _camera = ApplicationManager.GetInstance()._camera;
        prevY = ApplicationManager.GetInstance()._screen.y;
    }
    public void SetIndex(int i)
    {
        index = i;
    }
    // Update is called once per frame
    void Update()
    {
        if(rect != null)
        {
            if(_camera != null)
            {
                float keyBoardY = TouchScreenKeyboard.area.height;
                rect.sizeDelta = new Vector2(ApplicationManager.GetInstance()._screen.x, (ApplicationManager.GetInstance()._screen.y - keyBoardY));
                if (keyBoardY < 10)
                    index = 0;
                if(prevY != rect.sizeDelta.y)
                {
                    if (index > 0)
                    {
                        this.GetComponent<MyScrollRect>().content.anchoredPosition =
                                 new Vector2(this.GetComponent<MyScrollRect>().content.anchoredPosition.x,
                                             -this.GetComponent<MyScrollRect>().content.GetChild(0).GetChild(index).GetComponent<RectTransform>().anchoredPosition.y);
                    }
                    else
                        this.GetComponent<MyScrollRect>().verticalNormalizedPosition = 1;
                    prevY = rect.sizeDelta.y;
                }
            }
            else
                _camera = ApplicationManager.GetInstance()._camera;
        }
        else
            rect = this.GetComponent<RectTransform>();
        
    }
}

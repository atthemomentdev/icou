﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SearchBTN : ShopBTN
{
    void Start()
    {
    }

    public void SearchedClick()
    {
        ApplicationManager.GetInstance().ShopClick(this);

        for (int i = 0; i < DataController.Instance.data.search_list.Count; i ++)
        {
            if (DataController.Instance.data.search_list[i] == shop._id)
            {
                DataController.Instance.data.search_list.RemoveAt(i);
                break;
            }
        }
        DataController.Instance.data.search_list.Add(shop._id);
        DataController.Instance.SaveData();

        ApplicationManager.GetInstance().searchManager.record.GetComponent<Search>().ShowRecordedBTN();
    }

    public void SetInfo(Shop shop_ref)
    {
        shop = shop_ref;
        SetName();
        SetBranch();
        SetImg();
        index++;
    }
    void SetName()
    {
        title.text = shop._name;
    }
    void SetBranch()
    {
        branch.text = shop._branch;
    }
    public void RemoveFromSearchList()
    {
        int index = DataController.Instance.data.search_list.IndexOf(shop._id);
        DataController.Instance.data.search_list.RemoveAt(index);
        DataController.Instance.SaveData();
        GameObject.Destroy(this.gameObject);
    }
}

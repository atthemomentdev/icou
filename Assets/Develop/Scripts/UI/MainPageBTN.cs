﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainPageBTN : MonoBehaviour
{
    public MainPage.PageType type;
    public Sprite selected;
    public Sprite unSelected;
    
    public void Click()
    {
        if (ApplicationManager.GetInstance().mainPage.prevSelected != null)
            ApplicationManager.GetInstance().mainPage.prevSelected.UnSelected();

        this.transform.GetChild(0).GetComponent<Image>().sprite = selected;
        ApplicationManager.GetInstance().mainPage.ChangeMainPage(type);
        ApplicationManager.GetInstance().mainPage.prevSelected = this;

        ApplicationManager.GetInstance().mainPage.bar.Move(this.GetComponent<RectTransform>().anchoredPosition.x);
        //this.transform.GetChild(1).GetComponent<Text>().color = ApplicationManager.selectedColor;
        //this.transform.GetChild(1).GetComponent<Text>().fontStyle = FontStyle.Bold;
    }
    public void UnSelected()
    {
        this.transform.GetChild(0).GetComponent<Image>().sprite = unSelected;
        //this.transform.GetChild(1).GetComponent<Text>().color = ApplicationManager.unSelectedColor;
        //this.transform.GetChild(1).GetComponent<Text>().fontStyle = FontStyle.Normal;
    }
}

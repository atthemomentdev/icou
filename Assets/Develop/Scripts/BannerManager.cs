﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BannerManager : MonoBehaviour
{
    [SerializeField]
    public GameObject bannerBTN;
    [SerializeField]
    public GameObject bannerCircle;
    [SerializeField]
    public GameObject bannerContents;

    public List<BannerInfo> banner_list;
    
    public Transform circles;
    public Transform banners;
    public Transform content_parent;
    private int count;

    private int current_index = 0;
    
    private float circle_gap = 30.0f;


    // Start is called before the first frame update
    void Start()
    {
        circle_gap = 30.0f;
    }
    public int GetCount()
    {
        return count;
    }
    // Update is called once per frame
    void Update()
    { }

    public void MakeBannerLists()
    {
        if (banner_list == null)
            banner_list = new List<BannerInfo>();

        BannerInfo new_banner;
        foreach (var info in ApplicationManager.GetInstance().banners)
        {
            string[] tmp = info.Split('#');
            if (tmp.Length < 5)
            {
                return;
            }
            new_banner = new BannerInfo();

            new_banner.SetBannerInfo(tmp[0],tmp[1],tmp[2],tmp[3],tmp[4]);

            banner_list.Add(new_banner);
        }
    }
    public void LoadCurrentBanner()
    {
        LoadContentsImage();
    }

    public void LoadContentsImage()
    {
        foreach (var contents in banner_list[current_index]._content_img_list)
        {
            BannerContents new_content = Instantiate(bannerContents, content_parent).GetComponent<BannerContents>();
            new_content.SetTextureContainer(contents);
        }
        ApplicationManager.GetInstance().bannerManager.content_parent.GetChild(0).GetComponent<BannerContents>().LoadTexture();
    }

    public void ClearBanner()
    {
        banner_list[current_index].ClearContents();
        content_parent.GetComponent<RectTransform>().sizeDelta = new Vector2(ApplicationManager.GetInstance()._screen.x,0);
    }

    public void CreateBannerCircle()
    {
        for (int i = 0; i < count; i++)
        {
            GameObject circle = Instantiate(bannerCircle, circles.transform);
        }
        circles.GetComponent<RectTransform>().sizeDelta = new Vector2(count * circle_gap, circles.GetComponent<RectTransform>().sizeDelta.y);
        SetCirclesColor();
    }

    public void LoadBanners()
    {
        count = banner_list.Count;

        Vector2 scale = banners.GetChild(0).GetComponent<RectTransform>().sizeDelta;
        Vector2 ratio = ApplicationManager.GetInstance().GetRatio();
        
        if (count == 1)
        {
            GameObject.Destroy(banners.GetChild(0).gameObject);
            GameObject.Destroy(banners.GetChild(2).gameObject);

            banners.GetComponent<RectTransform>().sizeDelta = scale;
        }
        else
            banners.GetComponent<RectTransform>().sizeDelta = new Vector2(scale.x * 3 , scale.y);
        CreateBannerCircle();
        SetBannerInfos();
    }

    public void SetCountUp()
    {
        current_index++;
        if (current_index > count - 1)
            current_index = 0;
        SetCirclesColor();
    }
    private void SetCirclesColor()
    {
        for (int i = 0; i < count; i++)
        {
            if (i == current_index)
                circles.transform.GetChild(i).GetComponent<RawImage>().color = new Color(1, 1, 1);
            else
                circles.transform.GetChild(i).GetComponent<RawImage>().color = new Color(0.7f, 0.7f, 0.7f);
        }
    }
    public void SetCountDown()
    {
        current_index--;
        if (current_index < 0)
            current_index = count - 1;
        SetCirclesColor();
    }
    public void SetBannerInfos()
    {
        int prev = current_index -1;
        if (prev < 0)
        {
            if (count > 0)
                prev = count - 1;
            else
                prev = 0;
        }

        int next = current_index + 1;
        if (next > (count - 1))
            next = 0;


        if (count == 1)
        {
            if(banners.childCount > 1)
                banners.GetChild(1).GetComponent<Banner>().SetBannerInfo(banner_list[current_index]);
        }
        else
        {
            if(count == 0)
            {
                
            }
            else if(count == 2)
            {
                banners.GetChild(0).GetComponent<Banner>().SetBannerInfo(banner_list[next]);
                banners.GetChild(1).GetComponent<Banner>().SetBannerInfo(banner_list[current_index]);
                banners.GetChild(2).GetComponent<Banner>().SetBannerInfo(banner_list[next]);
            }
            else
            {
                banners.GetChild(0).GetComponent<Banner>().SetBannerInfo(banner_list[prev]);
                banners.GetChild(1).GetComponent<Banner>().SetBannerInfo(banner_list[current_index]);
                banners.GetChild(2).GetComponent<Banner>().SetBannerInfo(banner_list[next]);
            }
        }
    }
}

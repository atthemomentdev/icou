﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CategoryManager : MonoBehaviour
{
    private Text curr_cat;
    [SerializeField]
    public GameObject categoryBtn;
    
    List<GameObject> categories = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
    }

    public void SetCategories(List<string> categories)
    {
        foreach (var cat in categories)
        {
            CreateCategory(cat);
        }
        SetDefaultCategory();
    }
    void CreateCategory(string str)
    {
        switch (str)
        {
            case "RESTAURANT":
                str = "밥";
                break;
            case "CAFE":
                str = "카페";
                break;
            case "DRINK":
                str = "술집";
                break;
            case "LIFE":
                str = "라이프";
                break;
        }
        GameObject obj = Instantiate(categoryBtn, this.transform);
        categories.Add(obj);
        obj.GetComponent<CategoryBTN>().SetCategory(str);
    }

    public void SetDefaultCategory()
    {
        ChangeCategory(this.categories[0].GetComponent<CategoryBTN>());
    }
    public void ChangeCategory(CategoryBTN btn)
    {
        string cat = btn.transform.GetChild(0).GetComponent<Text>().text.ToString();

        if(curr_cat == null)
            curr_cat = btn.transform.GetChild(0).GetComponent<Text>();
        else if (curr_cat.text == cat)
        {
            this.transform.parent.gameObject.SetActive(false);
            return;
        }


        curr_cat.color = new Color(0.5f, 0.5f, 0.5f, 1);
        curr_cat.fontStyle = FontStyle.Normal;
        curr_cat.transform.parent.GetComponent<Image>().color = new Color(1f, 1f, 1f, 1f);

        curr_cat = btn.transform.GetChild(0).GetComponent<Text>();

        curr_cat.color = new Color(1, 1, 1, 1);
        curr_cat.fontStyle = FontStyle.Bold;
        curr_cat.transform.parent.GetComponent<Image>().color = ApplicationManager.yellow;

        curr_cat.text = cat;

        RectTransform rect = btn.transform.GetComponent<RectTransform>();


        ApplicationManager.GetInstance().shopManager.ChangeCategory(cat);

        CategoryBTN.Init();


        this.transform.parent.gameObject.SetActive(false);
    }
}

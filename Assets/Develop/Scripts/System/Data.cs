﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Data
{
    public string placeId;

    [HideInInspector]
    public List<string> like_list = new List<string>();

    [HideInInspector]
    public List<string> check = new List<string>();

    [HideInInspector]
    public List<string> search_list = new List<string>();

    [HideInInspector]
    public bool pushAlarm = true;

    [HideInInspector]
    public bool readNotification = false;

    void SetCheck()
    {
        check.CopyTo(like_list.ToArray());
    }
}
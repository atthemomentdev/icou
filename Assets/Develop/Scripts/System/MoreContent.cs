﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoreContent : MonoBehaviour
{
    public GameObject submit_button;
    public virtual void Init()
    {
        submit_button.SetActive(false);
    }
    // Start is called before the first frame update
    void Start()
    {
        Init();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

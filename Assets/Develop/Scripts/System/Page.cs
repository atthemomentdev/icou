﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Page : MonoBehaviour
{
    public Text title;
    protected const float TIME = 0.2f;
    protected float move_time = 0.0f;
    protected bool move = false;

    protected Vector3 RIGHT = new Vector3(1.0f, 0.0f, 0.0f);
    protected Vector3 LEFT = new Vector3(-1.0f, 0.0f, 0.0f);
    protected Vector3 STOP = new Vector3(0.0f, 0.0f, 0.0f);

    protected Vector3 direction = new Vector3(0,0,0);
    protected float moveSpeed = 0;

    protected float pull_targetX;

    protected float targetX = 0;
    public Header header;
    [HideInInspector]
    public bool reStart = false;

    //가속도
    private float a;
    [HideInInspector]
    public bool isOn = false;
    [HideInInspector]
    public bool back = false;


    private void Start()
    {
        SetSpeed();
        this.GetComponent<Canvas>().enabled = false;
        this.GetComponent<GraphicRaycaster>().enabled = false;
    }
    public void Pushed()
    {
        move_time = 0;
        move = true;
        isOn = true;
        back = true;
        direction = LEFT;
        targetX = -ApplicationManager.GetInstance().canvas.sizeDelta.x * 0.3f;
        SetSpeed();
    }
    public virtual void Pulled()
    {
        move_time = 0;
        move = true;
        isOn = true;
        back = true;
        direction = RIGHT;
        targetX = 0;
        SetSpeed();
    }
    public virtual void Pull()
    {
        SetSpeed();
    }
    public virtual void Push() {
        SetSpeed();
    }
    public virtual void Stop() {
        if(direction == RIGHT)
            GetComponent<RectTransform>().anchoredPosition = new Vector2(targetX,0);
        move = false;
        isOn = false;
        back = false;
        direction = STOP;
    }

    // Update is called once per frame
    void Update()
    {
        if (move)
        {
            move_time += Time.deltaTime;

            float x = startPos.x + (moveSpeed * move_time + a * (move_time) * (move_time) / 2) * direction.x;

            this.GetComponent<RectTransform>().anchoredPosition = new Vector2(x, startPos.y);

            if (direction == LEFT)
            {
                if (this.GetComponent<RectTransform>().anchoredPosition.x < targetX)
                {
                    Stop();
                    return;
                }
            }
            else if(direction == RIGHT)
            {
                if (this.GetComponent<RectTransform>().anchoredPosition.x > targetX)
                {
                    Stop();
                    return;
                }
            }
            if (move_time >= (TIME * 0.95f))
            {
                Stop();
            } 
        }
    }

    public virtual void SetTitle(string title_text)
    {
        title.text = title_text;
    }
    Vector2 startPos;
    protected void SetSpeed()
    {
        move_time = 0;
        startPos = this.GetComponent<RectTransform>().anchoredPosition;
        float d = Mathf.Abs(targetX - startPos.x);
        moveSpeed = 2 * d / TIME;
        a = - moveSpeed / TIME;
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PlaceInfo : MonoBehaviour
{
    public Text placeName;

    public Text number;

    [HideInInspector]
    public string placeId;

    public Text info;

    public BenefitImage sprite;

    TextureContainer img;

    public Image fixBTN;
    public Sprite fixSprite;
    public Sprite unFixSprite;
    bool stretchWidth = false;
    private void Start()
    {
        Vector2 size = this.transform.parent.parent.GetComponent<RectTransform>().sizeDelta;
        if (size.x > size.y)
        {
            stretchWidth = true;
        }
        else
            stretchWidth = false;
        this.GetComponent<LayoutElement>().preferredWidth = size.x;
        this.GetComponent<LayoutElement>().preferredHeight = size.y;
    }
    private void OnEnable()
    {
        if(img != null)
        {
            sprite.LoadImage(img);


            float sizeY = this.GetComponent<RectTransform>().rect.height;

            //sprite.GetComponent<RectTransform>().sizeDelta = new Vector2(sizeY, sizeY);
        }
    }
    public void SetInfo(string name, string imageURL, string id)
    {
        placeName.text = name;

        placeId = id;

        info.text = name + " 근처의 모든 혜택 \n" + name +"사장님들이 제공하는 응원을\n" + "받아보세요";
        
        SetImage(imageURL);
    }
    public void SetFix(bool fix)
    {
        if(fix)
        {
            fixBTN.sprite = fixSprite;
            fixBTN.color  = ApplicationManager.yellow;
            fixBTN.transform.GetChild(0).GetComponent<Text>().text = "살펴보기";
            fixBTN.transform.GetChild(0).GetComponent<Text>().color = ApplicationManager.selectedColor;
        }
        else
        {
            fixBTN.sprite = unFixSprite;
            fixBTN.color  = new Color(1,1,1);
            fixBTN.transform.GetChild(0).GetComponent<Text>().text = "고정하기";
            fixBTN.transform.GetChild(0).GetComponent<Text>().color = fixBTN.color = new Color(1, 1, 1);
        }
    }

    public void SetThisPlaceToMain()
    {
        ApplicationManager.GetInstance().homeButton.GetComponent<Button>().interactable = true;

        DataController.Instance.data.placeId = placeId;
        DataController.Instance.SaveData();

        ApplicationManager.GetInstance().mainPage.place.text = placeName.text;
        
        ApplicationManager.GetInstance().UpdateUserData("region", placeName.text);

        string new_topic = "region_" + placeId;

        string topic = ApplicationManager.GetInstance().placeManager.topic;
        //현재 지역 구독 알람 설정
        if (topic.Length == 0)
            ApplicationManager.GetInstance().Subscribe(new_topic);
        else if (topic != new_topic)
        {
            ApplicationManager.GetInstance().UnSubscribe(topic);
            ApplicationManager.GetInstance().Subscribe(new_topic);
        }

        ApplicationManager.GetInstance().placeManager.topic = new_topic;

        if (ApplicationManager.GetInstance().placeManager.fixedTarget != null)
            ApplicationManager.GetInstance().placeManager.fixedTarget.SetFix(false);

        SetFix(true);
        ApplicationManager.GetInstance().placeManager.fixedTarget = this;

        ApplicationManager.GetInstance().homeButton.Click();
        ApplicationManager.GetInstance().shopManager.ChangeCategory("전체");
    }
    public void SetImage(string url)
    {
        img = new TextureContainer();

        img.URL = url;

        int id = int.Parse(placeId);
        if (id < ApplicationManager.GetInstance().placeManager.placeImg.Length + 1)
            img.texture = ApplicationManager.GetInstance().placeManager.placeImg[id-1];


        sprite.LoadImage(img);


        float size = this.GetComponent<RectTransform>().rect.height;


        Vector2 sizeRect = this.transform.parent.parent.GetComponent<RectTransform>().sizeDelta;
        if (sizeRect.x > sizeRect.y)
        {
            stretchWidth = true;
        }
        else
            stretchWidth = false;

        if (stretchWidth)
            size = this.GetComponent<RectTransform>().rect.width;

        sprite.GetComponent<RectTransform>().sizeDelta = new Vector2(size, size);
    }
}

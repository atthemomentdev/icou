﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationSimulator : MonoBehaviour
{
    public float max_scale;
    public float min_scale;
    public float speed;
    Coroutine co;

    public void StartSimulation()
    {
        up = true;
        time = 0f;
        scale = 1f; co = StartCoroutine(simulation());
    }
    public void StopSimulation()
    {
        StopCoroutine(co);
    }

    bool up = true;
    float time = 0;
    float scale = 1;

    IEnumerator simulation()
    {
        while(true)
        {
            time += Time.deltaTime;
            if (up)
                scale += speed * Time.deltaTime;
            else
                scale -= speed * Time.deltaTime;

            if (scale > max_scale)
            {
                scale = max_scale;
                up = false;
            }
            else if (scale < min_scale)
            {
                scale = min_scale;
                up = true;
            }

            this.transform.localScale = new Vector3(scale, scale, scale);

            yield return null;
        }
        
    }
}

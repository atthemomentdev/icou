﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PrivacyPolicy : MonoBehaviour
{
    [HideInInspector]
    public Text policy;
    private void OnEnable()
    {
        if (policy == null)
        {
            policy = this.GetComponent<Text>();
            ApplicationManager.GetInstance().GetPolicy(policy);
        }
    }
}

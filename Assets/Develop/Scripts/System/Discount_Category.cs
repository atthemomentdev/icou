﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Discount_Category : MonoBehaviour
{
    public Text text;
    Vector2 margin = new Vector2(20, 20);
    Vector2 charSize = new Vector2(34, 40);
    public void SetDiscountCategory(string dis)
    {
        ReSizeTextBox(dis);
    }
    public void ReSizeTextBox(string dis)
    {
        text.text = dis;
        ReSize();
    }

    //텍스트 박스 크기 받아서 상하좌우로 약간의 여백만 남기고 scale
    public void ReSize()
    {
        if (text == null)
            return;

        Vector2 textBox = text.GetComponent<RectTransform>().sizeDelta;

        Vector2 ratio = ApplicationManager.GetInstance().GetRatio();

        float x = text.text.Length * charSize.x;
        float marginX = margin.x * 2 + x;
        float marginY = margin.y* 2 + textBox.y;

        GetComponent<RectTransform>().sizeDelta = new Vector2(marginX, marginY);
    }
}

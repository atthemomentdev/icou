﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class LoadingSceneObject : MonoBehaviour
{
    public Camera _camera;
    public bool Fixed_y_rate = false;
    public float x;
    public float y;
    public Vector2 anchored_position;
    protected RectTransform rect;
    [HideInInspector]
    public Vector2 _screen;
    // Start is called before the first frame update
    void Start()
    {
        SetScreenSize();
        ReSize();
    }
    void SetScreenSize()
    {
#if UNITY_EDITOR
        _screen = new Vector2(_camera.pixelWidth, _camera.pixelHeight);
#elif UNITY_ANDROID && !UNITY_EDITOR
        _screen = new Vector2(_camera.pixelWidth, _camera.pixelHeight);
#elif UNITY_IOS && !UNITY_EDITOR
        _screen = new Vector2(Screen.safeArea.width, Screen.safeArea.height);
#endif
    }
    public virtual void ReSize()
    {
        rect = this.GetComponent<RectTransform>();
        // x : y = x' : y'
        // y' = x' * y / x

        Vector2 ratio = new Vector2((_screen.x / 1080.0f), (_screen.y / 1920.0f));

        float width, height;

        width = x * ratio.x;

        if (Fixed_y_rate == false)
            height = y * ratio.y;
        else
            height = (width * y / x);

        rect.sizeDelta = new Vector2(width, height);
        float posX = anchored_position.x;
        float posY = anchored_position.y;

        posX = anchored_position.x * ratio.x;
        
        posY = anchored_position.y * ratio.x;

        rect.anchoredPosition = new Vector2(posX,posY);
    }

    public virtual void SetImage(Texture texture)
    {
        if (this.transform.GetComponent<RawImage>() != null)
        {
            this.transform.GetComponent<RawImage>().color = new Color(1,1,1,1);
            this.transform.GetComponent<RawImage>().texture = texture;
            AfterSetImage();
        }
    }
    public virtual void AfterSetImage() { }
}

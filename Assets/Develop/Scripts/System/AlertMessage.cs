﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class AlertMessage : MonoBehaviour
{
    public Text _title;
    public Text _message;
    public Text _button;
    public Text _button2;
    System.Action _function1;
    System.Action _function2;
    public void Alert(string title, string message, string button, System.Action function1, string button2 = null, System.Action function2 = null)
    {
        _title.text = title;
        Debug.Log(_title.text);
        _message.text = message;
        _button.text = button;
        _function1 = function1;
        _function2 = function2;

        if (button2 == null)
            _button2.transform.parent.gameObject.SetActive(false);
        else
        {
            _button2.transform.parent.gameObject.SetActive(true);
            _button2.text = button2;
        }

    }
    public void Button1Click()
    {
        if (_function1 != null)
            _function1();

        this.transform.parent.gameObject.SetActive(false);
    }
    public void Button2Click()
    {
        if (_function2 != null)
            _function2();

        this.transform.parent.gameObject.SetActive(false);
    }
}

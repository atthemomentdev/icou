﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class DropboxList : MonoBehaviour
{
    RectTransform rect;
    void Start()
    {
        ReSize();
    }

    public void ReSize()
    {
        rect = this.GetComponent<RectTransform>();
        // x : y = x' : y'
        // y' = x' * y / x
        if (ApplicationManager.GetInstance() == null)
            return;
        Vector2 ratio = ApplicationManager.GetInstance().GetRatio();

        float width, height;
        float x = this.GetComponent<RectTransform>().sizeDelta.x;
        float y = this.GetComponent<RectTransform>().sizeDelta.y;

        width = x * ratio.x;

        height = (width * y / x);

        rect.sizeDelta = new Vector2(width, height);

        rect.anchorMax = new Vector2(0, 1);
        rect.anchorMin = new Vector2(0, 1);

        float posY = - height * (this.transform.GetSiblingIndex()-1);

        rect.anchoredPosition = new Vector2(0,posY);
    }
}

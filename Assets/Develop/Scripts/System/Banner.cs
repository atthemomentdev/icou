﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Banner : BenefitImage
{
    private BannerInfo info;
    static int i = 0;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void SetBannerInfo(BannerInfo banner_info)
    {
        if (info == null || (info._title != banner_info._title))
        {
            info = banner_info;
            LoadImage(banner_info._thumbnail_img);
        }
    }
    public void OpenBannerPage()
    {
        if (info == null)
            return;
        if(info._shop_id == -1)
        {
            ApplicationManager.GetInstance().current_banner_info = info;
            ApplicationManager.GetInstance().adPage.Pull();
        }
        else
        {
            ApplicationManager.GetInstance().shopManager.OpenShopDetail(info._shop_id);
        }
    }
    public override void AfterSetImage()
    {
        i++;
        if(this.transform.parent.childCount == i)
        {
            //ApplicationManager.GetInstance().slide.timer = 4.0f;
        }
    }
}

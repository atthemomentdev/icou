﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadingScene : MonoBehaviour
{
    public Camera _camera;
    // Start is called before the first frame update
    void Start()
    {
        Screen.fullScreen = false;
        ApplicationChrome.statusBarState = ApplicationChrome.States.Visible;
        ApplicationChrome.navigationBarState = ApplicationChrome.States.Visible;

        StartCoroutine(Load_Scene());

        rect = this.GetComponent<RectTransform>();
        canvas = this.GetComponent<CanvasScaler>();
        ratio = (float)Screen.width / (float)Screen.height;
        SetScaler();
    }
    IEnumerator Load_Scene()
    {
        AsyncOperation async = SceneManager.LoadSceneAsync("icou");
        //async.allowSceneActivation = false;
        
        while (!async.isDone)
        {
            yield return null;
        }
    }
    // Update is called once per frame
    void Update()
    {
        float new_ratio = (float)Screen.width / (float)Screen.height;
        if (new_ratio != ratio)
        {
            SetScaler();
            ratio = new_ratio;
        }
    }
    RectTransform rect;
    CanvasScaler canvas;
    const float MAX_RATIO = 0.68f;
    float ratio;
    
    void SetScaler()
    {
        Debug.Log(ratio);
        if (ratio > MAX_RATIO)
            canvas.screenMatchMode = CanvasScaler.ScreenMatchMode.Expand;
        else
            canvas.screenMatchMode = CanvasScaler.ScreenMatchMode.MatchWidthOrHeight;
    }
}

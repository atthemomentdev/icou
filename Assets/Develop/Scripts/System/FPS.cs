﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class FPS : MonoBehaviour
{
    float deltaTime = 0.0f;
    // Start is called before the first frame update
    void Start()
    {
    }
    // Update is called once per frame
    void Update()
    {
        deltaTime += (Time.unscaledDeltaTime - deltaTime) * 0.1f;
    }

    private void OnGUI()
    {
        float msec = deltaTime * 10000.0f;
        float fps = 1.0f / deltaTime;
        this.transform.GetComponent<Text>().text = string.Format("{0:0,0} ms ({1:0.} fps)", msec, fps);
    }
}

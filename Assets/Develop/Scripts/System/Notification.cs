﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Notification : MonoBehaviour
{
    private void Start()
    {
    }
    public void SetMessage(string text)
    {
        this.transform.GetChild(1).GetChild(0).GetChild(0).GetComponent<TextBox>().Set(text);
    }
}

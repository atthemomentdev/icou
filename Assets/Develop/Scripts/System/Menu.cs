﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Menu
{
    public string categoryName;
    public TextureContainer categoryImage = new TextureContainer();
    public TextureContainer menuBoardImage = new TextureContainer();
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MyPageManager : MonoBehaviour
{
    int depth = 0;
    public Transform pages;
    public GameObject currPage;
    public Text title;
    const int pageIndex = -10;
    public void Back()
    {
        if (depth > 0)
            OpenPage(0);
        else
            ApplicationManager.GetInstance().homeButton.Click();
    }

    public void OpenPage(int index)
    {
        currPage.SetActive(false);

        currPage = pages.GetChild(index).gameObject;
        currPage.SetActive(true);

        depth = index;
        title.text = pages.GetChild(index).name;

        ApplicationManager.GetInstance().page_index = pageIndex - index;
    }
    public void LogOutClick()
    {
        ApplicationManager.GetInstance().ShowTwoBtnDialog("로그아웃", "정말로 로그아웃 하시겠습니까?", "로그아웃", "취소",
        ApplicationManager.GetInstance().LogOut, Cancle);
        ApplicationManager.GetInstance().page_index = 7;
    }
    public void Cancle()
    {
        ApplicationManager.GetInstance().page_index = -10;
    }
}

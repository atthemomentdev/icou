﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingIndicator : MonoBehaviour
{
    float timer;
    const float TIME = 0.1f; 
    private void Start()
    {
        timer = 0;
    }
    private void Update()
    {
        timer += Time.deltaTime;
        if(timer > TIME)
        {
            timer = 0;
            this.transform.Rotate(new Vector3(0, 0, -30f));
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Search : MonoBehaviour
{
    [SerializeField]
    public GameObject searchedBTN;

    private List<GameObject> buttons = new List<GameObject>();

    [HideInInspector]
    public Transform searched_list;

    public ShopManager resultUI;
    //public UnityEngine.UI.MyScrollRect scroll;

    public Text text;

    public RectTransform contents;
        
    public void Empty()
    {
        foreach (var btn in buttons)
        {
            GameObject.Destroy(btn);
        }
        buttons = new List<GameObject>();
        SearchBTN.index = 0;
        //나중에는 추천 or 최근검색 넣어주자
    }

    public void SearchItem(string item)
    {
        if (item == null)
            return;

        if (item == "")
            return;

        text.text = "'" + item + "' 검색 결과";
        int count = 0;

        string[] items = item.Split(' ');
        List<Shop> resultList = new List<Shop>();
        foreach (var shop in ApplicationManager.GetInstance().shopManager.shop_list)
        {
            foreach (var word in items)
            {
                if (word != "")
                {
                    if (shop._name.Contains(word))
                    {
                        resultList.Add(shop);
                        count++;
                        break;
                    }
                    else if (shop._branch.Contains(word))
                    {
                        resultList.Add(shop);
                        count++;
                        break;
                    }
                }
            }
        }

        resultUI.CreateNewBTNs(resultList,true);

        if (buttons.Count == 0)
        { }//notification.SetActive(true);
        else
        { }// notification.SetActive(false);
    }

    public void ShowRecordedBTN()
    {
        Empty();

        List<Shop> shops = ApplicationManager.GetInstance().searchManager.GetSearch_list();

        for(int i = shops.Count - 1; i >= 0 ; i --)
        {
            SearchBTN new_btn = Instantiate(searchedBTN, contents).GetComponent<SearchBTN>();
            new_btn.SetInfo(shops[i]);
            buttons.Add(new_btn.gameObject);
        }
    }
}

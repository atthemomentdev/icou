﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BannerInfo
{
    public int _index;
    public TextureContainer _thumbnail_img;
    public List<TextureContainer> _content_img_list;
    public string _title;
    public int _shop_id; // shop 아이디가 -1 이면 shop_detail로 들어갈 수 있는 버튼 없애고 웹페이지 url 띄워주기
    public string _web_url;

    public void SetBannerInfo(string banner_id, string shop_id, string thumbnail_url, string title, string web_url)
    {
        _index = int.Parse(banner_id);
        _shop_id = int.Parse(shop_id);
        _thumbnail_img = new TextureContainer();
        _thumbnail_img.URL = thumbnail_url;
        _title = title;
        _web_url = web_url;
        _content_img_list = new List<TextureContainer>();
        ApplicationManager.GetInstance().LoadBannerContent(this);
    }
    public void DivideContentInfo(string contents)
    {
        string[] tmp = contents.Split('|');
        if (tmp.Length > 1)
        {
            for (int i = 1; i < tmp.Length; i++)
            {
                if (tmp[i] != "")
                {
                    TextureContainer new_texture = new TextureContainer();
                    new_texture.URL = tmp[i];
                    _content_img_list.Add(new_texture);
                }
            }
        }
    }

    public void ClearContents()
    {
        Transform contents = ApplicationManager.GetInstance().bannerManager.content_parent;
        for(int i = 0; i < contents.childCount ; i ++)
        {
            GameObject.Destroy(contents.GetChild(i).gameObject);
        }
    }

    public void OnDestroy()
    {
        _content_img_list.Clear();
        _content_img_list = null;
    }
}

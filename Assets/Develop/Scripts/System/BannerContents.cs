﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BannerContents : BenefitImage
{
    TextureContainer content_img = new TextureContainer();
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void SetTextureContainer(TextureContainer img)
    {
        content_img = img;
    }
    public void LoadTexture()
    {
        LoadImage(content_img);
    }
    public override void Set(TextureContainer img)
    {
        if (image.URL != img.URL)
            return;

        if (this.transform.GetComponent<RawImage>() != null)
        {
            this.transform.GetComponent<RawImage>().texture = img.texture;
            Vector2 size = new Vector2((float)(img.texture.width), (float)(img.texture.height));
            float ratio = ApplicationManager.GetInstance().WIDTH / size.x;
            size.x = ApplicationManager.GetInstance().WIDTH;
            size.y = size.y * ratio;
            this.transform.GetComponent<RectTransform>().sizeDelta = size;
            AfterSetImage();
        }
    }

    public override void AfterSetImage()
    {
        int index = this.transform.parent.GetSiblingIndex();
        this.transform.GetComponent<RawImage>().color = after_color;
        Transform parentObj = ApplicationManager.GetInstance().bannerManager.content_parent;

        if (this.transform.GetSiblingIndex() + 1 < parentObj.childCount)
        {
            parentObj.GetChild(this.transform.GetSiblingIndex() + 1).GetComponent<BannerContents>().LoadTexture();
        }
    }
}

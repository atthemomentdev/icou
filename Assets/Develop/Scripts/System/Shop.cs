﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop : MonoBehaviour
{
    public string _placeId;     // 지역 아이디
    public string _category;    // 카테고리 종류
    public string _id;          // 매장 ID
    public bool _hide;
    public string _name;        // 매장 이름
    public string _branch;      // 매장 지점 이름
    public string _discountType;    // 할인 카테고리!
    public TextureContainer _thumbnail = new TextureContainer();   // 썸네일 사진
    public bool   _like;        // 찜 한 여부
    public bool   new_shop;     // 새로운 매장인지 여부

    public string _brief;        // 간단 소개
    public TextureContainer _introImg = new TextureContainer();  // 인트로 사진
    public string _benefit;      // 혜택 내용
    public string _address;      // 주소
    public string _contact;      // 연락처
    public string _openTime;     // 오픈시간
    public string _otherInfo;    // 기타정보
    public string _menu;         // 메뉴
    public string _benePick;     // 베네픽
    const int NEW_DAY = 7;
    public List<Menu> _menus = new List<Menu>();
    public List<string> _benePicks = new List<string>();
    public List<CheerDetail> _cheers;

    public bool CheckAlreadyLoad()
    {
        /*
                 Debug.Log("breif: " + _brief + "\nintro: " + _introImg.URL + "\n_discount: " + _discount.URL +
                  "\n_address: " + _address + "\n_contact: "+ _contact + "\n_openTime :" + _openTime +
                  "\n_otherInfo: "+ _otherInfo + "_menu: " + _menu + "\nbenepick: " + _benePick);
         
         */
        if (_brief == "")
            return false;
        if (_introImg.URL == "")
            return false;
        if (_benefit == "")
            return false;
        if (_address     == "")
            return false;
        if (_contact     == "")
            return false;
        if (_openTime    == "")
            return false;
        if (_menu        == "")
             return false;
        if (_benePick == "")
            return false;

        return true;
    }
    public void SetDetail(string[] detail)
    {
        /*
            1: Brief
            2: intro_url
            3: discount_url
            4: 상세주소
            5: 연락처
            6: 오픈시간
            7: 기타정보
            8: 메뉴
            9: 베네픽
         */
        if (detail.Length < 10)
            return;
        _brief        = detail[1];
        _introImg.URL = detail[2];
        _benefit      = detail[3];
        _address      = detail[4];
        _contact      = detail[5];
        _openTime     = detail[6];
        _otherInfo    = detail[7];
        _menu         = detail[8];
        _benePick     = detail[9];
        SetMenus(_menu);
        SetBenepicks(_benePick);
    }
    public void SetShop(string id, string category, string shop_name, string branch, string discount, string thumbnail, string time, string placeId)
    {
        _id = id;
        switch (category)
        {
            case "RESTAURANT":
                category = "밥";
                break;
            case "CAFE":
                category = "카페";
                break;
            case "DRINK":
                category = "술집";
                break;
            case "LIFE":
                category = "라이프";
                break;
        }
        _category = category;
        _name = shop_name;
        _branch = branch;
        _discountType = discount;
        _thumbnail.URL = thumbnail;
        CheckLikeShop();
        CheckNewShop(time);
        _placeId = placeId;
    }
    void CheckNewShop(string time)
    {
        System.DateTime update_time = ApplicationManager.GetInstance().String2Time(time);
        System.DateTime today = ApplicationManager.GetInstance().accessTime;
        System.DateTime lastDay = update_time.AddDays(NEW_DAY);

        int compareDate = System.DateTime.Compare(lastDay, today);

        if (compareDate > 0)
            new_shop = true;
        else
            new_shop = false;
    }
    void CheckLikeShop()
    {
        foreach(var id in DataController.Instance.data.like_list)
        {
            if (id == _id)
            {
                _like = true;
                return;
            }
        }
        _like = false;
    }

    void SetMenus(string menuList)
    {
        string[] tmp = menuList.Split('#');
        for (int i = 0; i < (tmp.Length / 3); i++)
        {
            Menu new_menu = new Menu();

            new_menu.categoryImage.URL = tmp[3 * i + 2];
            new_menu.categoryName = tmp[3 * i + 1];
            new_menu.menuBoardImage.URL = tmp[3 * i + 3];

            _menus.Add(new_menu);
        }
    }
    void SetBenepicks(string benePickList)
    {
        string[] tmp = benePickList.Split('#');
        for (int i = 0; i < tmp.Length; i++)
        {
            if (tmp[i] != "")
            {
                _benePicks.Add(tmp[i]);
            }
        }
    }

    public int AddCheers(string cheer)
    {
        if (_cheers == null)
            _cheers = new List<CheerDetail>();
        string[] tmp = pData.RETCODE.Split('|');
        int beforeCount = _cheers.Count;
        int cheerCount = tmp.Length - 1;
        _cheers.Clear();
        beforeCount = 0;
        _cheers = new List<CheerDetail>();

        if(cheerCount > beforeCount)
        {
            if (tmp.Length > 1)
            {
                for (int i = 0; i < cheerCount; i++)
                {
                    if ((beforeCount + i + 1) >= tmp.Length)
                        break;

                    if (tmp[beforeCount + i + 1] != "")
                    {
                        CheerDetail new_cheer = new CheerDetail();
                        new_cheer.SetDetail(tmp[beforeCount + i + 1]);
                        _cheers.Add(new_cheer);
                    }
                }
            }
        }
        else if(cheerCount <= beforeCount)
        {
            _cheers.Clear();
            beforeCount = 0;
            _cheers = new List<CheerDetail>();
            if (tmp.Length > 1)
            {
                for (int i = 0; i < cheerCount; i++)
                {
                    if (i + 1 >= tmp.Length)
                        break;
                    if (tmp[i + 1] != "")
                    {
                        CheerDetail new_cheer = new CheerDetail();
                        new_cheer.SetDetail(tmp[beforeCount + i + 1]);
                        _cheers.Add(new_cheer);
                    }
                }
            }
        }

        return _cheers.Count;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

public class DataController : MonoBehaviour
{   //싱클톤 =============================================

    static GameObject _container;

    static GameObject Container

    {
        get
        {
            return _container;
        }

    }

    static DataController _instance;

    public static DataController Instance

    {
        get
        {
            if (!_instance)
            {
                _container = new GameObject();
                _container.name = "DataController";
                _instance = _container.AddComponent(typeof(DataController)) as DataController;
                DontDestroyOnLoad(_container);
            }

            return _instance;
        }

    }
    public string DataFileName = "/benefit_data.json";  //이름 변경 절대 X 


    Data _data;
    public Data data
    {
        get
        {
            if (_data == null)
            {
                LoadData();
                SaveData();
            }
            return _data;
        }
    }

    public void LoadData()
    {
        string filePath = Application.persistentDataPath + DataFileName;
        
        if (File.Exists(filePath))
        {
            string FromJsonData = File.ReadAllText(filePath);
            _data = JsonUtility.FromJson<Data>(FromJsonData);
            string a = "";
            foreach (var id in _data.like_list)
            {
                a += id;
                a += ", ";
            }
        }

        else
        {
            Debug.Log("새로운 파일 생성");
            _data = new Data();
        }
    }
    public void SaveData()
    {
        string ToJsonData = JsonUtility.ToJson(data);
        string filePath = Application.persistentDataPath + DataFileName;
        File.WriteAllText(filePath, ToJsonData);
    }
    public void ClearData()
    {
        _data = new Data();

        SaveData();
    }
    private void OnApplicationQuit()
    {
        SaveData();
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI.Extensions;
using UnityEngine.UI;
public class PlaceManager : MonoBehaviour
{
    //int place_id별 shop 갯수 //ShopInfoSet할때 place_ids[tmp[7]]++ 해주면 될 듯!
    public Transform contents;
    public HorizontalScrollSnap snap;
    public Transform categories;
    public MoveBar bar;
    public GameObject btnObj;
    [HideInInspector]
    public List<PlaceBTN> placeList;
    PlaceBTN currTarget;
    PlaceBTN currPlace;
    [HideInInspector]
    public PlaceInfo fixedTarget;
    [HideInInspector]
    public int[] counts;
    [HideInInspector]
    public string topic = "";
    public Texture[] placeImg;

    bool move = false;

    public void SetPlaces()
    {
        counts = new int[placeList.Count + 1];

        for (int i = 0; i <categories.childCount; i++ )
        {
            PlaceInfo info = categories.GetChild(i).GetComponent<PlaceBTN>().info;
            ApplicationManager.GetInstance().placeManager.snap.AddChild(info.gameObject);
        }
    }

    public void OnEnable()
    {
        if (fixedTarget)
        {
            move = false;
            int i = fixedTarget.transform.GetSiblingIndex();
            categories.GetChild(i).GetComponent<PlaceBTN>().Click();
        }
    }

    public void InitPage()
    {
        if(fixedTarget)
            fixedTarget.SetFix(false);
        
        currTarget = null;
        fixedTarget = null;
    }

    public void SetPlacesCounts()
    {
        foreach (var p in placeList)
        {
            int i = int.Parse(p.info.placeId);
            p.info.number.text = counts[i].ToString() + " 매장";
        }

        SetCurrentPlace();
    }

    public void SetCurrentPlace()
    {
        if (ApplicationManager.GetInstance().user == null)
        {
            Debug.Log("user is null");
            return;
        }

        string placeName = ApplicationManager.GetInstance().user.region;
        
        foreach (var place in placeList)
        {
            if (place.info.placeName.text == placeName)
            {
                currTarget = place;

                currTarget.Click();
                currTarget.info.SetThisPlaceToMain();
                return;
            }
        }
        if (placeList.Count != 0)
        {
            placeList[0].Click();
            ApplicationManager.GetInstance().homeButton.GetComponent<Button>().interactable = false;
            ApplicationManager.GetInstance().page_index = 100;
        }
    }

    public void CreatePlaceBTN(string id, string name, string img)
    {
        if (placeList == null)
            placeList = new List<PlaceBTN>();

        PlaceBTN btn = Instantiate(btnObj, categories).GetComponent<PlaceBTN>();
        btn.SetPlaceInfo(id, name, img);

        placeList.Add(btn);
    }

    public void ChangePlace(int i)
    {
        foreach(var placebtn in placeList)
        {
            placebtn.OFF();
        }

        if (i != -1)
            snap.MovePage(i);
        else
            i = ApplicationManager.GetInstance().placeManager.snap.CurrentPage;

        currTarget = categories.GetChild(i).GetComponent<PlaceBTN>();
        RectTransform btn = currTarget.GetComponent<RectTransform>();

        currTarget.ON();
        bar.Move(btn.anchoredPosition.x);
        NavigationMove();
    }

    void NavigationMove()
    {
        if(move == false)
        {
            Transform navigation = categories.transform.parent;

            if (placeList == null)
                return;

            float space = currTarget.transform.GetComponent<RectTransform>().sizeDelta.x + categories.GetComponent<HorizontalLayoutGroup>().spacing;
            int n = currTarget.transform.GetSiblingIndex();
            float screenX = 1080;
            float targetX = screenX / 2 - n * (space) - currTarget.transform.GetComponent<RectTransform>().sizeDelta.x / 2;

            if (targetX > 0)
            {
                targetX = 0;
            }
            else if (targetX < screenX - navigation.GetComponent<RectTransform>().sizeDelta.x)
            {
                targetX = screenX - navigation.GetComponent<RectTransform>().sizeDelta.x;
            }

            StartCoroutine(MoveNavigator(navigation.GetComponent<RectTransform>().anchoredPosition.x, targetX));
        }
    }
    IEnumerator MoveNavigator(float startX, float targetX)
    {
        move = true;
        Transform navigation = categories.transform.parent;
        float X = startX;
        float TIME = 0.15F;
        float speed = (targetX - startX) / TIME;
        float time = 0;
        bool right = false;
        if (targetX > startX)
            right = true;
        else
            right = false;

        while (time < TIME)
        {
            time += Time.deltaTime;
            
            X += speed * Time.deltaTime;

            if ((((right == true) && (X > targetX)) || ((right == false) && (X < targetX))))
            {
                X = targetX;
                time += TIME;
            }

            navigation.GetComponent<RectTransform>().anchoredPosition = new Vector2(X, navigation.GetComponent<RectTransform>().anchoredPosition.y);
            
            yield return null;
        }

        /*
        do
        {
            yield return null;
            time += Time.deltaTime;
            X = startX + speed * time;

            navigation.GetComponent<RectTransform>().anchoredPosition = new Vector2(X, navigation.GetComponent<RectTransform>().anchoredPosition.y);

        } while ;
        */
        X = targetX;
        navigation.GetComponent<RectTransform>().anchoredPosition = new Vector2(X, navigation.GetComponent<RectTransform>().anchoredPosition.y);
        move = false;
    }
}

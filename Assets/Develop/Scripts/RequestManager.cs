﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RequestManager : MoreContent
{
    public Dropdown request_type;
    public Text title;
    public InputField input_title;
    public InputField input_contact;
    public InputField input_contents;
    public GameObject submit_screen;
    [HideInInspector]
    public string[] submit_values = new string[4];
    public override void Init()
    {
        submit_screen.SetActive(false);
        submit_button.SetActive(true);
        request_type.value = 0;
        input_title.text = "";
        input_contact.text = "";
        input_contents.text = "";
        submit_button.transform.GetChild(0).GetComponent<Text>().color = new Color(0.8f, 0.8f, 0.8f); ;
        submit_button.transform.GetComponent<Button>().enabled = false;
    }
    void Start()
    {
        Init();
    }
    public void TurnOnKeyBoard()
    {
        TouchScreenKeyboard.Open("", TouchScreenKeyboardType.Default, false, false, true);
    }
    public void ChangeType()
    {
        int type = request_type.value;
        string title_text = "";
        switch(type)
        {
            case 0:
                input_title.transform.GetChild(1).GetComponent<Text>().text = "업체(매장)명 입력";
                title_text = "업체명";
                break;
            default:
                input_title.transform.GetChild(1).GetComponent<Text>().text = "제목 입력";
                title_text = "제목";
                break;
        }
        title.text = title_text;
    }
    
    public void Check()
    {
        submit_button.SetActive(true);
        if (input_title.text.Length  == 0)
        {
            SetButtonActive(false);
            return;
        }
        if (input_contact.text.Length == 0)
        {
            SetButtonActive(false);
            return;
        }
        if(input_contents.text.Length == 0)
        {
            SetButtonActive(false);
            return;
        }
        SetButtonActive(true);
        return;
    }
    // Update is called once per frame
    void Update()
    {
        
    }

    void SetButtonActive(bool enable)
    {
        submit_button.transform.GetComponent<Button>().enabled = enable;

        if (enable == true)
            submit_button.transform.GetChild(0).GetComponent<Text>().color = new Color(0.2745f, 0.435f, 0.384f);
        else
            submit_button.transform.GetChild(0).GetComponent<Text>().color = new Color(0.8f, 0.8f, 0.8f);

    }
    public void Submit()
    {
        if(submit_button.transform.GetComponent<Button>().enabled == true)
        {
            /*
         string[] name = { "request_type", "request_title", "request_contact", "request_content" };    
         */
            submit_values[0] = request_type.transform.GetChild(0).GetComponent<Text>().text;
            submit_values[1] = input_title.text;
            submit_values[2] = input_contact.text;
            submit_values[3] = input_contents.text;
            submit_screen.SetActive(true);
            submit_button.SetActive(false);
            submit_screen.transform.GetChild(0).GetComponent<AnimationSimulator>().StartSimulation();
            ApplicationManager.GetInstance().SubmitRequest(submit_values);
        }
        else
        {
            if (input_title.text.Length == 0)
            {
                input_title.ActivateInputField();
                return;
            }
            if (input_contact.text.Length == 0)
            {
                input_contact.ActivateInputField();
                return;
            }
            if (input_contents.text.Length == 0)
            {
                input_contents.ActivateInputField();
                return;
            }
        }
    }
}

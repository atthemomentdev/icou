﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LikeManager : MonoBehaviour
{
    public ShopManager like_shops_contents;
    public Transform slideBanner;
    private void OnEnable()
    {
        ShowLikeShops();
    }
    public void ShowLikeShops()
    {
        like_shops_contents.CreateNewBTNs(ApplicationManager.GetInstance().shopManager.GetLikeShopList());
    }
}

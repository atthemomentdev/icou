﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DataRegister : MonoBehaviour
{
    public Button nextButton;
    private void OnEnable()
    {
        Init();
    }
    public virtual void CheckNextBTNAvailable() { }
    public virtual void Init() { }
    public void SetNextBTNActive(bool active)
    {
        nextButton.interactable = active;
    }
    protected virtual void RegisterInfo() { }
    public void Register()
    {
        RegisterInfo();

        this.gameObject.SetActive(false);
        this.transform.parent.GetChild(this.transform.GetSiblingIndex() + 1).gameObject.SetActive(true);
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using Firebase;
using Firebase.Database;
using Firebase.Auth;

public class User
{
    public string email;
    public string gender;
    public string birthDate;
    public string region;
    //
    public User() {
        email = "";
        gender = "";
        birthDate = "";
        region = "";
    }
    public User(string _email, string _gender, string _birthDate)
    {
        email = _email;
        gender = _gender;

        birthDate = _birthDate;
        region = "";
    }
}
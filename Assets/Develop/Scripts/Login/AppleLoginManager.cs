﻿using System;
using System.Security.Cryptography;
using System.Collections;
using System.Collections.Generic;
using System.Text;


using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using AppleAuth;
using AppleAuth.Enums;
using AppleAuth.Extensions;
using AppleAuth.Interfaces;
using AppleAuth.Native;

using Firebase.Auth;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using Firebase.Extensions;
using UnityEditor;


public class AppleLoginManager : MonoBehaviour
{
    ///////////////////////////
    /// <summary>
    /// 애플 로그인 기능!
    /// </summary>
    ///////////////////////////
    
    private const string AppleUserIdKey = "AppleUserId";

    private IAppleAuthManager _appleAuthManager;
    private void Start()
    {
        AppleLoginSupport();
    }
    public void AuthMgrUpdate()
    {
        // Updates the AppleAuthManager instance to execute
        // pending callbacks inside Unity's execution loop
        if (this._appleAuthManager != null)
        {
            this._appleAuthManager.Update();
        }
    }
    public bool AppleLoginSupport()
    {
        if(AppleAuthManager.IsCurrentPlatformSupported)
        {
            if (this._appleAuthManager == null)
            {
                // Creates a default JSON deserializer, to transform JSON Native responses to C# instances
                var deserializer = new PayloadDeserializer();
                // Creates an Apple Authentication manager with the deserializer
                this._appleAuthManager = new AppleAuthManager(deserializer);
            }

            return true;
        }
        else
            return false;
    }
    public bool CheckAppleLoginAlready()
    {
        if (AppleLoginSupport() == false)
            return true;

        // If at any point we receive a credentials revoked notification
        // we delete the stored User ID, and go back to login
        this._appleAuthManager.SetCredentialsRevokedCallback(result =>
        {
            Debug.Log("Received revoked callback " + result);
            PlayerPrefs.DeleteKey(AppleUserIdKey);
        });

        // If we have an Apple User Id available, get the credential status for it
        if (PlayerPrefs.HasKey(AppleUserIdKey))
            return true;
        else
            return false;
    }
    
    // 기존에 해당 id의 유저가 있는지 체크
    private void SearchUserOnFirebase(string appleUserId, ICredential credential)
    {
        Announce("SUCCESS", "User ID: " + appleUserId + "\nCredential: " + credential);

       // FA userID가 있는지 찾기
    }
    private void Announce(string title, string content)
    {
        ApplicationManager.GetInstance().ShowOneBtnDialog(title, content, "확인", null);
    }

    private void CheckCredentialStatusForUserId(string appleUserId)
    {
        // If there is an apple ID available, we should check the credential state
        this._appleAuthManager.GetCredentialState(
            appleUserId,
            state =>
            {
                switch (state)
                {
                    // If it's authorized, login with that user id
                    case CredentialState.Authorized:
                        this.SearchUserOnFirebase(appleUserId, null);
                        return;

                    // If it was revoked, or not found, we need a new sign in with apple attempt
                    // Discard previous apple user id
                    case CredentialState.Revoked:
                    case CredentialState.NotFound:
                        Announce("FAILED", "Credential NOT FOUNDED / REVOKED");
                        
                        PlayerPrefs.DeleteKey(AppleUserIdKey);
                        return;
                }
            },
            error =>
            {
                var authorizationErrorCode = error.GetAuthorizationErrorCode();

                Announce("ERROR", "User ID: " + "Error while trying to get credential state " + authorizationErrorCode.ToString() + " " + error.ToString());
            });
    }
    
    private static string GenerateRandomString(int length)
    {
        if (length <= 0)
        {
            throw new Exception("Expected nonce to have positive length");
        }

        const string charset = "0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._";
        var cryptographicallySecureRandomNumberGenerator = new RNGCryptoServiceProvider();
        var result = string.Empty;
        var remainingLength = length;

        var randomNumberHolder = new byte[1];
        while (remainingLength > 0)
        {
            var randomNumbers = new List<int>(16);
            for (var randomNumberCount = 0; randomNumberCount < 16; randomNumberCount++)
            {
                cryptographicallySecureRandomNumberGenerator.GetBytes(randomNumberHolder);
                randomNumbers.Add(randomNumberHolder[0]);
            }

            for (var randomNumberIndex = 0; randomNumberIndex < randomNumbers.Count; randomNumberIndex++)
            {
                if (remainingLength == 0)
                {
                    break;
                }

                var randomNumber = randomNumbers[randomNumberIndex];
                if (randomNumber < charset.Length)
                {
                    result += charset[randomNumber];
                    remainingLength--;
                }
            }
        }

        return result;
    }

    private static string GenerateSHA256NonceFromRawNonce(string rawNonce)
    {
        var sha = new SHA256Managed();
        var utf8RawNonce = Encoding.UTF8.GetBytes(rawNonce);
        var hash = sha.ComputeHash(utf8RawNonce);

        var result = string.Empty;
        for (var i = 0; i < hash.Length; i++)
        {
            result += hash[i].ToString("x2");
        }

        return result;
    }

    public void AppleLoginWithFirebase(Action<FirebaseUser> firebaseAuthCallback)
    {
        var rawNonce = GenerateRandomString(32);
        var nonce = GenerateSHA256NonceFromRawNonce(rawNonce);

        var loginArgs = new AppleAuthLoginArgs(
            LoginOptions.IncludeEmail | LoginOptions.IncludeFullName,
            nonce);

        _appleAuthManager.LoginWithAppleId(
            loginArgs,
            credential =>
            {
                var appleIdCredential = credential as IAppleIDCredential;
                if (appleIdCredential != null)
                {
                    PlayerPrefs.SetString(AppleUserIdKey, appleIdCredential.User);
                    this.PerformFirebaseAuthentication(appleIdCredential, rawNonce, firebaseAuthCallback);
                }
            },
            error =>
            {
                Announce("FA Failed", "Perform Login With AppleId And Firebase is failed");
            });
    }
    public void QuickAppleLoginWithFirebase(Action<FirebaseUser> firebaseAuthCallback)
    {
        var rawNonce = GenerateRandomString(32);
        var nonce = GenerateSHA256NonceFromRawNonce(rawNonce);

        var quickLoginArgs = new AppleAuthQuickLoginArgs(nonce);

        _appleAuthManager.QuickLogin(
            quickLoginArgs,
            credential =>
            {
                var appleIdCredential = credential as IAppleIDCredential;
                if (appleIdCredential != null)
                {
                    PlayerPrefs.SetString(AppleUserIdKey, appleIdCredential.User);
                    this.PerformFirebaseAuthentication(appleIdCredential, rawNonce, firebaseAuthCallback);
                }
            },
            error =>
            {
                Announce("FA Failed", "Perform Quick Login With AppleId And Firebase is failed");
            });
    }

    private void PerformFirebaseAuthentication(IAppleIDCredential appleIdCredential, string rawNonce, Action<FirebaseUser> firebaseAuthCallback)
    {
        var identityToken = Encoding.UTF8.GetString(appleIdCredential.IdentityToken);
        var authorizationCode = Encoding.UTF8.GetString(appleIdCredential.AuthorizationCode);
        var firebaseCredential = OAuthProvider.GetCredential("apple.com", identityToken, rawNonce, authorizationCode);
        
        ApplicationManager.GetInstance().auth.SignInWithCredentialAsync(firebaseCredential)
            .ContinueWithOnMainThread(task => FirebaseSignIn(task, firebaseAuthCallback));
    }

    private void FirebaseSignIn(System.Threading.Tasks.Task<FirebaseUser> task, Action<FirebaseUser> firebaseUserCallback)
    {
        if (task.IsCanceled)
        {
            Announce("로그인 실패", "Firebase auth was canceled");
            firebaseUserCallback(null);
        }
        else if (task.IsFaulted)
        {
            Announce("로그인 실패", "Firebase auth was failed");
            firebaseUserCallback(null);
        }
        else
        {
            var firebaseUser = task.Result;
            //Announce("로그인 성공","Firebase auth completed | User ID:" + firebaseUser.UserId);
            firebaseUserCallback(firebaseUser);
        }
    }
}

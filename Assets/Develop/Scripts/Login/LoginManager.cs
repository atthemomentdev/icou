﻿using System;
using System.Security.Cryptography;
using System.Collections;
using System.Collections.Generic;
using System.Text;


using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using AppleAuth;
using AppleAuth.Enums;
using AppleAuth.Extensions;
using AppleAuth.Interfaces;
using AppleAuth.Native;

using Firebase.Auth;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using Firebase.Extensions;
using UnityEditor;

using Facebook.Unity;


public class LoginManager : MonoBehaviour
{
    public GameObject registerPage;
    public AppleLoginManager appleLoginMgr;
    public NetworkingButton loginBTN;
    public NetworkingButton registerBTN;
    public NetworkingButton idCheckBTN;
    public NetworkingButton passwordBTN;
    public NetworkingButton appleLoginBTN;

    bool simpleLogin = false;

    NetworkingButton targetBTN;
    bool logInSuccess;

    public GameObject privacy;

    [HideInInspector]
    private static LoginManager instance;

    string id = "";
    string pw = "";
    GameObject indicator;

    public InputField email;
    public InputField password;


    bool loginFinish;
    string result;
    bool register = false;

    public static LoginManager GetInstance()
    {
        if (!instance)
        {
            instance = FindObjectOfType(typeof(LoginManager)) as LoginManager;
            if (!instance)
                Debug.LogError("There needs to be one active MyClass script on a GameObject in your scene.");
        }
        return instance;
    }
    Firebase.Auth.FirebaseAuth auth;
    Firebase.Auth.FirebaseUser user;

    private void OnEnable()
    {
        auth = ApplicationManager.GetInstance().auth;
        Init();
    }

    public void Update()
    {
        appleLoginMgr.AuthMgrUpdate();
        if (loginFinish)
        {
            if (logInSuccess)
            {
                if (register)
                    RegisterNewUser();

                email.text = "";
                password.text = "";

                LoginFinish();
                ApplicationManager.GetInstance().StartCheckAuth();
            }
            else
            {
                ApplicationManager.GetInstance().ShowOneBtnDialog("로그인 실패", result, "다시 시도", null);
                SetButtonOn();
            }
            loginFinish = false;
        }
    }

    private void LoginFinish()
    {
        FB.Mobile.FetchDeferredAppLinkData();

        var parameter = new Dictionary<string, object>();
        parameter[AppEventParameterName.Currency] = "WON";

        if (FB.IsInitialized == true)
        {
            FB.ActivateApp();
            FB.LogAppEvent(AppEventName.CompletedRegistration, 1, parameter);
        }
        else
        {
            FB.Init();
            FB.ActivateApp();
            FB.LogAppEvent(AppEventName.CompletedRegistration, 1, parameter);
        }
    }

    public void SetButtonOn()
    {
        if (targetBTN != null)
        {
            targetBTN.FinishLoad();
            targetBTN = null;
        }
    }

    void SetButtonOff()
    {
        if (targetBTN != null)
            targetBTN.StartLoad();
    }

    [HideInInspector]
    public User userInfo = new User();
    [HideInInspector]
    public bool emailCheck = false;
    [HideInInspector]
    public bool emailAvailable = false;
    public void Empty() { }

    public void LogIn()
    {
        if (loginBTN.clicked)
            return;

        targetBTN = loginBTN;

        SetButtonOff();

        logInSuccess = false;


        auth.SignInWithEmailAndPasswordAsync(email.text.ToString(), password.text.ToString()).ContinueWith(task =>
        {
            if (task.IsCanceled)
            {
                result = "잘못된 아이디 혹은 비밀번호를 입력하였습니다";
                loginFinish = true;

                return;
            }
            if (task.IsFaulted)
            {
                result = "잘못된 아이디 혹은 비밀번호를 입력하였습니다";
                loginFinish = true;
                return;
            }

            Firebase.Auth.FirebaseUser newUser = task.Result;
            Debug.LogFormat("User signed in successfully: {0} ({1})",
                newUser.DisplayName, newUser.UserId);

            loginFinish = true;
            logInSuccess = true;
        });
    }

    public void Init()
    {
        loginFinish = false;

        logInSuccess = false;

        register = false;

        simpleLogin = false;

        SetButtonOn();
        targetBTN = registerBTN;
        SetButtonOn();
        targetBTN = idCheckBTN;
        SetButtonOn();

        if (appleLoginMgr.AppleLoginSupport())
            appleLoginBTN.gameObject.SetActive(true);
        else
            appleLoginBTN.gameObject.SetActive(false);

        userInfo = new User();
        int count = this.transform.GetChild(1).GetChild(1).childCount;

        this.transform.GetChild(1).GetChild(1).GetChild(0).GetComponent<DataRegister>().Init();
        this.transform.GetChild(1).GetChild(1).GetChild(0).gameObject.SetActive(true);

        registerPage.transform.GetChild(0).GetChild(0).gameObject.SetActive(true); // back버튼 다시 생성

        for (int i = 1; i < count; i++)
        {
            this.transform.GetChild(1).GetChild(1).GetChild(i).GetComponent<DataRegister>().Init();
            this.transform.GetChild(1).GetChild(1).GetChild(i).gameObject.SetActive(false);
        }

        this.transform.GetChild(0).gameObject.SetActive(true);
        this.transform.GetChild(1).gameObject.SetActive(false);
    }

    public IEnumerator CheckEmailAvailable(string checkEmail)
    {
        emailCheck = false;
        emailAvailable = false;
        targetBTN = idCheckBTN;
        SetButtonOff();

        string name = "email";
        string pdata = checkEmail;
        pData.SetValue(name, pdata);
        UnityWebRequest keyUrl = pData.ActiveKey("https://benefit.atthemomentkor.com/user_check.php");
        keyUrl.useHttpContinue = false;
        keyUrl.chunkedTransfer = false;
        AsyncOperation async = keyUrl.SendWebRequest();

        while (!async.isDone)
        {
            yield return null;
        }

        emailCheck = true;
        if (keyUrl.isNetworkError || keyUrl.isHttpError)
        {
            Debug.Log("네트워크 연결에 실패했습니다.");
        }
        else
        {
            pData.RETCODE = keyUrl.downloadHandler.text;
            if (pData.RETCODE == "\navailable")
                emailAvailable = true;
            else

                emailAvailable = false;
        }

    }

    public void RegisterEmail(string rEmail, string rPassword)
    {
        if (auth == null)
            auth = Firebase.Auth.FirebaseAuth.DefaultInstance;

        auth.CreateUserWithEmailAndPasswordAsync(rEmail, rPassword).ContinueWith(task =>
        {
            if (task.IsCanceled)
            {
                ApplicationManager.GetInstance().ShowOneBtnDialog("가입 실패", "CreateUserWithEmailAndPasswordAsync was canceled.", "다시 시도", null);
                loginFinish = true;
                return;
            }
            if (task.IsFaulted)
            {
                ApplicationManager.GetInstance().ShowOneBtnDialog("가입 실패",
                    "CreateUserWithEmailAndPasswordAsync encountered an error: " + task.Exception, "다시 시도", null);
                loginFinish = true;
                return;
            }

            // Firebase user has been created.
            Firebase.Auth.FirebaseUser newUser = task.Result;
            Debug.LogFormat("Firebase user created successfully: {0} ({1})",
                newUser.DisplayName, newUser.UserId);


            id = rEmail;
            pw = rPassword;

            user = newUser;
            register = true;
            loginFinish = true;
            logInSuccess = true;
        });
    }

    public void CheckLoginBTNAvailable()
    {
        if ((email.text.Length == 0) || (password.text.Length == 0))
        {
            loginBTN.GetComponent<Button>().interactable = false;
        }
        else
        {
            loginBTN.GetComponent<Button>().interactable = true;
        }

    }

    public void Register()
    {
        if (registerBTN.clicked)
            return;

        targetBTN = registerBTN;

        SetButtonOff();
        if(simpleLogin == false)
        {
            string pw = passwordBTN.transform.parent.GetComponent<PWRegister>().pwField.text.ToString();
            RegisterEmail(userInfo.email, pw);
        }
        else
        {
            register = true;
            loginFinish = true;
            logInSuccess = true;
        }
    }

    public void ShowPrivacy()
    {
        privacy.SetActive(true);
    }

    public void RegisterNewUser()
    {
        StartCoroutine(CreateUserData());
    }

    IEnumerator CreateUserData()
    {
        string[] name = { "id", "email", "gender", "birthDate" };
        string[] pdata ={ auth.CurrentUser.UserId.ToString(), userInfo.email, userInfo.gender, userInfo.birthDate };

        if (simpleLogin == true)
            pdata[1] = auth.CurrentUser.Email;

        pData.SetValue(name, pdata);
        UnityWebRequest keyUrl = pData.ActiveKey("https://benefit.atthemomentkor.com/user_create.php");
        keyUrl.useHttpContinue = false;
        keyUrl.chunkedTransfer = false;
        AsyncOperation async = keyUrl.SendWebRequest();
        yield return null;
    }

    public void AppleLogin()
    {
        if (appleLoginBTN.clicked)
            return;

        appleLoginBTN.StartLoad();
        appleLoginMgr.AppleLoginWithFirebase(fUser =>
        {
            user = fUser;
            StartCoroutine(CheckAppleAuthExist());
        });
    }
    
    public IEnumerator CheckAppleAuthExist()
    {
        targetBTN = idCheckBTN;
        SetButtonOff();

        string name = "id";
        string pdata = ApplicationManager.GetInstance().auth.CurrentUser.UserId;
        pData.SetValue(name, pdata);
        UnityWebRequest keyUrl = pData.ActiveKey("https://benefit.atthemomentkor.com/exist_check.php");
        keyUrl.useHttpContinue = false;
        keyUrl.chunkedTransfer = false;
        AsyncOperation async = keyUrl.SendWebRequest();

        while (!async.isDone)
        {
            yield return null;
        }

        appleLoginBTN.FinishLoad();

        if (keyUrl.isNetworkError || keyUrl.isHttpError)
        {
            Debug.Log("네트워크 연결에 실패했습니다.");
        }
        else
        {
            pData.RETCODE = keyUrl.downloadHandler.text;
            if (pData.RETCODE == "\nexist")
            {
                loginFinish = true;
                logInSuccess = true;
            }
            else
            {
                simpleLogin = true;
                registerPage.SetActive(true);
                registerPage.transform.GetChild(0).GetChild(0).gameObject.SetActive(false); // back버튼 없애기
                registerPage.transform.GetChild(1).GetChild(0).gameObject.SetActive(false);
                registerPage.transform.GetChild(1).GetChild(2).gameObject.SetActive(true);
            }
        }
        yield return null;
    }
}

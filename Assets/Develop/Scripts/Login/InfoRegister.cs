﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using JetBrains.Annotations;
using System;

#if UNITY_ANDROID && !UNITY_EDITOR
using DeadMosquito.AndroidGoodies;
#elif UNITY_IOS && !UNITY_EDITOR
using DeadMosquito.IosGoodies;
#endif


public class InfoRegister : DataRegister
{
    public InputField input;

    public Button maleBTN;
    public Button femaleBTN;

    int gender = 0; // 0 : NONE   1: MALE   2: FEMALE

    public Color selected;
    public Color unselected;

    public Sprite maleON;
    public Sprite maleOFF;

    public Sprite femaleON;
    public Sprite femaleOFF;
    private void Start()
    {
        Init();
    }
    public override void Init()
    {
        gender = 0;
        maleBTN.image.color = unselected;
        maleBTN.image.sprite = maleOFF;

        femaleBTN.image.color = unselected;
        femaleBTN.image.sprite = femaleOFF;


        today = DateTime.Today;
        birthDate = today;

        birthDay.text = today.Year + "년 " + today.Month + "월 " + today.Day + "일";
        SetNotification("");
        SetNextBTNActive(false);
    }
    public void ChangeType(bool male)
    {
        if (male && (gender == 1))
            return;

        if (!male && (gender == 2))
            return;

        if (male)
        {
            gender = 1;
            maleBTN.image.color = selected;
            maleBTN.image.sprite = maleON;
            femaleBTN.image.color = unselected;
            femaleBTN.image.sprite = femaleOFF;
        }
        else
        {
            gender = 2;
            femaleBTN.image.color = selected;
            femaleBTN.image.sprite = femaleON;
            maleBTN.image.color = unselected;
            maleBTN.image.sprite = maleOFF;
        }
        RegisterInfo();
        CheckNextBTNAvailable();
    }
    protected override void RegisterInfo()
    {
        if(gender == 1)
            LoginManager.GetInstance().userInfo.gender = "Male";
        else if (gender == 2)
            LoginManager.GetInstance().userInfo.gender = "Female";

        LoginManager.GetInstance().userInfo.birthDate = birthDate.Year +"-"+birthDate.Month +"-"+birthDate.Day;
    }
    

    DateTime today;
    DateTime birthDate;
    public Text birthDay;
    public Text notification;
    const int MIN_AGE = 7;
    const int MAX_AGE = 100;
    public GameObject editorDateInput;


    public void ShowDatePicker()
    {
#if UNITY_EDITOR
        editorDateInput.SetActive(true);
        var now = today;
#elif UNITY_ANDROID && !UNITY_EDITOR
        AGDateTimePicker.ShowDatePicker(today.Year, today.Month, today.Day, OnDatePicked, OnDatePickCancel);
#elif UNITY_IOS && !UNITY_EDITOR
        IGDateTimePicker.ShowDatePickerWithRestrains(today.Year, today.Month, today.Day, iOSDatePicked,
            OnDatePickCancel, today.Year - MAX_AGE, 1, 1, today.Year, today.Month, today.Day);
#endif
    }
    public void iOSDatePicked(DateTime date)
    {
        birthDate = date;
        birthDay.color = new Color(0, 0, 0, 1);
        birthDay.text = birthDate.Year + "년 " + birthDate.Month + "월 " + birthDate.Day + "일";

        RegisterInfo();
        CheckNextBTNAvailable();
    }

    public void OnDatePicked(int year, int month, int day)
    {
        birthDay.color = new Color(0, 0, 0, 1);
        birthDate = new DateTime(year, month, day);
        birthDay.text = birthDate.Year + "년 " + birthDate.Month + "월 " + birthDate.Day + "일";

        RegisterInfo();
        CheckNextBTNAvailable();
    }
    public void CheckBirth()
    {
        if (input.text.Length < 8)
        {
            SetNotification("생년월일 8자리를 입력해주세요");
            nextButton.interactable = false;
        }
        else
            CheckDate();
    }
    public void CheckDate()
    {
        string date = input.text;
        // yyyy mm dd
        date = date.Insert(6, "-");
        date = date.Insert(4, "-");
        DateTime.TryParse(date, out birthDate);

        RegisterInfo();
        CheckNextBTNAvailable();
    }
    public void OnDatePickCancel()
    {
        OnDatePicked(today.Year, today.Month, today.Day);
        birthDay.color = new Color(0.5f, 0.5f, 0.5f, 1);
    }

    public override void CheckNextBTNAvailable()
    {
        if (gender == 0)
        {
            nextButton.interactable = false;
            return;
        }

        TimeSpan timeSpan = birthDate.Subtract(today);

        if (timeSpan.TotalDays > - 365 * MIN_AGE)
        {
            SetNotification("생년월일을 정확하게 입력해주세요");
            nextButton.interactable = false;
        }
        else if (timeSpan.TotalDays < - 365 * MAX_AGE)
        {
            SetNotification("생년월일을 정확하게 입력해주세요");
            nextButton.interactable = false;
        }
        else
        {
            SetNotification("");
            nextButton.interactable = true;
        }

    }
    void SetNotification(string note)
    {
        notification.text = note;
    }
}

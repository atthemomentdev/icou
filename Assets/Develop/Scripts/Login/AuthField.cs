﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AuthField : MonoBehaviour
{
    InputField prev;
    InputField curr;
    InputField next;
    Color unfilled = new Color(0.784f, 0.784f, 0.784f,1);
    Color filled = new Color(0.309f,0.450f,0.349f,1);
    public PWRegister regiester;

    private void Start()
    {
        curr = this.GetComponent<InputField>();
        curr.caretColor = new Color(0, 0, 0, 0);
        int index = this.transform.GetSiblingIndex();
        if (index > 0)
            prev = this.transform.parent.GetChild(index - 1).GetComponent<InputField>();
        else
            prev = null;
        if (index < this.transform.parent.childCount - 1)
            next = this.transform.parent.GetChild(index + 1).GetComponent<InputField>();
        else
            next = null;
    }
    private void Update()
    {
        if (curr.text.Length > 0)
        {
            curr.image.color = filled;
        }
        else
        {
            curr.image.color = unfilled;
        }

    }
    public void Check()
    {
        if(curr.text.Length > 1)
        {
            if(next != null)
            {
                char a = curr.text[0];
                char b = curr.text[1];

                curr.text = a.ToString();

                curr.DeactivateInputField();
                next.ActivateInputField();
                next.text = b.ToString();

                StartCoroutine(MoveEnd(next));
            }
        }
        else if (curr.text.Length == 0)
        {
            if(prev != null)
            {
                curr.DeactivateInputField();
                prev.ActivateInputField();

                StartCoroutine(MoveEnd(prev));
            }
        }
        regiester.CheckNextBTNAvailable();
    }
    IEnumerator MoveEnd(InputField f)
    {
        yield return new WaitForEndOfFrame();
        
        f.MoveTextEnd(true);

        yield return null;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NetworkingButton : MonoBehaviour
{
    GameObject indicator;
    Button btn;
    [HideInInspector]
    public bool clicked = false;
    private void Start()
    {
        btn = this.GetComponent<Button>();
    }
    public void StartLoad()
    {
        clicked = true;
        indicator = Instantiate(ApplicationManager.GetInstance().loadingIndicator, this.transform);
        this.transform.GetChild(0).gameObject.SetActive(false);
    }
    public void FinishLoad()
    {
        clicked = false;
        this.transform.GetChild(0).gameObject.SetActive(true);
        if (indicator)
            GameObject.Destroy(indicator);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PWRegister : DataRegister
{
    public Text userID;
    public InputField pwField;
    public InputField pwConfirmField;

    public Text notification;
    
    public override void Init()
    {
        pwField.text = "";
        pwConfirmField.text = "";
        userID.text = "";
        SetNotification("");

        pwConfirmField.interactable = false;
        SetNextBTNActive(false);
    }
    Coroutine co;
    private void OnEnable()
    {
        Init();
    }
    public void CheckPWConfirmFieldAvaileable()
    {
        pwConfirmField.text = "";
        if (pwField.text.Length > 7)
        {
            pwConfirmField.interactable = true;
            SetNotification("");
        }
        else
        {
            if (pwField.text.Length != 0)
                SetNotification("비밀번호는 8자리 이상이어야 합니다");
            pwConfirmField.interactable = false;
        }
    }
    public override void CheckNextBTNAvailable()
    {
        if (pwField.text == pwConfirmField.text)
        {
            SetNotification("");
            SetNextBTNActive(true);
        }
        else
        {
            SetNotification("비밀번호가 일치하지 않습니다");

            SetNextBTNActive(false);
        }
    }
    void SetNotification(string note)
    {
        notification.text = note;
    }
}

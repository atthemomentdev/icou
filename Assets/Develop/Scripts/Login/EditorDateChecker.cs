﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EditorDateChecker : MonoBehaviour
{
    public void Check()
    {
        int year = int.Parse(this.transform.GetChild(0).GetComponent<InputField>().text);
        int month = int.Parse(this.transform.GetChild(1).GetComponent<InputField>().text);
        int day = int.Parse(this.transform.GetChild(2).GetComponent<InputField>().text);
        
        this.transform.parent.GetComponent<InfoRegister>().OnDatePicked(year, month, day);
    }
}

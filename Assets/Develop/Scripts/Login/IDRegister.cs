﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IDRegister : DataRegister
{
    public InputField emailBox;

    public Text notification;
    
    public override void Init()
    {
        emailBox.text = "";
        notification.text = "";
        SetNextBTNActive(false);
    }
    public void Check()
    {
        StartCoroutine(LoginManager.GetInstance().CheckEmailAvailable(emailBox.text.ToString()));
    }
    private void Update()
    {
        if(LoginManager.GetInstance().emailCheck)
        {
            LoginManager.GetInstance().SetButtonOn();
            LoginManager.GetInstance().emailCheck = false;
            if (LoginManager.GetInstance().emailAvailable)
            {
                Register();
                notification.text = "";
            }
            else
            {
                notification.text = "이미 사용중인 이메일 주소 입니다";
            }
        }
    }

    public override void CheckNextBTNAvailable()
    {
        if (emailBox.text.Length > 0)
            SetNextBTNActive(true);
        else
            SetNextBTNActive(false);
    }

    protected override void RegisterInfo()
    {
        LoginManager.GetInstance().userInfo.email = emailBox.text.ToString();
    }
}

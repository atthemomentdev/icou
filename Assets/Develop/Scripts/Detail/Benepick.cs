﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Benepick : Content
{
    public static int BenepickCount = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void SetInfo(string content)
    {

        if (this.transform.GetChild(0).GetComponent<Text>() != null)
        {
            if (content == "주차장 有")
                content = "주차 가능";

            this.transform.GetChild(0).GetComponent<Text>().text = content;
        }
        
        RawImage icon = this.transform.GetChild(1).GetComponent<RawImage>();
        BenepickManager mgr = this.transform.parent.GetComponent<BenepickManager>();
        switch (content)
        {
            case "혼밥하기 좋음":
                icon.texture = mgr.icons[1];
                break;
            case "분위기 좋음":
                icon.texture = mgr.icons[2];
                break;
            case "공부하기 좋음":
                icon.texture = mgr.icons[3];
                break;
            case "테이크아웃 가능":
                icon.texture = mgr.icons[4];
                break;
            case "배달 가능":
                icon.texture = mgr.icons[5];
                break;
            case "instafood":
                icon.texture = mgr.icons[8];
                break;
            case "instacafe":
                icon.texture = mgr.icons[9];
                break;
            case "주차 가능":
                icon.texture = mgr.icons[10];
                break;
            case "혼술하기 좋음":
                icon.texture = mgr.icons[11];
                break;
            case "무첨가 생과일 음료":
                icon.texture = mgr.icons[12];
                break;
            case "이색 데이트":
                icon.texture = mgr.icons[14];
                break;
            case "단체석":
                icon.texture = mgr.icons[15];
                break;
            case "가성비 쵝오":
                icon.texture = mgr.icons[16];
                break;
            case "후식 제공":
                icon.texture = mgr.icons[17];
                break;
            default:
                icon.texture = mgr.icons[0];
                break;
        }
    }
}

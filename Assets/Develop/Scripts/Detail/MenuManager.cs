﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : Content
{
    [SerializeField]
        public GameObject menu_cat_img;
    [SerializeField]
        public GameObject menu_name;
    [SerializeField]
        public GameObject menu_info_img;
    [SerializeField]
        public GameObject blank;

    public List<Content> loaded = new List<Content>();
    private void OnEnable()
    {
        SetInfo(ApplicationManager.GetInstance().infoManager.shop._menus);
    }
    public override void SetInfo(List<Menu> menuList)
    {
        loaded.Clear();
        loaded = new List<Content>();
        foreach (var menu in menuList)
        {
            Content new_content;
            Instantiate(blank, this.transform);

            if(menu.categoryImage.URL != "NULL")
            {
                new_content = Instantiate(menu_cat_img, this.transform).GetComponent<Content>();
                new_content.Set(menu.categoryImage);
                loaded.Add(new_content);
            }

            new_content = Instantiate(menu_name, this.transform).GetComponent<Content>();
            new_content.Set(menu.categoryName);
            loaded.Add(new_content);

            new_content = Instantiate(menu_info_img, this.transform).GetComponent<Content>();
            new_content.Set(menu.menuBoardImage);
            loaded.Add(new_content);
        }
    }
    private void OnDisable()
    {
        Clear();
    }
    void Clear()
    {
        for (int i = 0; i < this.transform.childCount; i++)
            GameObject.Destroy(this.transform.GetChild(i).gameObject);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextBox : Content
{
    Text text_ui;

    [HideInInspector]
    public int line;


    private void Start()
    {
    }
    public override void SetInfo(string content) {
        text_ui = this.GetComponent<Text>();
        text_ui.text = SplitLine(content);
        isLoaded = true;
    }
    string SplitLine(string content)
    {
        line = 1;
        if (content == "")
            return "";
        string[] tmp = content.Split(new string[] { "\\r\\n" }, System.StringSplitOptions.None);

            string a = tmp[0];
            for (int i = 1; i < tmp.Length; i++)
            {
                a += "\n";
                a += tmp[i];
                line++;
            }
            return a;
    }
}

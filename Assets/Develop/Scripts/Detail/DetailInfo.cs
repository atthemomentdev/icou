﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DetailInfo : Content
{
    private void Start()
    {

    }
    public override void SetInfo(string content)
    {
        Transform child = this.transform.GetChild(1);
        child.GetComponent<Text>().text = content;

        if (child.GetComponent<TextBox>() != null)
        {
            Vector2 ratio = ApplicationManager.GetInstance().GetRatio();

            TextBox textBox = child.GetComponent<TextBox>();
            textBox.SetInfo(content);
        }
        isLoaded = true;
    }
    private void Update()
    {
        ControlSpaceSize();
    }
    void ControlSpaceSize()
    {
        int i = this.transform.GetSiblingIndex();
        float size = this.transform.GetChild(1).GetComponent<RectTransform>().sizeDelta.y;
        this.transform.parent.GetChild(i + 1).GetComponent<RectTransform>().sizeDelta = new Vector2(0, size);
    }

}

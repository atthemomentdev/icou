﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class BenefitImage : MonoBehaviour
{
    [HideInInspector]
    public bool isLoaded = false;
    protected TextureContainer image;
    public Color load_color = new Color(0.94f, 0.94f, 0.94f, 1.0f);
    public Color after_color = new Color(1, 1, 1, 1);

    private void OnEnable()
    {
        if (this.transform.GetComponent<RawImage>().texture == null)
        {
            if(image != null)
                LoadImage(image);
        }
    }

    public void LoadImage(TextureContainer img)
    {
        image = img;
        if (img.texture == null)
        {
            this.GetComponent<RawImage>().texture = null;
            StartCoroutine(ApplicationManager.GetInstance().LoadImg(this, img));
        }
        else
        {
            Set(img);
            isLoaded = true;
        }
    }

    public virtual void Set(TextureContainer img)
    {
        if (img.URL != image.URL)
            return;
        if (this.transform.GetComponent<RawImage>() != null)
        {
            image = img;
            this.transform.GetComponent<RawImage>().texture = img.texture;
            AfterSetImage();
        }
    }

    public virtual void AfterSetImage()
    {
        isLoaded = true;
        this.transform.GetComponent<RawImage>().color = after_color;
    }
}
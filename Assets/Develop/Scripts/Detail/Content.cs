﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Content : MonoBehaviour
{
    public bool innerContent = false;
    // Start is called before the first frame update
    [HideInInspector]
    public bool isLoaded = false;

    public void Set(string content = "") {
        SetInfo(content);
        if(isLoaded && (!innerContent))
        {
            ApplicationManager.GetInstance().infoManager.LoadNextContent();
        }
    }

    public void Set(TextureContainer tex)
    {
        if (this.GetComponent<BenefitImage>() == null)
            return;

        BenefitImage img = this.GetComponent<BenefitImage>();

        img.LoadImage(tex);

        if (img.isLoaded && (!innerContent))
        {
            ApplicationManager.GetInstance().infoManager.LoadNextContent();
        }
    }

    public void Set(List<Menu> menuList)
    {
        SetInfo(menuList);
    }

    public void Set(List<string> benepickList)
    {
        SetInfo(benepickList);
        if (isLoaded && (!innerContent))
        {
            ApplicationManager.GetInstance().infoManager.LoadNextContent();
        }
    }

    public virtual void Init()
    {
        if (this.GetComponent<Text>() != null)
        {
            if(this.transform.childCount < 1)
                this.GetComponent<Text>().text = "";
        }
        if (this.GetComponent<RawImage>() != null)
        {
            this.GetComponent<RawImage>().texture = null;
            
        }
    }

    public virtual void SetInfo(List<string> benepickList) { }

    public virtual void SetInfo(List<Menu> menuList) { }

    public virtual void SetInfo(string content) {
        if (this.GetComponent<RawImage>() != null)
        {
            if (content != "")
            {
                isLoaded = true;
                //this.GetComponent<RawImage>().color = load_color;
                //ApplicationManager.GetInstance().LoadImage(this, content);
            }
            else
            {
                isLoaded = true;
            }
        }
        else
        {
            if(this.GetComponent<Text>() != null)
            {
                this.transform.GetComponent<Text>().text = content;
            }
            isLoaded = true;
        }
    }

}

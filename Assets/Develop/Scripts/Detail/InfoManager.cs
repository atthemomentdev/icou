﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoManager : MonoBehaviour
{
    //public Text header_title;
    public LikeBTN like;
    public Content benepick;
    public Content menuMgr;

    public Text menuBTNText;
    public GameObject closeBTN;
    [HideInInspector]
    public Shop shop;

    [HideInInspector]
    public int target;
    Queue<string> contents = new Queue<string>(15);

    // Start is called before the first frame update
    void Start()
    {
        target = 0;
    }

    public void SetInfo(Shop tmp_shop)
    {
        this.transform.localScale = new Vector3(1, 1, 1);
        shop = tmp_shop;
        like.InitLike(shop);
        string info = "";
        if (tmp_shop._discountType == "서비스")
            info = "본 매장 주문 시 오른쪽 하단의 쿠폰버튼을 누르고, 화면을 제시하면 혜택이 적용됩니다.";
        else
            info = "본 매장 결제 시 오른쪽 하단의 쿠폰버튼을 누르고, 화면을 제시하면 혜택이 적용됩니다.";
        
        InitMenu();

        if(contents != null)
            contents.Clear();

        contents = new Queue<string>(11);

        contents.Enqueue(shop._brief);
        contents.Enqueue(shop._name);
        contents.Enqueue("shop._introImg");
        contents.Enqueue(shop._benefit);
        contents.Enqueue("shop._benePick");
        contents.Enqueue("shop._menus");
        contents.Enqueue(shop._address);
        contents.Enqueue(shop._contact);
        contents.Enqueue(shop._openTime);
        contents.Enqueue(shop._otherInfo);
        contents.Enqueue(info);

        target = 0;
        LoadNextContent();
    }
    void InitMenu()
    {
        if (menuMgr.gameObject.activeSelf)
        {
            menuBTNText.text = "메뉴 보기";
            menuBTNText.transform.GetChild(0).transform.localScale = new Vector3(1, 1, 1);
            menuMgr.gameObject.SetActive(false);
            closeBTN.SetActive(false);
        }
    }
    public void MenuLoad()
    {
        if (menuMgr.gameObject.activeSelf)
        {
            menuBTNText.text = "메뉴 보기";
            menuBTNText.transform.GetChild(0).transform.localScale = new Vector3(1, 1, 1);
            menuMgr.gameObject.SetActive(false);
            closeBTN.SetActive(false);
        }
        else
        {
            menuBTNText.text = "메뉴 접기";
            menuBTNText.transform.GetChild(0).transform.localScale = new Vector3(1, 1, 1);
            menuMgr.gameObject.SetActive(true);
            closeBTN.SetActive(true);
        }
    }
    IEnumerator StartLoad()
    {
        target++;
        if (target >= this.transform.childCount)
        {
            yield return null;
        }
        else
        {
            if (this.transform.GetChild(target).GetComponent<Content>() != null)
            {
                if(contents.Count > 0)
                {
                    string content = contents.Dequeue();
                    switch (content)
                    {
                        case "shop._introImg":
                            this.transform.GetChild(target).GetComponent<Content>().Set(shop._introImg);
                            break;
                        case "shop._benePick":
                            this.transform.GetChild(target).GetComponent<Content>().Set(shop._benePicks);
                            break;
                        default:
                            this.transform.GetChild(target).GetComponent<Content>().Set(content);
                            break;
                    }
                }
            }
            else
            {
                LoadNextContent();
            }
            yield return null;
        }
    }
    public void LoadNextContent()
    {
        if (ApplicationManager.GetInstance().detailPage.isOn == false)
            return;
        StartCoroutine(StartLoad());
    }
    public void ReSet()
    {
        target = 0;
        for (int i = 0; i < menuMgr.transform.childCount ; i ++)
        {
            GameObject.Destroy(menuMgr.transform.GetChild(i).gameObject);
            menuMgr.GetComponent<MenuManager>().loaded.Clear();
            menuMgr.GetComponent<MenuManager>().loaded = new List<Content>();
        }
        for (int i = 0; i < benepick.transform.childCount; i++)
        {
            GameObject.Destroy(benepick.transform.GetChild(i).gameObject);
            benepick.GetComponent<BenepickManager>().loaded.Clear();
            benepick.GetComponent<BenepickManager>().loaded = new List<Content>();
        }
        for (int i = 0; i < this.transform.childCount; i++)
        {
            if (this.transform.GetChild(i).GetComponent<Content>() != null)
                this.transform.GetChild(i).GetComponent<Content>().Init();
        }
    }

    public void Call()
    {
        ApplicationManager.GetInstance().PhoneCall(shop._contact);
    }

    public void ShowMap()
    {
        ApplicationManager.GetInstance().OpenGoogleMap(shop._address);
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BenepickManager : Content
{
    [SerializeField]
    public GameObject pick;

    [HideInInspector]
    public List<Content> loaded = new List<Content>();

    [SerializeField]
    public List<Texture> icons;
    public override void SetInfo(List<string> benepickList)
    {
        loaded.Clear();
        loaded = new List<Content>();
        Content new_content;
        Benepick.BenepickCount = benepickList.Count;
        if(Benepick.BenepickCount >= 4)
            this.GetComponent<GridLayoutGroup>().constraintCount = 2;
        else
            this.GetComponent<GridLayoutGroup>().constraintCount = 1;

        foreach (var benepick in benepickList)
        {
            new_content = Instantiate(pick, this.transform).GetComponent<Content>();
            new_content.Set(benepick);

            loaded.Add(new_content);
        }
        
        ApplicationManager.GetInstance().infoManager.LoadNextContent();
    }
}


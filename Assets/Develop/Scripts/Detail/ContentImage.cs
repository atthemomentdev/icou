﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContentImage : BenefitImage
{
    public override void Set(TextureContainer img)
    {
        if (image.URL != img.URL)
            return;
        base.Set(img);

        if (img.texture == null)
            return;

        RectTransform rect = this.GetComponent<RectTransform>();
        // x : y = x' : y'
        // y' = x' * y / x
        if (ApplicationManager.GetInstance() == null)
            return;

        Vector2 ratio = ApplicationManager.GetInstance().GetRatio();
        

        Vector2 imgSize = new Vector2();

        imgSize.x = ApplicationManager.GetInstance().WIDTH;
        imgSize.y = imgSize.x * img.texture.height / img.texture.width;

        rect.sizeDelta = imgSize; 
    }
    public override void AfterSetImage()
    {
        base.AfterSetImage();
        ApplicationManager.GetInstance().infoManager.LoadNextContent();
    }
}

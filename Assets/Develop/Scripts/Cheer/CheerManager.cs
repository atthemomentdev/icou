﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class CheerManager : MonoBehaviour
{
    public Text shopName;
    public Text shopName2;
    public Text count;
    public InputField cheer;
    public NetworkingButton submitBTN;
    public DetailPage detail;
    public Button nextBTN;
    public Button prevBTN;
    public Text pageIndex;
    public Transform cheerContent;
    public CheerBoard cheerBoard;
    
    
    private int page;
    private int messageCount;

    public void CheckAvailable()
    {
        StartCoroutine(CheckCheerAvailable());
    }
    public void Check()
    {
        if (cheer.text.Length > 5)
            submitBTN.GetComponent<Button>().interactable = true;
        else
            submitBTN.GetComponent<Button>().interactable = false;
    }
    public void Submit()
    {
        StartCoroutine(UploadCheer());
    }
    private void Init()
    {
        cheer.text = "";
        cheer.shouldHideMobileInput = true;


        submitBTN.GetComponent<Button>().interactable = false;
    }
    void SubmitCheerMsg(bool todayFirstUse = true)
    {
        if(todayFirstUse)
            ApplicationManager.GetInstance().ShowToast("매장을 응원해주셔서 감사합니다 :)");
        else
            ApplicationManager.GetInstance().ShowToast("오늘은 이미 해당 매장 응원을 하셨습니다");

        ApplicationManager.GetInstance().CouponClick(detail. shopBTN.shop, todayFirstUse);

        SetButtonOn();
        cheerBoard.gameObject.SetActive(false);
    }

    public void Skip() {
        SubmitCheerMsg(false);
    }
    public void Show()
    {
        Init();
        cheerBoard.transform.GetChild(1).gameObject.SetActive(true);
        cheer.ActivateInputField();
        shopName.text = detail.shopBTN.shop._name;
    }

    IEnumerator CheckCheerAvailable()
    {
        cheerBoard.transform.GetChild(1).gameObject.SetActive(false);

        string[] name = { "user_id", "shop_id"};
        string[] pdata = { ApplicationManager.GetInstance().auth.CurrentUser.UserId.ToString(), detail.shopBTN.shop._id};
        pData.SetValue(name, pdata);
        UnityWebRequest keyUrl = pData.ActiveKey("https://benefit.atthemomentkor.com/cheer_check.php");
        keyUrl.useHttpContinue = false;
        keyUrl.chunkedTransfer = false;
        AsyncOperation async = keyUrl.SendWebRequest();

        while (!async.isDone)
        {
            yield return null;
        }
        if (keyUrl.isNetworkError || keyUrl.isHttpError)
        {
            Debug.Log("네트워크 연결에 실패했습니다.");
        }
        else
        {
            pData.RETCODE = keyUrl.downloadHandler.text;
            Debug.Log(pData.RETCODE);
            if (pData.RETCODE == "\navailable")
                Show();
            else
                Skip();
        }
    }
    IEnumerator UploadCheer()
    {
        SetButtonOff();
        string[] name = { "user_id", "shop_id", "cheer" };

        string[] pdata = { ApplicationManager.GetInstance().auth.CurrentUser.UserId.ToString(), detail.shopBTN.shop._id, cheer.text };
        pData.SetValue(name, pdata);
        UnityWebRequest keyUrl = pData.ActiveKey("https://benefit.atthemomentkor.com/cheer_upload.php");
        keyUrl.useHttpContinue = false;
        keyUrl.chunkedTransfer = false;
        AsyncOperation async = keyUrl.SendWebRequest();
        while (!async.isDone)
        {
            yield return null;
        }

        pData.RETCODE = keyUrl.downloadHandler.text;

        SubmitCheerMsg(true);
    }
    public void StartRemoveCheer(string cheer_id)
    {
        StartCoroutine(RemoveCheer(cheer_id));
    }
    IEnumerator RemoveCheer(string cheer_id)
    {
        string name = "cheer_id";
        string pdata = cheer_id;
        pData.SetValue(name, pdata);
        UnityWebRequest keyUrl = pData.ActiveKey("https://benefit.atthemomentkor.com/cheer_remove.php");
        keyUrl.useHttpContinue = false;
        keyUrl.chunkedTransfer = false;
        AsyncOperation async = keyUrl.SendWebRequest();
        while (!async.isDone)
        {
            yield return null;
        }
    }

    public void StartDownloadCheer()
    {
        StartCoroutine(DownloadCheer());
    }

    IEnumerator DownloadCheer()
    {
        cheerContent.GetComponent<CanvasGroup>().alpha = 0;
        string name = "shop_id";
        string pdata = detail.shopBTN.shop._id;
        pData.SetValue(name, pdata);
        UnityWebRequest keyUrl = pData.ActiveKey("https://benefit.atthemomentkor.com/cheer_download.php");
        keyUrl.useHttpContinue = false;
        keyUrl.chunkedTransfer = false;
        AsyncOperation async = keyUrl.SendWebRequest();

        while (!async.isDone)
        {
            yield return null;
        }

        if (keyUrl.isNetworkError || keyUrl.isHttpError)
        {
            ApplicationManager.GetInstance().SetNotification("네트워크 연결에 실패했습니다.");
        }
        else
        {
            pData.RETCODE = keyUrl.downloadHandler.text;

            messageCount = detail.shopBTN.shop.AddCheers(pData.RETCODE);

            InitPageInfo();
        }
    }

    public void SetButtonOn()
    {
        if (submitBTN != null)
        {
            submitBTN.FinishLoad();
        }
    }

    void SetButtonOff()
    {
        if (submitBTN != null)
            submitBTN.StartLoad();
    }

    int pageMax;

    void InitPageInfo()
    {
        shopName2.text = detail.shopBTN.shop._branch + " " + detail.shopBTN.shop._name;
        count.text = messageCount.ToString();
        CheckEmpty();
        page = 1;
        pageMax = messageCount / 11 + 1;
        SetPage();
    }
    void CheckEmpty()
    {
        if (messageCount == 0)
        {
            cheerContent.GetChild(10).gameObject.SetActive(false);
            cheerContent.GetChild(11).gameObject.SetActive(true);
        }
        else
        {
            cheerContent.GetChild(10).gameObject.SetActive(true);
            cheerContent.GetChild(11).gameObject.SetActive(false);
        }
    }
    void SetPage()
    {
        cheerContent.GetComponent<RectTransform>().position = new Vector2(0, 0);

        if(page <= 1)
            prevBTN.interactable = false;
        else
            prevBTN.interactable = true;

        if (page >= pageMax)
        {
            int visibleBlock = messageCount % 10;
            for(int i = 0; i < 10; i++)
            {
                if(i < visibleBlock)
                    cheerContent.GetChild(i).gameObject.SetActive(true);
                else
                    cheerContent.GetChild(i).gameObject.SetActive(false);

            }
            nextBTN.interactable = false;
        }
        else
        {
            for (int i = 0; i < 10; i++)
            {
                cheerContent.GetChild(i).gameObject.SetActive(true);
            }
            nextBTN.interactable = true;
        }
        SetMessages();
        pageIndex.text = page.ToString() + " / " + pageMax.ToString();
        cheerContent.GetComponent<CanvasGroup>().alpha = 1;
    }
    void SetMessages()
    {
        int count = 0;
        for(int i = 0; i < 10; i ++)
        {
            if (cheerContent.GetChild(i).gameObject.activeSelf == true)
            {
                int index = messageCount - (page - 1) * 10 - count;
                CheerMessage msg = cheerContent.GetChild(i).GetComponent<CheerMessage>();
                
                if (msg != null)
                    msg.SetUI(detail.shopBTN.shop._cheers[index - 1], index);

                count++;
            }
        }
    }
    public void PageNext()
    {
        Debug.Log("NEXT");
        page++;
        SetPage();
    }
    public void PagePrev()
    {
        Debug.Log("PREV");
        page--;
        SetPage();
    }
}

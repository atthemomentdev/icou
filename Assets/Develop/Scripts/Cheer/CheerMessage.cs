﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheerMessage : MonoBehaviour
{
    public Text number;
    public Text contents;
    public Text order;
    GameObject removeBtn;
    CheerDetail cheer;
    public CheerDetailBoard detailBoard;
    public void SetUI(CheerDetail detail, int index)
    {
        if (detail == null)
            return;

        cheer = detail;
        number.text = "#"+index.ToString();
        if (cheer.contents.Length > 18)
        {
            int nl = 18;
            for(int i = 0; i < 18; i++)
            {
                if (cheer.contents[i] == '\\')
                {
                    nl = i;
                    break;
                }
            }
            contents.text = cheer.contents.Substring(0, nl);
            contents.transform.GetChild(0).gameObject.SetActive(true);
        }
        else
        {
            contents.text = cheer.contents;
            contents.transform.GetChild(0).gameObject.SetActive(false);
        }
        order.text = cheer.nickName + "님의 " + cheer.order.ToString() +"번째 방문";
    }
    public void OpenDetail()
    {
        ApplicationManager.GetInstance().page_index = 1002;
        ApplicationManager.GetInstance().subwindow = detailBoard.gameObject;
        detailBoard.gameObject.SetActive(true);
        detailBoard.SetDetail(cheer);
    }
}

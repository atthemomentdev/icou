﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheerDetail
{
    //upload,userid,order,contents
    public string id;
    public string userId;
    public string nickName;
    public int order;
    public string contents;

    public void SetDetail(string cheerString)
    {
        // 데이터 RETCODE에 넣어놨으니 |로 끊어서(cheer별) #으로 또 끊으면(upload,userid,order,contents) 순서대로 데이터가 나옴!
        string[] tmp = cheerString.Split('#');
        id = tmp[0];
        userId = tmp[1];
        nickName = tmp[2].Substring(0, 4) + "****";

        order = int.Parse(tmp[3]);
        contents = tmp[4];

        Debug.Log("#" + id + "\nID: " + userId + "\nNickname: "+ nickName + "\nOrder: " + order +"\nContent: " + contents);
    }
}

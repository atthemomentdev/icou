﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheerBoard : MonoBehaviour
{
    public CheerManager cheerMgr;
    private void OnEnable()
    {
        ApplicationManager.GetInstance().page_index = 1000;
        ApplicationManager.GetInstance().subwindow = this.gameObject;
        cheerMgr.CheckAvailable();
    }
    private void OnDisable()
    {
        ApplicationManager.GetInstance().page_index = 3;
    }
}

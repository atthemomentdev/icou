﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.EventSystems;
using System.Windows;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using UnityEngine.UI;
using System.Web;
using System.IO;
using UnityEngine.SceneManagement;
using Facebook.Unity;

#if UNITY_ANDROID && !UNITY_EDITOR
using DeadMosquito.AndroidGoodies;
#elif UNITY_IOS && !UNITY_EDITOR
using DeadMosquito.IosGoodies;
#endif
using JetBrains.Annotations;

public class ApplicationManager : MonoBehaviour
{
    [HideInInspector]
    static public Color selectedColor = new Color(0.1875f, 0.4196f, 0.3451f);

    [HideInInspector]
    static public Color unSelectedColor = new Color(0.5f, 0.5f, 0.5f);

    [HideInInspector]
    static public Color yellow = new Color(1f, 0.796f, 0,1f);

    [SerializeField]
    public GameObject alert;
    [HideInInspector]
    public bool ADMIN_MODE = false;
    public FrontAD frontAD;
    public float editorNotchHead;
    public float editorNotchFoot;

    public GameObject subwindow;

    [SerializeField]
    public GameObject loadingIndicator;

    public Toast toastMsg;

    [HideInInspector]
    public float notchUp
    {
        get {
            int ver = ApplicationManager.GetSDKInt();
            if (ver >= 29)
                return editorNotchHead;
            else
            {
#if UNITY_EDITOR
                return editorNotchHead;
#else
                return _camera.pixelHeight - Screen.safeArea.yMax;
#endif
            }
        }
    }
    [HideInInspector]
    public float notchDown
    {
        get
        {
            int ver = ApplicationManager.GetSDKInt();
            if (ver >= 29)
                return editorNotchFoot;
            else
            {
#if UNITY_EDITOR
                return editorNotchFoot;
#else
                return Screen.safeArea.yMin;
#endif
            }
        }
    }
    public PlaceManager placeManager;
    [HideInInspector]
    public int WIDTH = 1080;
    [HideInInspector]
    public int HEIGHT = 1920;
    const string VERSION = "0.1ver";
    [HideInInspector]
    public int page_index = 0;
    private static ApplicationManager instance;
    public Camera _camera;
    public ShopManager shopManager;
    public MyScrollRect mainScroll;
    public CategoryManager categoryManager;
    public DetailPage detailPage;
    public CouponPage couponPage;
    public ADPage adPage;
    public MainPage mainPage;
    public LoadingPage loadingPage;
    public InfoManager infoManager;
    public LoginManager loginManager;
    public SearchManager searchManager;
    public MyPageManager myPageManager;
    public AppleLoginManager appleLogin;
    public MainPageBTN homeButton;
    public MainPageBTN placeButton;
    public EventSystem eventSystem;
    public RequestManager requsetMgr;
    public SlideBanner slide;
    public User user;
    [HideInInspector]
    public Vector2 _screen
    {
        get {
#if UNITY_EDITOR
            return new Vector2(_camera.pixelWidth, _camera.pixelHeight);
#elif UNITY_ANDROID && !UNITY_EDITOR
            return new Vector2(Screen.safeArea.width, Screen.safeArea.height);
#elif UNITY_IOS && !UNITY_EDITOR
            return new Vector2(Screen.safeArea.width, Screen.safeArea.height);
#endif
        }
    }
    public RectTransform canvas;
    [HideInInspector]
    public Vector2 _originScreen = new Vector2(1080,1920);
    [SerializeField]
    public GameObject progressCircle;
    const string main_site = "https://benefit.atthemomentkor.com/load_site_with_coordinate.php";
    string address;

    string URL_load_shop;
    string URL_load_banners;
    string URL_load_banner_contents;
    string URL_load_places;

    string URL_load_detail;
    string URL_update_hide;
    string shop_count;
    string URL_submit_request;
    string URL_policy;

    [HideInInspector]
    public Firebase.Auth.FirebaseAuth auth;

    [HideInInspector]
    public DatabaseReference mDatabaseRef;

    public string APIKey = "AIzaSyBnkjxsetwzJwnJFO7-ipu_8GI12w1y8HA";

    [HideInInspector]
    public BannerInfo current_banner_info;

    [HideInInspector]
    public BannerManager bannerManager; // 첫 페이지의 배너매니저!

    [HideInInspector]
    public List<PlaceInfo> places = new List<PlaceInfo>();

    [HideInInspector]
    public List<string> categories = new List<string>();

    [HideInInspector]
    public List<string> shop_infos = new List<string>();

    [HideInInspector]
    public List<string> shop_detail = new List<string>();

    public List<string> banners = new List<string>();

    public Text _log;
    public GameObject notification;

    public void SetNotification(string info)
    {
        notification.SetActive(true);
        notification.transform.GetChild(1).gameObject.SetActive(true);
        notification.transform.GetChild(0).GetComponent<Text>().text = info;
        bannerManager = this.transform.GetComponent<BannerManager>();
    }
    public void OffNotification()
    {
        notification.transform.GetChild(1).gameObject.SetActive(false);
        notification.SetActive(false);
    }
    [UsedImplicitly]
    public void ShowOneBtnDialog(string title, string message, string button, System.Action function)
    {
        if (function == null)
            function = NONE;
#if UNITY_EDITOR
        alert.gameObject.SetActive(true);
        alert.transform.GetChild(0).GetComponent<AlertMessage>().Alert(title, message, button, function);
#elif UNITY_ANDROID && !UNITY_EDITOR
        AGAlertDialog.ShowMessageDialog(title, message, button, function, null, AGDialogTheme.Light);
#elif UNITY_IOS && !UNITY_EDITOR
        IGDialogs.ShowOneBtnDialog(title, message, button, function);
#endif
    }

    [UsedImplicitly]
    public void ShowTwoBtnDialog(string title, string message, string button1, string button2, System.Action function1, System.Action function2)
    {
        if (function1 == null)
            function1 = NONE;
        if (function2 == null)
            function2 = NONE;

#if UNITY_EDITOR
        alert.gameObject.SetActive(true);
        alert.transform.GetChild(0).GetComponent<AlertMessage>().Alert(title, message, button1, function1, button2, function2);
#elif UNITY_ANDROID && !UNITY_EDITOR
        AGAlertDialog.ShowMessageDialog(title, message, button1, function1, button2, function2, null, AGDialogTheme.Light);
#elif UNITY_IOS && !UNITY_EDITOR
        IGDialogs.ShowTwoBtnDialog(title, message,button1, function1, button2, function2);
#endif
    }
    void NONE() { }

    IEnumerator Load_URL()
    {
        pData.SetValue("version", VERSION);
        UnityWebRequest keyUrl = pData.ActiveKey(main_site);

        keyUrl.useHttpContinue = false;
        keyUrl.chunkedTransfer = false;

        AsyncOperation async = keyUrl.SendWebRequest();
        
        while (!async.isDone)
        {
            yield return null;
        }
        if (keyUrl.isNetworkError || keyUrl.isHttpError)
        {
            ShowOneBtnDialog("네트워크 연결 실패","네트워크 연결에 실패했습니다. 인터넷 연결을 확인한 후 다시 시도해주세요","다시 시도",null);
        }
        else
        {
            pData.RETCODE = keyUrl.downloadHandler.text;

            string[] tmp = pData.RETCODE.Split('|');
            if(tmp[0] == "SERVER_REQUEST_FAILED")
            {
                if (loadingPage.gameObject.activeSelf)
                    loadingPage.SetNotification(tmp[1]);
            }
            else
            {
                if (tmp.Length > 0)
                {
                    if(auth.CurrentUser == null)
                    {
                        ADMIN_MODE = false;
                        URL_load_shop = tmp[1];
                    }
                    else if (auth.CurrentUser.Email == tmp[7])
                    {
                        ADMIN_MODE = true;
                        URL_load_shop = tmp[6];
                    #if UNITY_EDITOR
                        AnalyzeUserData();
                    #endif
                    }
                    else
                    {
                        ADMIN_MODE = false;
                        URL_load_shop = tmp[1];
                    }
                    address            =  tmp[0];
                    URL_load_detail    =  tmp[2];
                    shop_count         =  tmp[3];
                    URL_submit_request =  tmp[4];
                    APIKey             =  tmp[5];
                    URL_update_hide    =  tmp[8];
                    URL_load_places    =  tmp[9];
                    URL_policy         = tmp[10];
                }

                StartCoroutine(Load_Places());
            }
        }
    }

    // Start is called before the first frame update
    public System.DateTime accessTime = new System.DateTime();

    void ApplicationInit()
    {
        Application.targetFrameRate = 60;
        TouchScreenKeyboard.hideInput = true;
        SetDragThreshold(true);
        FacebookInit();
    }

    void FacebookInit()
    {
        FB.Init();
    }
    
    public void LogOut()
    {
        Firebase.Auth.FirebaseAuth.DefaultInstance.SignOut();
        loginManager.gameObject.SetActive(true);
        DataController.Instance.ClearData();
        InitPages();
    }
    public void ConfirmDeleteUserData()
    {
        ShowTwoBtnDialog("계정 삭제", "계정을 삭제하시면 관련된 데이터가 모두 삭제됩니다. 정말로 삭제하시겠습니까?", "삭제", "취소", DeleteUserData, null);
    }
    void DeleteUserAuth()
    {
        Firebase.Auth.FirebaseUser user = auth.CurrentUser;
        if (user != null)
        {
            continueFunc = false;
            user.DeleteAsync().ContinueWith(task => {
                if (task.IsCanceled)
                {
                    Debug.LogError("DeleteAsync was canceled.");
                    return;
                }
                if (task.IsFaulted)
                {
                    Debug.LogError("DeleteAsync encountered an error: " + task.Exception);
                    return;
                }
                continueFunc = true;
                Debug.Log("User deleted successfully.");
            });
        }
        StartCoroutine(ContinueFunction(DeleteFinish));
    }
    DataSnapshot remove_target;
    void DeleteUserData()
    {
        continueFunc = false;
        FirebaseDatabase.DefaultInstance.GetReference("users/" + auth.CurrentUser.UserId.ToString())
               .GetValueAsync().ContinueWith(task =>
               {
                   if (task.IsFaulted)
                   {
                       Debug.LogError(task.Exception.Data);
                       finishCheckUserData = true;
                   }
                   else if (task.IsCompleted)
                   {
                       remove_target = task.Result;
                   }
                   continueFunc = true;
               });
        StartCoroutine(ContinueFunction(DeleteUserAuth));
        StartCoroutine(DeleteUserDataQuery());
    }
    void DeleteFinish()
    {
        RemoveData();
        ShowOneBtnDialog("계정 삭제 완료", "계정이 성공적으로 삭제되었습니다. 그동안 아이쿠!를 이용해주셔서 감사했습니다 :)", "확인", LogOut);
    }
    void RemoveData()
    {
        if (remove_target == null)
            return;
        
        remove_target.Reference.RemoveValueAsync();
    }
    bool continueFunc = false;
    IEnumerator ContinueFunction(System.Action function)
    {
        while (continueFunc == false)
        {
            yield return null;
        }

        function();
        yield return null;
    }
    void InitPages()
    {
        placeButton.Click();
        placeManager.InitPage();
    }
    void Start()
    {
        ApplicationInit();
        FireBaseInit();
    }

    public void StartCheckAuth()
    {
        StartCoroutine(CheckAuth());
    }
    IEnumerator CheckAuth()
    {
        while (!finishInitFirebase)
        {
            CheckAuth();
            yield return null;
        }        

        if (dependencyAvaileable)
        {
            if (CheckAlreadyLogin() == true)
            {
                SetUserData();
#if !UNITY_EDITOR
                StartCoroutine(ActiveUser());
#endif
            }
            else
            {
                loginManager.gameObject.SetActive(true);
                SetEnvironment();
            }
        }
        else
            ShowOneBtnDialog("네트워크 연결 실패", "네트워크 연결에 실패했습니다. 인터넷 환경을 확인 한 후 다시 시도해주세요", "다시 시도", StartCheckAuth);
        yield return null;
    }
    bool applLoginIsAvailable = false;

    bool CheckAlreadyLogin()
    {
        if (appleLogin.CheckAppleLoginAlready())
        {
            if (auth.CurrentUser != null)
                return true;
            else
                return false;
        }
        else
            return false;
    }

    bool finishCheckUserData;
    bool result;
    void SetUserData()
    {
        finishCheckUserData = false;
        result = false;
        StartCoroutine(Load_UserData());
    }
    IEnumerator Load_UserData()
    {
        pData.SetValue("user_id", auth.CurrentUser.UserId.ToString());
        UnityWebRequest keyUrl = pData.ActiveKey("https://benefit.atthemomentkor.com/load_user.php");

        keyUrl.useHttpContinue = false;
        keyUrl.chunkedTransfer = false;

        AsyncOperation async = keyUrl.SendWebRequest();

        while (!async.isDone)
        {
            yield return null;
        }
        if (keyUrl.isNetworkError || keyUrl.isHttpError)
        {
            ShowOneBtnDialog("네트워크 연결 실패", "네트워크 연결에 실패했습니다. 인터넷 환경을 확인 한 후 다시 시도해주세요", "다시 시도", SetUserData);
        }
        else
        {
            pData.RETCODE = keyUrl.downloadHandler.text;

            string[] tmp = pData.RETCODE.Split('|');

            user = new User
            {
                email = tmp[1],
                gender = tmp[2],
                birthDate = tmp[3],
                region = tmp[4]
            };

            finishCheckUserData = true;
            result = true;

            StartInitEnvironment();
        }
    }
    public void StartInitEnvironment()
    {
        StartCoroutine(InitEnvironment());
    }
    IEnumerator InitEnvironment()
    {
        while (!finishCheckUserData)
        {
            InitEnvironment();
            yield return null;
        }

        if (result)
            SetEnvironment();
        else
            ShowOneBtnDialog("네트워크 연결 실패", "네트워크 연결에 실패했습니다. 인터넷 환경을 확인 한 후 다시 시도해주세요", "다시 시도", StartInitEnvironment);

        yield return null;
    }
    string sql = "";
    void AnalyzeUserData()
    {
        int[] kmu = new int[3];
        int[] knu = new int[3];
        int[] ynu = new int[3];
        int[] etc = new int[3];
        int[] age = new int[11];
        FirebaseDatabase.DefaultInstance.GetReference("users")
            .GetValueAsync().ContinueWith(task =>
            {
                if (task.IsFaulted)
                {
                    Debug.LogError(task.Exception.Data);
                }
                else if (task.IsCompleted)
                {
                    DataSnapshot snapshot = task.Result;

                    if (snapshot != null)
                    {
                        // DataSnapshot 타입에 저장된 값 불러오기
                        foreach (var item in snapshot.Children)
                        {
                            string i = "'" + item.Key.ToString() + "'";
                            string e = "'" + item.Child("email").Value.ToString() + "'";
                            string r = "'" + item.Child("region").Value.ToString() + "'";
                            string b = "'" + item.Child("birthDate").Value.ToString() + "'";
                            string g = "'" + item.Child("gender").Value.ToString() + "'";

                            int target = 2;

                            if (item.Child("gender").Value.ToString() == "Male")
                            {
                                target = 0;
                            }
                            else if (item.Child("gender").Value.ToString() == "Female")
                            {
                                target = 1;
                            }
                            else
                            {
                                g = "'NONE'";
                            }
                            sql += "INSERT INTO user(id,email,gender,birthDate,region) VALUES(" + i + "," + e + "," + g + "," + b + "," + r + ");\n";

                            switch (item.Child("region").Value.ToString())
                            {
                                case "계명대":
                                    kmu[target]++;
                                    break;
                                case "경북대":
                                    knu[target]++;
                                    break;
                                case "영남대":
                                    ynu[target]++;
                                    break;
                                default:
                                    etc[target]++;
                                    r = "'NONE'";
                                    break;
                            }

                            string birth = item.Child("birthDate").Value.ToString();
                            if(birth.Length > 0)
                            {
                                birth = birth.Substring(0,4);
                                switch (birth)
                                {
                                    case "2001":
                                        age[0]++;
                                        break;
                                    case "2000":
                                        age[1]++;
                                        break;
                                    case "1999":
                                        age[2]++;
                                        break;
                                    case "1998":
                                        age[3]++;
                                        break;
                                    case "1997":
                                        age[4]++;
                                        break;
                                    case "1996":
                                        age[5]++;
                                        break;
                                    case "1995":
                                        age[6]++;
                                        break;
                                    case "1994":
                                        age[7]++;
                                        break;
                                    case "1993":
                                        age[8]++;
                                        break;
                                    case "1992":
                                        age[9]++;
                                        break;
                                    default:
                                        age[10]++;
                                        break;
                                }
                            }
                        }

                        //Debug.Log("계명대 [Total: " + (int)(kmu[0]+kmu[1]+kmu[2]) + "] [남: "+ kmu[0] + "] [여: "+ kmu[1] + "] [미선택: " + kmu[2] +"]");

                        //Debug.Log("경북대 [Total: " + (int)(knu[0] + knu[1] + knu[2]) + "] [남: " + knu[0] + "] [여: " + knu[1] + "] [미선택: " + knu[2] + "]");

                        //Debug.Log("영남대 [Total: " + (int)(ynu[0] + ynu[1] + ynu[2]) + "] [남: " + ynu[0] + "] [여: " + ynu[1] + "] [미선택: " + ynu[2] + "]");

                        //Debug.Log("미선택 [Total: " + (int)(etc[0] + etc[1] + etc[2]) + "] [남: " + etc[0] + "] [여: " + etc[1] + "] [미선택: " + etc[2] + "]");

                        string old = "나이  ";
                        for (int i = 0; i < 10; i ++)
                        {
                            old += "["+(20+i) + "세: " + (age[i]) + "]";
                        }
                        old += "[기타: "+ age[10] +"]";
                        //Debug.Log(old);
                        int[] total = new int[3];
                      
                        total[0] = kmu[0] + knu[0] + ynu[0] + etc[0];
                        total[1] = kmu[1] + knu[1] + ynu[1] + etc[1];
                        total[2] = kmu[2] + knu[2] + ynu[2] + etc[2];

                        //Debug.Log("합  계 [Total: " + (int)(total[0] + total[1] + total[2]) + "] [남: " + total[0] + "] [여: " + total[1] + "] [미선택" + total[2] + "]");
                    }
                }
            });
    }
    void SetEnvironment()
    {
        // 첫 로딩이면 Load URL부터...
        if (placeManager.placeList.Count == 0)
        {
            DataController.Instance.LoadData();
            frontAD.Init();
            categories.Add("전체");
            StartCoroutine(Load_URL());
        }
        //두번째 로딩이면 user 정보만 받아서 업데이트
        else
        {
            SetUserEnvironment();
            loginManager.gameObject.SetActive(false);
        }
    }
    void SetUserEnvironment()
    {
        Debug.Log(auth.CurrentUser.UserId);
        placeManager.SetCurrentPlace();
    }
    public void ShuffleShop()
    {
        System.Random rng = new System.Random();
        int n = shop_infos.Count;

        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            string value = shop_infos[k];
            shop_infos[k] = shop_infos[n];
            shop_infos[n] = value;
        }
    }

    public void Log(string log)
    {
        if(_log != null)
            _log.text = log;
    }

    public void OnTokenReceived(object sender, Firebase.Messaging.TokenReceivedEventArgs token)
    {
        Debug.Log("Received Registration Token: " + token.Token);
    }

    public void OnMessageReceived(object sender, Firebase.Messaging.MessageReceivedEventArgs e)
    {
        Debug.Log("Received a new message from: " + e.Message.From);
    }

    bool finishInitFirebase;
    bool dependencyAvaileable;

    private void FireBaseInit()
    {
        dependencyAvaileable = false;
        finishInitFirebase = false;


        auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
        // Set this before calling into the realtime database.
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://benefit-5cf34.firebaseio.com/");
        // Get the root reference location of the database.
        mDatabaseRef = FirebaseDatabase.DefaultInstance.RootReference;

        

        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {

            var dependencyStatus = task.Result;
            if (dependencyStatus == Firebase.DependencyStatus.Available)
            {
                dependencyAvaileable = true;
                //cloud message
                Firebase.Messaging.FirebaseMessaging.TokenReceived += OnTokenReceived;
                Firebase.Messaging.FirebaseMessaging.MessageReceived += OnMessageReceived;
            }
            else
            {
                dependencyAvaileable = false;
                UnityEngine.Debug.LogError(System.String.Format(
                  "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
            }
            finishInitFirebase = true;
        });

        StartCheckAuth();
    }
    public void UnSubscribe(string topic)
    {
        Firebase.Messaging.FirebaseMessaging.UnsubscribeAsync("/topics/" + topic);
    }
    public void Subscribe(string topic)
    {
        Firebase.Messaging.FirebaseMessaging.SubscribeAsync("/topics/" + topic);
    }
    private void Update()
    {
        /*
         -2: 
         -1:
         0: 메인
         1: 검색
         2: 즐겨찾기
         3: 디테일
         4: 쿠폰
         5: 검색-추천-최근기록
         6: 광고
         7: 화면 전환 중 (Escape키 안 먹히게)
         */

        //if (Application.platform == RuntimePlatform.Android)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                switch (page_index)
                {
                    case 100:
                        break;
                    case 0:
                        float y = shopManager.GetComponent<RectTransform>().anchoredPosition.y;
                        if (Mathf.Abs(y) >= 10.0f)
                            mainScroll.verticalNormalizedPosition = 1;
                        else if (!loginManager.gameObject.activeSelf)
                            Quit();
                        break;
                    case -10: // 마이페이지 일때
                        page_index = 7;
                        homeButton.Click();
                        break;
                    case -1: // 위치 페이지 일때
                        page_index = 7;
                        homeButton.Click();
                        break;
                    case 1:
                        page_index = 7;
                        homeButton.Click();
                        break;
                    case 2:
                        page_index = 7;
                        homeButton.Click();
                        break;
                    case 3:
                        page_index = 7;
                        detailPage.Push();
                        break;
                    case 4:
                        page_index = 7;
                        couponPage.Push();
                        break;
                    case 5:
                        page_index = 7;
                        searchManager.DeActive();
                        break;
                    case 6:
                        page_index = 7;
                        adPage.Push();
                        break;
                    default: 

                        if(page_index < -10) // 마이페이지에서 depth 들어갈 때,
                            myPageManager.Back();

                        if(page_index == 1000) // 응원하기
                        {
                            subwindow.gameObject.SetActive(false);
                        }
                        else if(page_index == 1001) // 응원 보기
                        {
                            subwindow.GetComponent<SubWindow>().Close();
                        }
                        else if(page_index == 1002) // 응원 상세보기
                        {
                            subwindow.gameObject.SetActive(false);
                            page_index = 1001;
                            subwindow = detailPage.cheerBoard.gameObject;
                        }

                        break;
                }
            }
        }


    }
    
    public void OpenGoogleMap(string address)
    {
        string add = address.Replace(" ", "+");
        string new_address = UnityWebRequest.EscapeURL(add);

#if UNITY_ANDROID && !UNITY_EDITOR
        Application.OpenURL("https://www.google.com/maps/search/?api=1&query="+new_address);
#elif UNITY_IOS && !UNITY_EDITOR
        Application.OpenURL("https://maps.apple.com/maps?q=" + new_address);
#else
        Application.OpenURL("https://www.google.com/maps/search/?api=1&query=" + new_address);
#endif
    }
    private void Quit()
    {
#if UNITY_EDITOR
    UnityEditor.EditorApplication.isPlaying = false;
#elif UNITY_WEBPLAYER
                Application.openURL("https://google.com");
#else
                ApplicationManager.AndroidQuit();
#endif
    }
    public static ApplicationManager GetInstance()
    {
        if (!instance)
        {
            instance = FindObjectOfType(typeof(ApplicationManager)) as ApplicationManager;
            if (!instance)
                Debug.LogError("There needs to be one active MyClass script on a GameObject in your scene.");
        }
        return instance;
    }
    public void ChangePage(Page page)
    {
        page.Pull();
    }
    public void CouponClick(Shop shop, bool todayFirstUse)
    {
        couponPage.Pull();
        couponPage.SetShop(shop);
        if(todayFirstUse)
            OpenCoupon(shop._id);
    }

    public static void AndroidQuit()
    {
        AndroidJavaClass ajc = new AndroidJavaClass("com.lancekun.quit_helper.AN_QuitHelper");
        AndroidJavaObject UnityInstance = ajc.CallStatic<AndroidJavaObject>("Instance");
        UnityInstance.Call("AN_Exit");
    }

    public void SetDragThreshold(bool useClick = true)
    {
        if (useClick == true)
        {
            float inchToCm = 2.54f;
            float dragThresholdCM = 0.15f;

            eventSystem.pixelDragThreshold = (int)(dragThresholdCM * Screen.dpi / inchToCm);
        }
        else
            eventSystem.pixelDragThreshold = 0;
    }

    public void ShopClick(ShopBTN btn)
    {
        detailPage.Pull();
        detailPage.SetTitle(btn.shop._name);
        detailPage.authBTN.SetBTN(btn.shop);
        detailPage.shopBTN = btn;
    }
    IEnumerator ActiveUser()
    {
        string authid = auth.CurrentUser.UserId.ToString();
        pData.SetValue("user_id", authid);
        UnityWebRequest keyUrl = pData.ActiveKey("https://benefit.atthemomentkor.com/user_active.php");
        keyUrl.useHttpContinue = false;
        keyUrl.chunkedTransfer = false;
        AsyncOperation async = keyUrl.SendWebRequest();
        
        yield return null;
    }

    public void ShopCountUp(string shop_id, bool useCoupon = true)
    {
        if(useCoupon)
            StartCoroutine(Shop_CouponCountUp(shop_id));
        else
            StartCoroutine(Shop_ViewCountUp(shop_id));
    }
    public void OpenCoupon(string shop_id)
    {
        StartCoroutine(Shop_CouponOpenCountUp(shop_id));
    }
    IEnumerator Shop_CouponCountUp(string shop_id)
    {
        string[] name = { "shop_id", "user_id" };
        string[] pdata = { shop_id, auth.CurrentUser.UserId.ToString() };
        pData.SetValue(name, pdata);
        UnityWebRequest keyUrl = pData.ActiveKey(shop_count);
        keyUrl.useHttpContinue = false;
        keyUrl.chunkedTransfer = false;
        AsyncOperation async = keyUrl.SendWebRequest();
        yield return null;
    }
    IEnumerator Shop_CouponOpenCountUp(string shop_id)
    {
        string[] name = { "shop_id", "user_id" };
        string[] pdata = { shop_id, auth.CurrentUser.UserId.ToString() };
        pData.SetValue(name, pdata);
        UnityWebRequest keyUrl = pData.ActiveKey("https://benefit.atthemomentkor.com/coupon_open_count.php");
        keyUrl.useHttpContinue = false;
        keyUrl.chunkedTransfer = false;
        AsyncOperation async = keyUrl.SendWebRequest();
        yield return null;
    }
    IEnumerator Shop_ViewCountUp(string shop_id)
    {
        string[] name = { "shop_id", "user_id" };
        string[] pdata = { shop_id, auth.CurrentUser.UserId.ToString() };
        pData.SetValue(name, pdata);
        UnityWebRequest keyUrl = pData.ActiveKey("https://benefit.atthemomentkor.com/page_count_up.php");
        keyUrl.useHttpContinue = false;
        keyUrl.chunkedTransfer = false;
        AsyncOperation async = keyUrl.SendWebRequest();
        yield return null;
    }

    public void ShopUpdate(string shop_id, bool hide)
    {
        StartCoroutine(ShopHideUpdate(shop_id,hide));
    }

    IEnumerator ShopHideUpdate(string shop_id, bool hide)
    {
        string[] name = { "shop_id", "shop_hide"};
        string[] pdata = { shop_id, hide.ToString() };
        pData.SetValue(name, pdata);
        UnityWebRequest keyUrl = pData.ActiveKey(URL_update_hide);
        keyUrl.useHttpContinue = false;
        keyUrl.chunkedTransfer = false;
        AsyncOperation async = keyUrl.SendWebRequest();

        while (!async.isDone)
        {
            yield return null;
        }

        if (keyUrl.isNetworkError || keyUrl.isHttpError)
        {
            Debug.Log("네트워크 연결에 실패했습니다.");
        }
        else
        {
            pData.RETCODE = keyUrl.downloadHandler.text;
        }
    }

    public Vector2 GetRatio()
    {
        return new Vector2((_screen.x / 1080.0f), (_screen.y / 1920.0f));
    }

    public void LoadDetail(Shop shop)
    {
        if (shop.CheckAlreadyLoad() == false)
            StartCoroutine(Load_Detail_info(shop));
        else
            infoManager.SetInfo(shop);
    }

    public void LoadBannerContent(BannerInfo info)
    {
        StartCoroutine(Load_Banner_Contents(info));
    }

    public System.DateTime String2Time(string string_date)
    {
        string[] date = string_date.Split('-');

        return new System.DateTime(System.Int32.Parse(date[0]), System.Int32.Parse(date[1]), System.Int32.Parse(date[2]));
    }

    IEnumerator Load_Shops()
    {
        UnityWebRequest keyUrl = pData.ActiveKey(URL_load_shop);

        keyUrl.useHttpContinue = false;
        keyUrl.chunkedTransfer = false;
        AsyncOperation async = keyUrl.SendWebRequest();
        float timer = 0;
        while (!async.isDone)
        {
            yield return null;
            timer += Time.deltaTime;
            /*
            if (loadingProgress != null)
            {
                    timer += Time.deltaTime;
                    if (async.progress >= 0.68f)
                    {
                        //loadingProgress.Progress(0.5f + (Mathf.Lerp(loadingProgress.amount, 0.75f, timer)) * 0.25f);
                    }
                    else
                    {
                        loadingProgress.Progress(0.5f + (Mathf.Lerp(loadingProgress.amount, async.progress, timer)) * 0.25f);
                        if (loadingProgress.amount >= (0.5f + (async.progress * 0.25f)))
                        {
                            timer = 0f;
                        }
                    }
            }*/
        }
       // if (loadingProgress != null)
        //    loadingProgress.Progress(0.75f);

        if (keyUrl.isNetworkError || keyUrl.isHttpError)
        {
            SetNotification("네트워크 연결에 실패했습니다.");
        }
        else
        {
            pData.RETCODE = keyUrl.downloadHandler.text;

            string[] tmp = pData.RETCODE.Split('|');
            if (tmp.Length > 2)
            {
                accessTime = String2Time(tmp[1]);

                for (int i = 2; i < tmp.Length; i++)
                {
                    if (tmp[i] != "")
                    {
                        shop_infos.Add(tmp[i]);
                    }
                }
                ShuffleShop();
                shopManager.MakeShopLists();
                placeManager.SetPlacesCounts();
                StartCoroutine(Load_Banners());
            }
        }
    }

    IEnumerator Load_Banners()
    {
        URL_load_banners = "https://benefit.atthemomentkor.com/load_banners.php";
        UnityWebRequest keyUrl = pData.ActiveKey(URL_load_banners);

        keyUrl.useHttpContinue = false;
        keyUrl.chunkedTransfer = false;
        AsyncOperation async = keyUrl.SendWebRequest();
        while (!async.isDone)
        {
            yield return null;
        }

        if (keyUrl.isNetworkError || keyUrl.isHttpError)
        {
            SetNotification("네트워크 연결에 실패했습니다.");
        }
        else
        {
            pData.RETCODE = keyUrl.downloadHandler.text;

            string[] tmp = pData.RETCODE.Split('|');
            if (tmp.Length > 1)
            {
                for (int i = 1; i < tmp.Length; i++)
                {
                    if (tmp[i] != "")
                    {
                        banners.Add(tmp[i]);
                    }
                }
                bannerManager.MakeBannerLists();
                bannerManager.LoadBanners();
                StartCoroutine(Load_Categories());
            }
        }
    }

    IEnumerator Load_Banner_Contents(BannerInfo info)
    {
        pData.SetValue("banner_id", info._index.ToString());
        URL_load_banner_contents = "https://benefit.atthemomentkor.com/load_banner_contents.php";
        UnityWebRequest keyUrl = pData.ActiveKey(URL_load_banner_contents);

        keyUrl.useHttpContinue = false;
        keyUrl.chunkedTransfer = false;
        AsyncOperation async = keyUrl.SendWebRequest();
        while (!async.isDone)
        {
            yield return null;
        }

        if (keyUrl.isNetworkError || keyUrl.isHttpError)
        {
            SetNotification("네트워크 연결에 실패했습니다.");
        }
        else
        {
            pData.RETCODE = keyUrl.downloadHandler.text;
            info.DivideContentInfo(pData.RETCODE);
        }
    }

    public void SubmitRequest(string[] request)
    {
        if (request.Length < 4)
            return;
        else
            StartCoroutine(Submit_Request(request));
    }

    IEnumerator Submit_Request(string[] request)
    {

        string[] name = { "request_type", "request_title", "request_contact", "request_content" };
        pData.SetValue(name, request);

        UnityWebRequest keyUrl = pData.ActiveKey(URL_submit_request);

        keyUrl.useHttpContinue = false;
        keyUrl.chunkedTransfer = false;


        AsyncOperation async = keyUrl.SendWebRequest();
        
        GameObject progressBar = Instantiate(progressCircle, requsetMgr.submit_screen.transform);
        ///////////////////////////////

        Image circle = null;
        if (progressBar != null)
            circle = progressBar.transform.GetChild(1).GetComponent<Image>();

        float timer = 0;

        while (!async.isDone)
        {
            yield return null;
            timer += Time.deltaTime;
            if (circle != null)
                circle.fillAmount = Mathf.Lerp(circle.fillAmount, async.progress, timer);
        }
        if (circle != null)
            circle.fillAmount = 1.0f;

        GameObject.DestroyImmediate(progressBar);

        if (keyUrl.isNetworkError || keyUrl.isHttpError)
        {
            SetNotification("네트워크 연결에 실패했습니다.");
        }
        else
        {
            pData.RETCODE = keyUrl.downloadHandler.text;
            requsetMgr.submit_screen.transform.GetChild(1).GetComponent<Text>().text = "제출이 완료되었습니다.\n\n빠른 시간 안에 " + pData.RETCODE + " 로 답변드리겠습니다.\n\n문의해주셔서 감사합니다 :D";
        }
    }

    IEnumerator Load_Categories()
    {
        pData.RETCODE = "";
        pData.BACKCODE = "";

        UnityWebRequest keyUrl = pData.ActiveKey(address);

        keyUrl.useHttpContinue = false;
        keyUrl.chunkedTransfer = false;
        AsyncOperation async = keyUrl.SendWebRequest();
        //float timer = 0;
        while (!async.isDone)
        {
            yield return null;
            /*
            if (loadingProgress != null)
            {
                timer += Time.deltaTime;
                if (async.progress >= 0.9f)
                {
                    loadingProgress.Progress((Mathf.Lerp(loadingProgress.amount, 1f, timer)) * 0.25f);
                }
                else
                {
                    loadingProgress.Progress(0.75f +  (Mathf.Lerp(loadingProgress.amount, async.progress, timer)) * 0.25f);
                    if (loadingProgress.amount >= (async.progress * 0.5f))
                    {
                        timer = 0f;
                    }
                }
            }*/
        }
        //loadingProgress.Progress(1.0f);

        if (keyUrl.isNetworkError || keyUrl.isHttpError)
        {
            SetNotification("네트워크 연결에 실패했습니다.");
        }
        else
        {
            pData.RETCODE = keyUrl.downloadHandler.text;

            string[] tmp = pData.RETCODE.Split('/');

            for (int i = 1; i < tmp.Length - 1; i++)
            {
                if (tmp[i] != "")
                {
                    categories.Add(tmp[i]);
                }
            }

            categoryManager.SetCategories(categories);

            if(loadingPage.gameObject.activeSelf)
                loadingPage.Push();
        }
    }

    IEnumerator Load_Places()
    {
        pData.RETCODE = "";
        pData.BACKCODE = "";

        UnityWebRequest keyUrl = pData.ActiveKey(URL_load_places);

        keyUrl.useHttpContinue = false;
        keyUrl.chunkedTransfer = false;
        AsyncOperation async = keyUrl.SendWebRequest();

        while (!async.isDone)
        {
            yield return null;
        }

        if (keyUrl.isNetworkError || keyUrl.isHttpError)
        {
            SetNotification("네트워크 연결에 실패했습니다.");
        }
        else
        {
            pData.RETCODE = keyUrl.downloadHandler.text;

            string[] tmp = pData.RETCODE.Split('|');

            for (int i = 1; i < tmp.Length - 1; i++)
            {
                string[] place = tmp[i].Split('#');
                placeManager.CreatePlaceBTN(place[0], place[1], place[2]);
            }

            placeManager.SetPlaces();

            StartCoroutine(Load_Shops());
        }
    }
    
    IEnumerator Load_Detail_info(Shop shop)
    {
        shop_detail.Clear();
        pData.SetValue("shop_id", shop._id);

        UnityWebRequest keyUrl = pData.ActiveKey(URL_load_detail);

        keyUrl.useHttpContinue = false;
        keyUrl.chunkedTransfer = false;

        yield return keyUrl.SendWebRequest();
        
        if (keyUrl.isNetworkError || keyUrl.isHttpError)
        {
            Debug.Log("failed network connection");
            SetNotification("네트워크 연결에 실패했습니다.");
        }
        else
        {
            pData.RETCODE = keyUrl.downloadHandler.text;

            
            string[] tmp = pData.RETCODE.Split('|');

            shop.SetDetail(tmp);

            infoManager.SetInfo(shop);
        }
    }

    public IEnumerator LoadImg(BenefitImage target, TextureContainer img)
    {
        if (target.gameObject.activeSelf == false)
            yield return null;

        if (target.GetComponent<RawImage>() == null)
            yield return null;

        GameObject progressBar = Instantiate(progressCircle, target.transform);

        target.isLoaded = false;
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(img.URL);

        AsyncOperation async = www.SendWebRequest();
        Image circle = null;
        if (progressBar != null)
            circle = progressBar.transform.GetChild(1).GetComponent<Image>();
        float timer = 0;

        while (!async.isDone)
        {
            yield return null;
            timer += Time.deltaTime;
            if (circle != null)
                circle.fillAmount = Mathf.Lerp(circle.fillAmount, async.progress, timer);
        }

        if (circle != null)
            circle.fillAmount = 1.0f;

        GameObject.DestroyImmediate(progressBar);

        if (target != null)
        {
            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log("failed network connection");
                SetNotification("네트워크 연결에 실패했습니다.");
                img.texture = null;
                target.Set(null);

                target.isLoaded = true;
            }
            else
            {
                Texture texture = ((DownloadHandlerTexture)www.downloadHandler).texture;
                img.texture = texture;
                target.Set(img);

                target.isLoaded = true;
            }
        }
    }
    public void GetPolicy(Text policy)
    {
        StartCoroutine(ShowPolicy(policy));
    }
    IEnumerator ShowPolicy(Text policy)
    {
        UnityWebRequest keyUrl = pData.ActiveKey(URL_policy);

        keyUrl.useHttpContinue = false;
        keyUrl.chunkedTransfer = false;

        yield return keyUrl.SendWebRequest();

        if (keyUrl.isNetworkError || keyUrl.isHttpError)
        {
            Debug.Log("failed network connection");
            SetNotification("네트워크 연결에 실패했습니다.");
        }
        else
        {
            Debug.Log(keyUrl.downloadHandler.text);
            policy.text = keyUrl.downloadHandler.text;
        }
    }
    public void UpdateUserData(string data, string value)
    {
            StartCoroutine(ChangeRegion(data, value));
    }
    IEnumerator ChangeRegion(string data, string value)
    {

        string[] name = { "user_id","data", data };
        string[] pdata = { auth.CurrentUser.UserId.ToString(),data, value };
        pData.SetValue(name,pdata);
        UnityWebRequest keyUrl = pData.ActiveKey("https://benefit.atthemomentkor.com/user_update.php");
        keyUrl.useHttpContinue = false;
        keyUrl.chunkedTransfer = false;
        AsyncOperation async = keyUrl.SendWebRequest();
        yield return null;
    }
    IEnumerator DeleteUserDataQuery()
    {
        pData.SetValue("user_id", auth.CurrentUser.UserId.ToString());
        UnityWebRequest keyUrl = pData.ActiveKey("https://benefit.atthemomentkor.com/user_delete.php");
        keyUrl.useHttpContinue = false;
        keyUrl.chunkedTransfer = false;
        AsyncOperation async = keyUrl.SendWebRequest();
        yield return null;
    }
    public void PhoneCall(string number)
    {
        string phoneNumber = number.Replace("-", "");

#if UNITY_ANDROID && !UNITY_EDITOR
        Application.OpenURL( "tel://" + phoneNumber );
#elif UNITY_IOS && !UNITY_EDITOR
        IGApps.OpenDialer(phoneNumber);
#else
        /*
        string path = "C:/Users/viny5/Desktop/icou/test.txt";

        //Write some text to the test.txt file
        StreamWriter writer = new StreamWriter(path, true);
        writer.WriteLine(sql);
        writer.Close();*/
#endif
    }

    public void ShowPermissionErrorDialog()
    {
        ShowOneBtnDialog("전화 실패","휴대폰 설정에서 전화 접근을 허용하지 않았습니다. 설정 변경 후 다시 시도해주세요","확인",null);
    }

    static int GetSDKInt()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        using (var version = new AndroidJavaClass("android.os.Build$VERSION"))
        {
            return version.GetStatic<int>("SDK_INT");
        }
#else
        return -1;
#endif
    }
    public void ShowToast(string message)
    {
        toastMsg.gameObject.SetActive(true);
        toastMsg.Show(message);
    }

}

public static class pData
{
    static List<IMultipartFormSection> formData = new List<IMultipartFormSection>();

    static UnityWebRequest url;
    public static string RETCODE;
    public static string BACKCODE;

    public static UnityWebRequest ActiveKey(string _address)
    {
        UnityWebRequest url = UnityWebRequest.Post(_address, formData);
        return url;
    }

    public static void SetValue(string post_name, string category)
    {
       formData.Clear();
       formData.Add(new MultipartFormDataSection(post_name, category));
    }
    public static void SetValue(string[] post_name, string[] category)
    {
        if (post_name.Length != category.Length)
            return;

        formData.Clear();

        for (int i = 0; i < post_name.Length; i ++)
        {
            if(category[i].Length > 0)
                formData.Add(new MultipartFormDataSection(post_name[i], category[i]));
        }
    }
}
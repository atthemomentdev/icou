﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopManager : MonoBehaviour
{
    ApplicationManager instance;
       
    public Transform contents;

    int num = 0;
    int count = 0;
    public List<Shop> shop_list = new List<Shop>();
    public List<Shop> target_shop_list = new List<Shop>();
    private List<GameObject> buttons = new List<GameObject>();

    public Transform shopParent;

    [SerializeField]
    public GameObject shop;

    // Start is called before the first frame update
    void Start()
    {
        instance = ApplicationManager.GetInstance();
    }

    private void OnEnable()
    {
        if (buttons.Count == 0)
            return;
        //LoadImage();
    }

    public List<Shop> GetLikeShopList()
    {
        List<Shop> like_list = new List<Shop>();

        like_list = new List<Shop>();

        foreach(var shop in shop_list)
        {
            if (DataController.Instance.data.like_list.Contains(shop._id))
            {
                like_list.Add(shop);
            }
        }
        
        return like_list;
    }

    public void OpenShopDetail(int id)
    {
        Shop a = shop_list.Find(x => System.Int16.Parse(x._id) == id);
        buttons.Find(x => x.GetComponent<ShopBTN>().shop._name == a._name).GetComponent<ShopBTN>().Click();
    }

    public void MakeShopLists()
    {
        int i = 0;

        foreach (var info in instance.shop_infos)
        {
            string[] tmp = info.Split('#');
            if (tmp.Length < 6)
            {
                return;
            }
            //id,name,category,branch,url,discount
            Shop new_shop = Instantiate(shop, shopParent).GetComponent<Shop>();
            new_shop.SetShop(tmp[0], tmp[5], tmp[1], tmp[2], tmp[4], tmp[3], tmp[6], tmp[7]);

            int placeID = int.Parse(tmp[7]);
            ApplicationManager.GetInstance().placeManager.counts[placeID]++;

            if (new_shop.new_shop == true)
                shop_list.Insert(0, new_shop);
            else
                shop_list.Add(new_shop);
            i++;
        }
        count = i;
    }
    public void SetTargetShopList(string category)
    {
        if(target_shop_list != null)
            target_shop_list.Clear();

        target_shop_list = new List<Shop>();

        string place = DataController.Instance.data.placeId;
        foreach (var shop in shop_list)
        {
            if (((place == null) || (place == shop._placeId)) &&
                ((category == "전체") || (category == shop._category)))
                target_shop_list.Add(shop);
        }
    }
    public void SetTargetShopList()
    {
        if (target_shop_list != null)
            target_shop_list.Clear();

        target_shop_list = new List<Shop>();

        string place = DataController.Instance.data.placeId;
        foreach (var shop in shop_list)
        {
            target_shop_list.Add(shop);
        }
    }
    public void ContinueLoadImage()
    {
        for (int i = num; i < (num + 10); i++)
        {
            if (i >= buttons.Count)
                return;

            buttons[i].GetComponent<ShopBTN>().SetImg();
        }
    }

    public void LoadFinish()
    {
        num++;

        if((num%10) == 0)
        {
            LoadImage();
        }
    }

    public void LoadImage()
    {
        for(int i = num; i < (num + 10); i ++)
        {
            if (i >= buttons.Count)
                return;
            if(buttons[i].gameObject.activeSelf)
                buttons[i].GetComponent<ShopBTN>().SetImg();
        }
    }
    

    public void CreateNewBTNs(List<Shop> list, bool searchBTN = false)
    {
        target_shop_list = list;
        
        contents.GetComponent<MyGridLayout>().Init(this);

    }

    public void ChangeCategory(string category)
    {
        num = 0;

        if (instance == null)
            instance = ApplicationManager.GetInstance();

        foreach (var info in buttons)
            GameObject.Destroy(info);

        buttons = new List<GameObject>();

        SetTargetShopList(category);
        contents.GetComponent<MyGridLayout>().Init(this);
    }

}

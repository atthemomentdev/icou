﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SearchManager : MonoBehaviour
{
    public InputField inputField;
    public Button initButton;
    public Search result;
    public Search record;

    public List<Shop> search_list = new List<Shop>();

    private void OnEnable()
    {
        if (inputField.text.Length > 0)
            result.GetComponent<Search>().SearchItem(inputField.text);
        else
            ShowRecorded();
    }
    public List<Shop> GetSearch_list()
    {
        search_list.Clear();
        search_list = new List<Shop>();
        foreach (var shops in DataController.Instance.data.search_list)
        {
            foreach(var shop in ApplicationManager.GetInstance().shopManager.shop_list)
            {
                if(shop._id == shops)
                {
                    search_list.Add(shop);
                    break;
                }
            }
        }
        return search_list;
    }
    public void InitClick()
    {
        inputField.text = "";
        inputField.ActivateInputField();
    }
    bool isShrink = false;
    void SetSize(bool shrink)
    {
        if(shrink)
        {
            if(isShrink == false)
            {
                float x_shrink = 0.88f;
                Vector2 size = inputField.transform.GetComponent<RectTransform>().sizeDelta;
                inputField.transform.GetComponent<RectTransform>().sizeDelta = new Vector2(size.x * x_shrink, size.y);
                isShrink = true;
            }
        }
        else
        {
            if(isShrink == true)
            {
                float x_shrink = 0.88f;
                Vector2 size = inputField.transform.GetComponent<RectTransform>().sizeDelta;
                inputField.transform.GetComponent<RectTransform>().sizeDelta = new Vector2(size.x / x_shrink, size.y);
                isShrink = false;
            }
        }
    }
    public void DeActive()
    {
        InitClick();
        inputField.DeactivateInputField();
        result.gameObject.SetActive(false);
        record.gameObject.SetActive(true);
        ApplicationManager.GetInstance().page_index = 1;
        ApplicationManager.GetInstance().mainPage.page_index = 1;
    }
    public void Search()
    {
        inputField.DeactivateInputField();
        ShowResultWindow();
    }
    public void ShowResultWindow()
    {
        if (inputField.text.Length == 0)
            ShowRecorded();
        else
            ShowSearchResult();
    }
    public void ShowRecorded()
    {
        initButton.gameObject.SetActive(false);
        result.gameObject.SetActive(false);
        record.gameObject.SetActive(true);

        record.GetComponent<Search>().ShowRecordedBTN();
    }
    public void ShowSearchResult()
    {
        initButton.gameObject.SetActive(true);
        ApplicationManager.GetInstance().page_index = 5;
        ApplicationManager.GetInstance().mainPage.page_index = 5;

        result.gameObject.SetActive(true);
        record.gameObject.SetActive(false);
        result.GetComponent<Search>().SearchItem(inputField.text);
    }
}

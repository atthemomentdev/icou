﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapContent : Content
{
    private MapTile mapTile;
    // Start is called before the first frame update
    void Start()
    {
        mapTile = this.transform.GetComponent<MapTile>();
    }
    public override void SetInfo(string content)
    {
        if (mapTile == null)
            Start();

        string[] coord = content.Split(',');

        float x = float.Parse(coord[1]);
        float y = float.Parse(coord[0]);

        mapTile.LoadMap(y,x);
    }
}

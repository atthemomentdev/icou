﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Map : MonoBehaviour
{
    [HideInInspector]
    public Slider slider;
    
    public MapManager mapManager;

    int i = 0;
    /*
        여기에서 MyScrollRect의 Origin을 어찌 어찌 해줘야할듯...!
    */
    public void ScaleMap(float scale)
    {
        float value = scale;
        if ((value >= 2))
        {
            if (mapManager.zoom < 19)
            {
                if (mapManager.load == false)
                {
                    mapManager.zoom++;
                    mapManager.StartLoad();
                }
                mapManager.scaleDuringLoading =  scale * 0.5f;
            }
            else
                value = 2;
        }
        else if (value <= 0.5)
        {
            if (mapManager.zoom > 7)
            {
                if (mapManager.load == false)
                {
                    mapManager.zoom--;
                    mapManager.StartLoad();
                }

                mapManager.scaleDuringLoading = scale * 2f;
            }
            else
                value = 0.5f;
        }

        this.transform.localScale = new Vector3(value, value, 1);
    }

    public void LoadEntireMap()
    {
        int count = this.transform.childCount;
        int center = (count - 1) / 2;

        LoadMap(center);

        for (int i = 1; i < (count - center); i++)
        {
            LoadMap(center + i);
            LoadMap(center - i);
        }
    }

    public void FinishLoad()
    {
        i++;
        if(i == this.transform.childCount)
        {
            mapManager.FinishLoad();
            i = 0;
        }
    }
    
    void LoadMap(int index){
        int colmn_count = this.GetComponent<GridLayoutGroup>().constraintCount;
        

        int colmn = (index % colmn_count) - (colmn_count / 2);
        int row = -((index / colmn_count) - (colmn_count / 2));
        
        MapTile map = this.transform.GetChild(index).GetComponent<MapTile>();
        
        map.LoadMap((mapManager.y + mapManager.currDY * row),
                    (mapManager.x + mapManager.currDX * colmn),
                     mapManager.zoom,true);
    }
}

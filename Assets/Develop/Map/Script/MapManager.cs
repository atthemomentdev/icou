﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapManager : MonoBehaviour
{
    [HideInInspector]
    public int zoom = 16;
    [HideInInspector]
    public Map backBuffer;
    [HideInInspector]
    public Map frontBuffer;

    [HideInInspector]
    public MyScrollRect scroll;

    [HideInInspector]
    public double y = 35.8549f;

    [HideInInspector]
    public double x = 128.4925f;

    [HideInInspector]
    double DX = 0.0043;

    [HideInInspector]
    double DY = 0.00272;

    [HideInInspector]
    public double currDX = 0.0043;

    [HideInInspector]
    public double currDY = 0.00272;

    int count = 0;
    //이거 나중에는 한번만 설정해주자
    float space;

    [HideInInspector]
    public bool load = false;

    [HideInInspector]
    public float scaleDuringLoading = 1;

    Vector3 prevPos = new Vector2();
    // Start is called before the first frame update
    void Start()
    {
        currDX = DX;
        currDY = DY;
        backBuffer = this.transform.GetChild(0).GetComponent<Map>();
        frontBuffer = this.transform.GetChild(1).GetComponent<Map>();
        scroll = this.transform.parent.parent.GetComponent<MyScrollRect>();
        count = backBuffer.GetComponent<GridLayoutGroup>().constraintCount;
    }

    public void SetInfo(float new_x, float new_y)
    {
        Start();

        x = new_x;
        y = new_y;

        backBuffer.LoadEntireMap();
    }

    void SetSpriteNull()
    {
        for (int i = 0; i < count* count ; i++)
        {
            frontBuffer.transform.GetChild(i).GetComponent<MapTile>().StopLoad();
            frontBuffer.transform.GetChild(i).GetComponent<RawImage>().texture = null;
            frontBuffer.transform.GetChild(i).GetComponent<RawImage>().color = new Color(0.94117f, 0.92941f, 0.89803f, 1);
        }
    }
    public void StartLoad()
    {
        load = true;
        if (zoom == 16)
        {
            currDX = DX;
            currDY = DY;
        }
        else if (zoom > 16)
        {
            int num = zoom - 16;
            float scale = 1f;

            for (int i = 0; i < num; i++)
                scale *= 0.5f;

            currDX = DX * scale;
            currDY = DY * scale;
        }
        else
        {
            int num = 16 - zoom;
            float scale = 1f;
            for (int i = 0; i < num; i++)
                scale *= 2f;

            currDX = DX * scale;
            currDY = DY * scale;
        }

        Vector2 size = frontBuffer.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta;
        Vector2 pos = this.GetComponent<RectTransform>().anchoredPosition;
        x = x - currDX * pos.x / size.x;
        y = y - currDY * pos.y / (size.y - space);
        
        backBuffer.LoadEntireMap();
    }
    public void FinishLoad()
    {
        load = false;

        SetSpriteNull();

        this.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
        frontBuffer.transform.SetSiblingIndex(0);

        backBuffer = this.transform.GetChild(0).GetComponent<Map>();
        backBuffer.GetComponent<ContentSizeFitter>().enabled = true;
        backBuffer.GetComponent<GridLayoutGroup>().enabled = true;
        backBuffer.transform.localScale = new Vector3(1, 1, 1);

        frontBuffer = this.transform.GetChild(1).GetComponent<Map>();
        frontBuffer.GetComponent<ContentSizeFitter>().enabled = false;
        frontBuffer.GetComponent<GridLayoutGroup>().enabled = false;
        frontBuffer.transform.localScale = new Vector3(scaleDuringLoading, scaleDuringLoading, 1);

        if ((scaleDuringLoading >= 2) || (scaleDuringLoading <= 0.5))
            frontBuffer.ScaleMap(scaleDuringLoading);
        else
            scaleDuringLoading = 1;

        origin = new Vector2(0.5f, 0.5f);
        prevPos = this.transform.position;
        scroll.prevDistance = 0;
    }

    Vector2 origin = new Vector2(0.5f,0.5f);
    public void Scale(float scale = 0)
    {
        if ((zoom >= 19) && scale > 1)
            return;

        if ((zoom <= 7) && scale < 1)
            return;

        else
            origin.x = frontBuffer.transform.localScale.x;
            origin.y = frontBuffer.transform.localScale.y;
            frontBuffer.GetComponent<Map>().ScaleMap(scale);
            SetPosition();
    }
    void SetPosition()
    {
        Vector2 newPos = this.GetComponent<RectTransform>().anchoredPosition;
        newPos.x = newPos.x * frontBuffer.transform.localScale.x / origin.x;
        newPos.y = newPos.y * frontBuffer.transform.localScale.y / origin.y;

        this.GetComponent<RectTransform>().anchoredPosition = newPos;
    }
    public void UpdateMap()
    {
        Vector3 direction = this.transform.position - prevPos;

        MapTile ref_tile;
        MapTile target_tile = backBuffer.transform.GetChild(count * count - 1).GetComponent<MapTile>();

        
        Vector2 pos = frontBuffer.transform.GetChild(count * count - 1).position;
        Vector2 size = frontBuffer.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta;

        space = size.y - (frontBuffer.transform.GetChild(0).GetComponent<RectTransform>().anchoredPosition.y
                              - frontBuffer.transform.GetChild(count).GetComponent<RectTransform>().anchoredPosition.y);

        if (direction.y < 0) // SHOW UPPER SIDE
        {
            Vector2 pos2 = frontBuffer.transform.GetChild(0).position;
            float pos3y = this.transform.parent.parent.position.y;
            
            if (pos2.y < pos3y)
            {
                for (int i = 0; i < count; i++)
                {
                    target_tile = frontBuffer.transform.GetChild((count - 1) * count + i).GetComponent<MapTile>();
                    ref_tile = frontBuffer.transform.GetChild(2 * i).GetComponent<MapTile>();

                    double newY = ref_tile.locationY + currDY;
                    target_tile.LoadMap(newY, target_tile.locationX, zoom, false);

                    target_tile.transform.SetSiblingIndex(i);
                    pos = ref_tile.GetComponent<RectTransform>().anchoredPosition;
                    target_tile.GetComponent<RectTransform>().anchoredPosition = new Vector2(pos.x, pos.y + size.y - space);
                }
            }
        }
        else if (direction.y > 0) // UP
        {
            Vector2 pos2 = frontBuffer.transform.GetChild(count).position;
            float pos3y = this.transform.parent.parent.position.y;
            if (pos2.y > pos3y)
            {
                for (int i = 0; i < count; i++)
                {
                    target_tile = frontBuffer.transform.GetChild(0).GetComponent<MapTile>();
                    ref_tile = frontBuffer.transform.GetChild((count - 1) * count).GetComponent<MapTile>();

                    double newY = ref_tile.locationY - currDY;
                    target_tile.LoadMap(newY, target_tile.locationX, zoom, false);

                    target_tile.transform.SetSiblingIndex(count * count - 1);
                    pos = ref_tile.GetComponent<RectTransform>().anchoredPosition;
                    target_tile.GetComponent<RectTransform>().anchoredPosition = new Vector2(pos.x, pos.y - size.y + space);
                }
            }
        }
        if (direction.x > 0)  // LEFT
        {
            Vector2 pos2 = frontBuffer.transform.GetChild(0).position;
            if (pos2.x > 0)
            {
                for (int i = 0; i < count; i++)
                {
                    target_tile = frontBuffer.transform.GetChild((i + 1) * count - 1).GetComponent<MapTile>();
                    ref_tile = frontBuffer.transform.GetChild(i * count).GetComponent<MapTile>();

                    double newX = ref_tile.locationX - currDX;
                    target_tile.LoadMap(target_tile.locationY, newX, zoom, false);

                    target_tile.transform.SetSiblingIndex(i * count);
                    pos = ref_tile.GetComponent<RectTransform>().anchoredPosition;
                    target_tile.GetComponent<RectTransform>().anchoredPosition = new Vector2(pos.x - size.x, pos.y);
                }
            }
        }
        else if (direction.x < 0) // RIGHT
        {
            Vector2 pos2 = frontBuffer.transform.GetChild(count * count - 1).position;

            if (pos2.x < ApplicationManager.GetInstance()._screen.x)
            {
                for (int i = 0; i < count; i++)
                {
                    target_tile = frontBuffer.transform.GetChild(i * count).GetComponent<MapTile>();
                    ref_tile = frontBuffer.transform.GetChild((i + 1) * count - 1).GetComponent<MapTile>();

                    double newX = ref_tile.locationX + currDX;
                    target_tile.LoadMap(target_tile.locationY, newX, zoom, false);

                    target_tile.transform.SetSiblingIndex((i + 1) * count - 1);
                    pos = ref_tile.GetComponent<RectTransform>().anchoredPosition;
                    target_tile.GetComponent<RectTransform>().anchoredPosition = new Vector2(pos.x + size.x, pos.y);
                }
            }
        }
        prevPos = this.transform.position;
    }
}

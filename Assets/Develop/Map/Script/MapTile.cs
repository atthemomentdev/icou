﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class MapTile : MonoBehaviour
{
    RawImage img;
    string url;
    
    string coordinate;
    
    float moveY;

    [HideInInspector]
    bool finish_load = true;

    float alpha = 0;

    [HideInInspector]
    public double locationX;
    [HideInInspector]
    public double locationY;
    [HideInInspector]
    public int zoom;
    
    // Use this for initialization
    void Start()
    {
        img = gameObject.GetComponent<RawImage>();
    }

    IEnumerator Load(bool bufferSwitch)
    {
        finish_load = false;
        //if (ApplicationManager.GetInstance().APIKey != "")
        {
            string key = "AIzaSyBnkjxsetwzJwnJFO7-ipu_8GI12w1y8HA=";
            string secret = "GGKMB2sDo_4vdTazrUrvhhnLNSw=";
            img.texture = null;
            img.color   = new Color(0.94117f,0.92941f,0.89803f,1);

            string location_string = locationY + "," + locationX;
            img = this.GetComponent<RawImage>();

            string label = "&markers=size:0%7Ccolor:blue%7C%7C" + location_string;


            url = "https://maps.googleapis.com/maps/api/staticmap?center=" + location_string +
                  "&zoom=" + zoom + "&size=400x300&scale=2" + label
                  + "&key=" + key;
            string signedURL = SignUrl.GoogleSignedUrl.Sign(url, secret);
            
            UnityWebRequest www = UnityWebRequestTexture.GetTexture(signedURL);
            
            AsyncOperation async = www.SendWebRequest();

            while (!async.isDone)
            {
                yield return null;
            }

            finish_load = true;
            img.color = new Color(1, 1, 1, 1);
            img.texture = ((DownloadHandlerTexture)www.downloadHandler).texture;
            
            StartCoroutine(Fade());
        }
    }
    public void NewLoad()
    {
    }
    IEnumerator Fade()
    {
        alpha += 0.25f;
        img.color = new Color(1,1,1,alpha);
        yield return null;
        alpha += 0.25f;
        img.color = new Color(1, 1, 1, alpha);
        yield return null;
        alpha += 0.25f;
        img.color = new Color(1, 1, 1, alpha);
        yield return null;
        alpha += 0.25f;
        img.color = new Color(1, 1, 1, alpha);
        yield return null;
    }
    Coroutine co;
    public void LoadMap(double new_locationY, double new_locationX)
    {
        if (img == null)
            Start();

        locationX = new_locationX;
        locationY = new_locationY;

        zoom = 17;
        if (finish_load == false)
        {
            StopCoroutine(co);
            finish_load = true;
        }
        co = StartCoroutine(Load(false));
    }
    public void LoadMap(double new_locationY, double new_locationX, int new_zoom, bool bufferSwitch)
    {
        if (img == null)
            Start();

        locationX = new_locationX;
        locationY = new_locationY;

        zoom = new_zoom;
        if(finish_load == false)
        {
            StopCoroutine(co);
            finish_load = true;
        }
        co = StartCoroutine(Load(bufferSwitch));
    }
    public void StopLoad()
    {
        if (finish_load == false)
        {
            StopCoroutine(co);
            finish_load = true;
        }
    }
    public void LoadURL()
    {
        ApplicationManager.GetInstance().OpenGoogleMap(this.transform.parent.GetComponent<InfoManager>().shop._address);
    }
}